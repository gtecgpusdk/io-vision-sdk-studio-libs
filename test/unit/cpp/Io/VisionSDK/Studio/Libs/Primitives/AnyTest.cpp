/* ********************************************************************************************************* *
*
* Copyright (c) 2017-2019 NXP
*
* SPDX-License-Identifier: BSD-3-Clause
* The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
* or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
*
* ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Io::VisionSDK::Studio::Libs::Primitives {

    TEST(AnyTest, Cast) {
        Any x = 4;
        ASSERT_EQ(4, Any::Cast<int>(x));
        Any f = 1.1;
        ASSERT_EQ(1.1, Any::Cast<double>(f));
        Any c = string("hello");
        ASSERT_STREQ("hello", Any::Cast<string>(c).c_str());
        c = 11;
        ASSERT_EQ(11, Any::Cast<int>(c));
    }

    TEST(AnyTest, getValue) {
        Any x = 4;
        ASSERT_EQ(4, x.getValue<int>());
    }

    TEST(AnyTest, getValueRef) {
        Any x = 1.000f;
        ASSERT_EQ(1.000f, x.getValue<float>());
        ASSERT_EQ(1.000f, *x.getValueRef<float>());
    }
}
