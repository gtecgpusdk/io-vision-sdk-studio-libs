/* ********************************************************************************************************* *
*
* Copyright (c) 2017-2019 NXP
*
* SPDX-License-Identifier: BSD-3-Clause
* The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
* or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
*
* ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Io::VisionSDK::Studio::Libs::Media {

    class CameraTest : public testing::Test {
     protected:
        void SetUp() override {
            vx_status status;

            this->context = vxCreateContext();

            this->graph = vxCreateGraph(this->context);
            status = vxGetStatus((vx_reference)this->graph);
            if (status != VX_SUCCESS) {
                GTEST_FATAL_FAILURE_(": Testing VxGraph can not be initialized.");
            }
        }

        void TearDown() override {
            vxReleaseGraph(&this->graph);
            vxReleaseContext(&this->context);
        }

     public:
        vx_context context = nullptr;
        vx_graph graph = nullptr;
    };

    TEST_F(CameraTest, OpenClose) {
        Camera camera(this->context);
        ASSERT_TRUE(camera.Open(0));
        camera.Close();
        ASSERT_FALSE(camera.IsOpen());
    }

    TEST_F(CameraTest, Properties) {
        Camera camera(this->context);
        ASSERT_TRUE(camera.Open(0));
        ASSERT_EQ(camera.getWidth(), 640);
        ASSERT_EQ(camera.getHeight(), 480);

        camera.Close();
    }

    TEST_F(CameraTest, Capture) {
        Camera camera(this->context);

        ASSERT_TRUE(camera.Open(0));
        vx_image frame = nullptr;

        for (int i = 0; i < 30; i++) {
            camera.getFrame(frame, true);
            vx_df_image type;
            ASSERT_EQ(vxQueryImage(frame, vx_image_attribute_e::VX_IMAGE_FORMAT, &type, sizeof(type)), vx_status_e::VX_SUCCESS);
            ASSERT_EQ(type, vx_df_image_e::VX_DF_IMAGE_RGB);
        }

        vxReleaseImage(&frame);
    }
}
