/* ********************************************************************************************************* *
*
* Copyright (c) 2017-2019 NXP
*
* SPDX-License-Identifier: BSD-3-Clause
* The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
* or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
*
* ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Io::VisionSDK::Studio::Libs::Media {

    class VideoTest : public testing::Test {
     protected:
        void SetUp() override {
            vx_status status;

            this->context = vxCreateContext();

            this->graph = vxCreateGraph(this->context);
            status = vxGetStatus((vx_reference)this->graph);
            if (status != vx_status_e::VX_SUCCESS) {
                GTEST_FATAL_FAILURE_(": Testing VxGraph can not be initialized.");
            }
        }

        void TearDown() override {
            vxReleaseGraph(&this->graph);
            vxReleaseContext(&this->context);
        }

     public:
        string testDir = "test/resource/data/Io/VisionSDK/Studio/Libs/Media/";
        string testFile = "video.mp4";
        vx_context context = nullptr;
        vx_graph graph = nullptr;
    };

    TEST_F(VideoTest, OpenClose) {
        Video video(this->context);

        ASSERT_TRUE(video.Open(this->testDir + this->testFile));
        video.Close();
        ASSERT_FALSE(video.IsOpen());
    }

    TEST_F(VideoTest, Properties) {
        Video video(this->context);

        ASSERT_TRUE(video.Open(this->testDir + this->testFile));

        ASSERT_EQ(0.0, video.getCurrentTime());
        ASSERT_EQ(921, video.getTotalFrames());
        ASSERT_EQ(0, video.getCurrentFrame());
        ASSERT_EQ(30.0, video.getFps());

        video.Close();
    }

    TEST_F(VideoTest, OpenVideoOutput) {
        Video video(this->context);

        ASSERT_TRUE(video.Open(this->testDir + "some" + this->testFile + ".avi", 30, vx_df_image_e::VX_DF_IMAGE_RGB, 640, 480));
        video.Close();

        ASSERT_TRUE(video.Open(this->testDir + "some" + this->testFile + ".avi", 30, vx_df_image_e::VX_DF_IMAGE_RGB, 640, 480, "PIM1"));
        video.Close();

        ASSERT_TRUE(video.Open(this->testDir + "some" + this->testFile + ".avi", 30, vx_df_image_e::VX_DF_IMAGE_RGB, 640, 480, "MJPG"));
        video.Close();

        testing::internal::CaptureStderr();
        ASSERT_FALSE(video.Open(this->testDir + "some" + this->testFile + ".avi", 30, vx_df_image_e::VX_DF_IMAGE_RGB, 640, 480, "XQX"));
        ASSERT_FALSE(testing::internal::GetCapturedStderr().empty());
    }

    TEST_F(VideoTest, CaptureAndSave) {
        Video video(this->context);
        Video videoOut(this->context);

        ASSERT_TRUE(video.Open(this->testDir + this->testFile));
        vx_image frame = nullptr;
        video.getFrame(frame, true);

        vx_uint32 height, width;
        ASSERT_EQ(vxQueryImage(frame, vx_image_attribute_e::VX_IMAGE_WIDTH, &width, sizeof(vx_uint32)), vx_status_e::VX_SUCCESS);
        ASSERT_EQ(vxQueryImage(frame, vx_image_attribute_e::VX_IMAGE_HEIGHT, &height, sizeof(vx_uint32)), vx_status_e::VX_SUCCESS);

        ASSERT_TRUE(videoOut.Open(this->testDir + "test" + this->testFile + ".avi", 30.0, vx_df_image_e::VX_DF_IMAGE_U8, width, height));

        vx_image rChannel = vxCreateImage(this->context, width, height, vx_df_image_e::VX_DF_IMAGE_U8);
        vx_image procImg = vxCreateImage(this->context, width, height, vx_df_image_e::VX_DF_IMAGE_U8);
        ASSERT_EQ(vxGetStatus((vx_reference)vxChannelExtractNode(this->graph, frame, vx_channel_e::VX_CHANNEL_R, rChannel)),
                  vx_status_e::VX_SUCCESS);
        ASSERT_EQ(vxGetStatus((vx_reference)vxNotNode(this->graph, rChannel, procImg)), vx_status_e::VX_SUCCESS);

        ASSERT_EQ(vxVerifyGraph(this->graph), vx_status_e::VX_SUCCESS);

        long lastStart = static_cast<long>(std::chrono::duration_cast<std::chrono::milliseconds>(
                std::chrono::system_clock::now().time_since_epoch()).count());

        for (int i = 0; i < 30; i++) {
            long current = static_cast<long>(std::chrono::duration_cast<std::chrono::milliseconds>(
                    std::chrono::system_clock::now().time_since_epoch()).count());
            double waitFor = (1 / video.getFps()) * 1000 - (current - lastStart);

            if (waitFor > 0) {
                std::this_thread::sleep_for(std::chrono::milliseconds((long)(waitFor)));
            }
            // std::cout << "loop: " << i << "; slept: " << waitFor << std::endl;
            video.getFrame(frame, true);
            vx_df_image type;
            ASSERT_EQ(vxQueryImage(frame, vx_image_attribute_e::VX_IMAGE_FORMAT, &type, sizeof(type)), vx_status_e::VX_SUCCESS);
            ASSERT_EQ(type, vx_df_image_e::VX_DF_IMAGE_RGB);

            ASSERT_EQ(vxProcessGraph(this->graph), vx_status_e::VX_SUCCESS);

            lastStart = static_cast<long>(std::chrono::duration_cast<std::chrono::milliseconds>(
                    std::chrono::system_clock::now().time_since_epoch()).count());
//            Display::Show(this->context, frame, "test");
//            Display::Show(this->context, procImg, "proc");
            videoOut.setFrame(procImg);
        }

        vxReleaseImage(&frame);
        vxReleaseImage(&rChannel);
        vxReleaseImage(&procImg);
        // Display::WaitForAllClosed();
    }
}
