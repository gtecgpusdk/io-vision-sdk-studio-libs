/* ********************************************************************************************************* *
*
* Copyright (c) 2017-2019 NXP
*
* SPDX-License-Identifier: BSD-3-Clause
* The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
* or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
*
* ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Io::VisionSDK::Studio::Libs::Media {

    class DisplayTest : public testing::Test {
     protected:
        void SetUp() override {
            vx_status status;

            this->context = vxCreateContext();

            this->graph = vxCreateGraph(this->context);
            status = vxGetStatus((vx_reference)this->graph);
            if (status != VX_SUCCESS) {
                GTEST_FATAL_FAILURE_(": Testing VxGraph can not be initialized.");
            }
        }

        void TearDown() override {
            vxReleaseGraph(&this->graph);
            vxReleaseContext(&this->context);
        }

     public:
        vx_status create(int $width, int $height) {
            this->vxImageInput = vxCreateImage(this->context, static_cast<vx_uint32>($width),
                                               static_cast<vx_uint32>($height),
                                               VX_DF_IMAGE_U8);
            this->vxImageOutput = vxCreateImage(this->context, static_cast<vx_uint32>($width),
                                                static_cast<vx_uint32>($height),
                                                VX_DF_IMAGE_S16);

            vx_uint32 value = 4;
            this->accValue = vxCreateScalar(this->context, VX_TYPE_UINT32, &value);
            vxAccumulateSquareImageNode(this->graph, this->vxImageInput, this->accValue, this->vxImageOutput);

            vx_status status = vxVerifyGraph(this->graph);
            if (status == VX_SUCCESS) {
                status = vxProcessGraph(this->graph);
                if (status != VX_SUCCESS) {
                    std::cout << "process failed: " << status << std::endl;
                }
            } else {
                std::cout << "graph verify failed: " << status << std::endl;
            }

            return status;
        }

        vx_context context = nullptr;
        vx_graph graph = nullptr;
        vx_image vxImageInput = nullptr;
        vx_image vxImageOutput = nullptr;
        vx_scalar accValue = nullptr;
    };

    TEST_F(DisplayTest, DISABLED_Show_Image) {
        while (true) {
            ASSERT_EQ(VX_SUCCESS, this->create(640, 480));
            Image::Load("test/resource/data/Io/VisionSDK/Studio/Libs/Media/bikegray_640x480.png",
                        this->context, this->vxImageInput);

            vx_status status = vxGetStatus((vx_reference)this->graph);
            if (status == VX_SUCCESS) {
                status = vxProcessGraph(this->graph);
                if (status != VX_SUCCESS) {
                    std::cout << "process failed: " << status << std::endl;
                }
            } else {
                std::cout << "check failed: " << status << std::endl;
            }

            Display::Show(this->context, this->vxImageInput, "Input");
            Display::Show(this->context, this->vxImageOutput, "Output");
        }
        Display::WaitForAllClosed();
    }
}
