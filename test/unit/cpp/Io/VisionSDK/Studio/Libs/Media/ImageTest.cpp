/* ********************************************************************************************************* *
*
* Copyright (c) 2017-2019 NXP
*
* SPDX-License-Identifier: BSD-3-Clause
* The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
* or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
*
* ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Io::VisionSDK::Studio::Libs::Media {

    class ImageTest : public testing::Test {
     protected:
        void SetUp() override {
            vx_status status;

            this->context = vxCreateContext();

            this->graph = vxCreateGraph(this->context);
            status = vxGetStatus((vx_reference)this->graph);
            if (status != vx_status_e::VX_SUCCESS) {
                GTEST_FATAL_FAILURE_(": Testing VxGraph can not be initialized.");
            }
        }

        void TearDown() override {
            vxReleaseGraph(&this->graph);
            vxReleaseContext(&this->context);
        }

        void CompareImages(const string &$path1, const string &$path2) {
            cv::Mat img1 = cv::imread($path1, CV_LOAD_IMAGE_UNCHANGED);
            cv::Mat img2 = cv::imread($path2, CV_LOAD_IMAGE_UNCHANGED);

            return CompareImages(img1, img2);
        }

        void CompareImages(const cv::Mat &$img1, const cv::Mat &$img2) {
            ASSERT_NE($img1.data, nullptr);
            ASSERT_NE($img2.data, nullptr);

            ASSERT_EQ($img1.channels(), $img2.channels());
            ASSERT_EQ($img1.rows, $img2.rows);
            ASSERT_EQ($img1.cols, $img2.cols);
            ASSERT_EQ($img1.depth(), $img2.depth());

            cv::Mat planes1[$img1.channels()];  // NOLINT(runtime/arrays)
            cv::Mat planes2[$img2.channels()];  // NOLINT(runtime/arrays)
            cv::split($img1, planes1);  // NOLINT
            cv::split($img2, planes2);  // NOLINT

            cv::Mat tmpOut;
            for (int i = 0; i < $img1.channels(); i++) {
                cv::subtract(planes1[i], planes2[i], tmpOut);
                ASSERT_EQ(cv::countNonZero(tmpOut), 0);
            }
        }

     public:
        vx_context context = nullptr;
        vx_graph graph = nullptr;
    };

    TEST_F(ImageTest, LoadSaveRGB) {
        string inputPath = "test/resource/data/Io/VisionSDK/Studio/Libs/Media/speedLimit.png";
        string outputPath = "test/resource/data/Io/VisionSDK/Studio/Libs/Media/testSpeedLimit.png";

        Image inputImage(inputPath, this->context);
        Image outputImage(outputPath, this->context);

        vx_image image = nullptr;
        inputImage.getFrame(image);
        vx_df_image type;
        ASSERT_EQ(vxQueryImage(image, vx_image_attribute_e::VX_IMAGE_FORMAT, &type, sizeof(type)), vx_status_e::VX_SUCCESS);
        ASSERT_EQ(type, vx_df_image_e::VX_DF_IMAGE_RGB);

        outputImage.setFrame(image);

        this->CompareImages(inputPath, outputPath);
        vxReleaseImage(&image);
    }

    TEST_F(ImageTest, LoadSaveGray) {
        string inputPath = "test/resource/data/Io/VisionSDK/Studio/Libs/Media/speedLimitGray.png";
        string outputPath = "test/resource/data/Io/VisionSDK/Studio/Libs/Media/testSpeedLimitGray.png";

        Image inputImage(inputPath, this->context);
        Image outputImage(outputPath, this->context);

        vx_image image = nullptr;
        inputImage.getFrame(image);

        vx_df_image type;
        ASSERT_EQ(vxQueryImage(image, vx_image_attribute_e::VX_IMAGE_FORMAT, &type, sizeof(type)), vx_status_e::VX_SUCCESS);
        ASSERT_EQ(type, vx_df_image_e::VX_DF_IMAGE_U8);

        outputImage.setFrame(image);

        this->CompareImages(inputPath, outputPath);
        vxReleaseImage(&image);
    }

    TEST_F(ImageTest, LoadProcessSaveGray) {
        string inputPath = "test/resource/data/Io/VisionSDK/Studio/Libs/Media/speedLimitGray.png";
        string outputPath = "test/resource/data/Io/VisionSDK/Studio/Libs/Media/testProcessSpeedLimitGray.png";

        Image inputImage(inputPath, this->context);
        Image outputImage(outputPath, this->context);

        vx_image image = nullptr;
        inputImage.getFrame(image);

        vx_df_image type;
        ASSERT_EQ(vxQueryImage(image, vx_image_attribute_e::VX_IMAGE_FORMAT, &type, sizeof(type)), vx_status_e::VX_SUCCESS);
        ASSERT_EQ(type, vx_df_image_e::VX_DF_IMAGE_U8);

        vx_image out = vxCreateImage(this->context, 310, 310, vx_df_image_e::VX_DF_IMAGE_U8);
        vxNotNode(this->graph, image, out);

        ASSERT_EQ(vxVerifyGraph(this->graph), vx_status_e::VX_SUCCESS);
        ASSERT_EQ(vxProcessGraph(this->graph), vx_status_e::VX_SUCCESS);

        outputImage.setFrame(out);

        cv::Mat tmp = cv::imread(inputPath, CV_LOAD_IMAGE_UNCHANGED);
        cv::Mat origin(tmp.rows, tmp.cols, tmp.type());
        cv::bitwise_not(tmp, origin);

        cv::Mat vxTmp = cv::imread(outputPath, CV_LOAD_IMAGE_UNCHANGED);

        // uncomment to show outputs
        // Display::Show(vxTmp, "OpenVX processed speedLimitGray.png");
        // Display::Show(origin, "OpenCV processed speedLimitGray.png");
        // Display::WaitForAllClosed();

        this->CompareImages(vxTmp, origin);
        vxReleaseImage(&image);
        vxReleaseImage(&out);
    }

    TEST_F(ImageTest, LoadCopyProcessSaveGray) {
        string inputPath = "test/resource/data/Io/VisionSDK/Studio/Libs/Media/speedLimitGray.png";
        string outputPath = "test/resource/data/Io/VisionSDK/Studio/Libs/Media/testProcessSpeedLimitGray.png";

        Image inputImage(inputPath, this->context);
        Image outputImage(outputPath, this->context);

        vx_image image = nullptr;
        inputImage.getFrame(image);

        vx_df_image type;
        ASSERT_EQ(vxQueryImage(image, vx_image_attribute_e::VX_IMAGE_FORMAT, &type, sizeof(type)), vx_status_e::VX_SUCCESS);
        ASSERT_EQ(type, vx_df_image_e::VX_DF_IMAGE_U8);

        vx_image out = vxCreateImage(this->context, 310, 310, vx_df_image_e::VX_DF_IMAGE_U8);
        vxNotNode(this->graph, image, out);

        ASSERT_EQ(vxVerifyGraph(this->graph), vx_status_e::VX_SUCCESS);
        ASSERT_EQ(vxProcessGraph(this->graph), vx_status_e::VX_SUCCESS);

        outputImage.setFrame(out);

        cv::Mat tmp = cv::imread(inputPath, CV_LOAD_IMAGE_UNCHANGED);
        cv::Mat origin(tmp.rows, tmp.cols, tmp.type());
        cv::bitwise_not(tmp, origin);

        cv::Mat vxTmp = cv::imread(outputPath, CV_LOAD_IMAGE_UNCHANGED);

        // uncomment to show outputs
        // Display::Show(vxTmp, "OpenVX processed speedLimitGray.png");
        // Display::Show(origin, "OpenCV processed speedLimitGray.png");
        // Display::WaitForAllClosed();

        this->CompareImages(vxTmp, origin);
        vxReleaseImage(&image);
        vxReleaseImage(&out);
    }
}
