/* ********************************************************************************************************* *
*
* Copyright (c) 2018-2019 NXP
*
* SPDX-License-Identifier: BSD-3-Clause
* The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
* or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
*
* ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Io::VisionSDK::Studio::Libs::Media {

    class MockImage : public Image {
     public:
        MockImage(const string &$path, const vx_context &$context) : Image($path, $context) {}

        const cv::Mat &getCvMat() {
            return this->image;
        }
    };

    class ImageFormatTest : public testing::TestWithParam<std::pair<string, vx_df_image_e>> {
     protected:
        void SetUp() override {
            vx_status status;

            this->context = vxCreateContext();

            this->graph = vxCreateGraph(this->context);
            status = vxGetStatus((vx_reference)this->graph);
            if (status != vx_status_e::VX_SUCCESS) {
                GTEST_FATAL_FAILURE_(": Testing VxGraph can not be initialized.");
            }
        }

        void TearDown() override {
            vxReleaseGraph(&this->graph);
            vxReleaseContext(&this->context);
        }

        void CompareImages(const string &$path1, const string &$path2) {
            cv::Mat img1 = cv::imread($path1, CV_LOAD_IMAGE_UNCHANGED);
            cv::Mat img2 = cv::imread($path2, CV_LOAD_IMAGE_UNCHANGED);

            return CompareImages(img1, img2);
        }

        void CompareImages(const cv::Mat &$img1, const cv::Mat &$img2) {
            ASSERT_NE($img1.data, nullptr);
            ASSERT_NE($img2.data, nullptr);

            ASSERT_EQ($img1.channels(), $img2.channels());
            ASSERT_EQ($img1.rows, $img2.rows);
            ASSERT_EQ($img1.cols, $img2.cols);
            ASSERT_EQ($img1.depth(), $img2.depth());

            cv::Mat planes1[$img1.channels()];  // NOLINT(runtime/arrays)
            cv::Mat planes2[$img2.channels()];  // NOLINT(runtime/arrays)
            cv::split($img1, planes1);  // NOLINT
            cv::split($img2, planes2);  // NOLINT

            cv::Mat tmpOut;
            for (int i = 0; i < $img1.channels(); i++) {
                cv::subtract(planes1[i], planes2[i], tmpOut);
                ASSERT_EQ(cv::countNonZero(tmpOut), 0);
            }
        }

     public:
        vx_context context = nullptr;
        vx_graph graph = nullptr;
    };

    TEST_P(ImageFormatTest, LoadSaveImages) {
        string pathPrefix = "test/resource/data/Io/VisionSDK/Studio/Libs/Media/Images/";

        MockImage image(pathPrefix + GetParam().first, this->context);

        vx_image img;
        image.getFrame(img);

        vx_df_image type;
        ASSERT_EQ(vxQueryImage(img, vx_image_attribute_e::VX_IMAGE_FORMAT, &type, sizeof(type)), vx_status_e::VX_SUCCESS);
        char imgTypeName[] = {static_cast<char>(type), static_cast<char>(type >> 8), static_cast<char>(type >> 16),
                              static_cast<char>(type >> 24), '\0'};
        ASSERT_EQ(type, GetParam().second) << "type is: " << imgTypeName;

        MockImage outputImage(pathPrefix + "test" + GetParam().first, this->context);
        outputImage.setFrame(img);

        this->CompareImages(image.getCvMat(), outputImage.getCvMat());
        vxReleaseImage(&img);
    }

    INSTANTIATE_TEST_CASE_P(
            Format,
            ImageFormatTest,
            testing::Values(std::pair<string, vx_df_image_e>{"Sample_01.png", vx_df_image_e::VX_DF_IMAGE_RGBX},
                            std::pair<string, vx_df_image_e>{"Sample_02.png", vx_df_image_e::VX_DF_IMAGE_RGB},
                            std::pair<string, vx_df_image_e>{"Sample_03.bmp", vx_df_image_e::VX_DF_IMAGE_RGB},
                            std::pair<string, vx_df_image_e>{"Sample_05.jpg", vx_df_image_e::VX_DF_IMAGE_RGB},
                            std::pair<string, vx_df_image_e>{"Sample_06.png", vx_df_image_e::VX_DF_IMAGE_RGB},
                            std::pair<string, vx_df_image_e>{"Sample_07.tif", vx_df_image_e::VX_DF_IMAGE_RGBX},
//                            std::pair<string, vx_df_image_e>{"Sample_09.jpg", vx_df_image_e::VX_DF_IMAGE_RGB},
                            std::pair<string, vx_df_image_e>{"Sample_10.png", vx_df_image_e::VX_DF_IMAGE_RGBX},
                            std::pair<string, vx_df_image_e>{"Sample_11.png", vx_df_image_e::VX_DF_IMAGE_RGBX},
                            std::pair<string, vx_df_image_e>{"Sample_12.png", vx_df_image_e::VX_DF_IMAGE_RGBX},
                            std::pair<string, vx_df_image_e>{"Sample_13.png", vx_df_image_e::VX_DF_IMAGE_U8},
                            std::pair<string, vx_df_image_e>{"Sample_14.png", vx_df_image_e::VX_DF_IMAGE_U8}));
}
