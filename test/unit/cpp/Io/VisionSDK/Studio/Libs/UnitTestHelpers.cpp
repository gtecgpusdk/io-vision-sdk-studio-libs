/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "UnitTestHelpers.hpp"

testing::AssertionResult AssertJson(const char *$actualExpr, const char *$expectedExpr, const json &$actual, const json &$expected) {
    // compare as string to reduce float rounding issues (for dump float is rounded to 10 digits)
    if ($actual.dump() == $expected.dump()) {
        return testing::AssertionSuccess();
    }
    testing::Message msg;
    msg << $actualExpr << std::endl << "Which is: " << $actual << std::endl << $expectedExpr << std::endl << "Which is: " << $expected;
    return testing::AssertionFailure(msg);
}
