/* ********************************************************************************************************* *
*
* Copyright (c) 2018-2019 NXP
*
* SPDX-License-Identifier: BSD-3-Clause
* The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
* or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
*
* ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

#include "opencv2/video/tracking.hpp"
#include "opencv2/features2d.hpp"

namespace Io::VisionSDK::Studio::Libs::Validation {
    using Io::VisionSDK::Studio::Libs::Pattern;
    using Io::VisionSDK::Studio::Libs::Utils::Convert;

    void VX_CALLBACK logHandlerComplexNodes(vx_context $context, vx_reference $ref, vx_status $status, const vx_char $string[]) {
        std::cout << "log: " << $status << "; " << $string << std::endl;
    }

    class VxComplexNodesTest : public testing::Test {
     public:
        vx_context context{};
        vx_graph graph{};

        void Run() {
            vx_status status = vxVerifyGraph(this->graph);
            if (status == vx_status_e::VX_SUCCESS) {
                status = vxProcessGraph(this->graph);
                if (status != vx_status_e::VX_SUCCESS) {
                    GTEST_FATAL_FAILURE_(("Graph process failed:" + std::to_string(status)).c_str());
                }
            } else {
                GTEST_FATAL_FAILURE_(("Graph verify failed:" + std::to_string(status)).c_str());
            }
        }

     protected:
        void SetUp() override {
            vx_status status;

            this->context = vxCreateContext();
            vxRegisterLogCallback(this->context, logHandlerComplexNodes, _vx_bool_e::vx_false_e);

            this->graph = vxCreateGraph(this->context);
            status = vxGetStatus((vx_reference)this->graph);
            if (status != vx_status_e::VX_SUCCESS) {
                GTEST_FATAL_FAILURE_("Testing VxGraph can not be initialized.");
            }
        }

        void TearDown() override {
            vxReleaseGraph(&this->graph);
            vxReleaseContext(&this->context);
        }
    };

    template<typename InType, typename OutType>
    void Convert(const Pattern<InType> &$in, Pattern<OutType> &$out, std::function<OutType(InType)> $converter) {
        if ($in.getWidth() * $in.getHeight() != $out.getHeight() * $out.getWidth()) {
            std::cout << "Input Output dimensions differ." << std::endl;
            return;
        }

        auto inData = reinterpret_cast<InType *>($in.getMat().data);
        auto outData = reinterpret_cast<OutType *>($out.getMat().data);

        for (int i = 0; i < $in.getMat().rows * $in.getMat().cols; i++) {
            outData[i] = $converter(inData[i]);
        }
    }

#if VX_VERSION == VX_VERSION_1_2
    class FCParams {
     public:
        explicit FCParams(vx_context $context, const std::vector<vx_size> &$wDims, const std::vector<vx_size> &$bDims) {
            this->weightDims = new vx_size[$wDims.size()];
            std::copy($wDims.begin(), $wDims.end(), this->weightDims);

            this->biasDims = new vx_size[$bDims.size()];
            std::copy($bDims.begin(), $bDims.end(), this->biasDims);

            this->weights = vxCreateTensor($context, $wDims.size(), this->weightDims, VX_TYPE_INT16, 8);
            this->bias = vxCreateTensor($context, $bDims.size(), this->biasDims, VX_TYPE_INT16, 8);
        }

        vx_tensor getWeight() const { return this->weights; }

        vx_tensor getBias() const { return this->bias; }

        ~FCParams() {
            vxReleaseTensor(&this->weights);
            vxReleaseTensor(&this->bias);
            delete[] this->weightDims;
            delete[] this->biasDims;
        }

     private:
        vx_tensor weights;
        vx_tensor bias;
        vx_size *weightDims;
        vx_size *biasDims;
    };

    class TensorLayer {
     public:
        explicit TensorLayer(vx_context $context, const std::vector<vx_size> &$dims) {
            this->dims = new vx_size[$dims.size()];
            std::copy($dims.begin(), $dims.end(), this->dims);
            this->tensor = vxCreateTensor($context, $dims.size(), this->dims, VX_TYPE_INT16, 8);
        }

        vx_tensor getTensor() const { return this->tensor; }

        ~TensorLayer() {
            vxReleaseTensor(&this->tensor);
            delete[] this->dims;
        }

     private:
        vx_size *dims;
        vx_tensor tensor;
    };

    vx_status CopyToFromTensor(vx_tensor $tensor, int16_t *$buffer,
                               ssize_t $bufSize, vx_accessor_e $accesor) {
        vx_status status = VX_SUCCESS;
        vx_size numDims;
        status = vxQueryTensor($tensor, VX_TENSOR_NUMBER_OF_DIMS, &numDims, sizeof(numDims));
        vx_type_e data_type;
        status |= vxQueryTensor($tensor, VX_TENSOR_DATA_TYPE, &data_type, sizeof(data_type));
        vx_uint8 fixed_point_pos = 0;
        status |= vxQueryTensor($tensor, VX_TENSOR_FIXED_POINT_POSITION, &fixed_point_pos, sizeof(fixed_point_pos));

        if ((data_type == VX_TYPE_INT16) && (fixed_point_pos == 8)) {
            vx_size elementSize = 2;
            std::vector<vx_size> dims;
            dims.resize(numDims);        // vx_size dims[numDims];
            std::vector<vx_size> start;
            start.resize(numDims);      // vx_size start[numDims];
            std::vector<vx_size> strides;
            strides.resize(numDims);  // vx_size strides[numDims];
            status |= vxQueryTensor($tensor, VX_TENSOR_DIMS, &dims[0], dims.size() * sizeof(vx_size));
            for (vx_size i = 0; i < numDims; i++) {
                start[i] = 0;
                if (i == 0)
                    strides[i] = elementSize;
                else
                    strides[i] = strides[i - 1] * dims[i - 1];
            }
            if (($bufSize == -1)
                || ($bufSize == static_cast<ssize_t>(strides[numDims - 1] * dims[numDims - 1]))) {
                status = vxCopyTensorPatch($tensor, numDims, &start[0], &dims[0], &strides[0], $buffer, $accesor, VX_MEMORY_TYPE_HOST);
                $bufSize = strides[numDims - 1] * dims[numDims - 1] / elementSize;
            } else {
                status = VX_FAILURE;
            }
        }
        return status;
    }

    TEST_F(VxComplexNodesTest, testVxNN) {
        Pattern<float> weightData1{{-2.2333994f, -0.6585823f, 2.6329818f, 3.1697698f,
                                           3.302886f, -0.5718869f, 2.7338078f, -1.9193857f}};
        Pattern<float> biasData1{{0.82130927f,
                                         0.9078386f,
                                         -0.64589936f,
                                         0.58110344f}};

        Pattern<float> weightData2{{-4.4117165f, 1.24535f, 3.8559446f, -4.2980924f}};
        Pattern<float> biasData2{{0.7974343f}};

        Pattern<int16_t> weightData1Q78(4, 2);
        Pattern<int16_t> biasData1Q78(1, 4);

        Pattern<int16_t> weightData2Q78(4, 1);
        Pattern<int16_t> biasData2Q78(1, 1);

        const auto floatToQ78ConvertFunction = [](float $val) -> int16_t {
            float r = $val < 0.0f ? -0.5f : 0.5f;
            int tmpValue = static_cast<int>(($val * 256.0 + r));
            int16_t value = tmpValue > SHRT_MAX ? SHRT_MAX : (tmpValue < SHRT_MIN ? SHRT_MIN : (int16_t)tmpValue);
            int16_t res;
            memcpy(&res, &value, sizeof(int16_t));
            return res;
        };

        const auto q78ToFloatConvertFunction = [](int16_t $val) -> float {
            return (static_cast<float>($val)) / 256.0f;
        };

        Convert<float, int16_t>(weightData1, weightData1Q78, floatToQ78ConvertFunction);
        Convert<float, int16_t>(biasData1, biasData1Q78, floatToQ78ConvertFunction);

        Convert<float, int16_t>(weightData2, weightData2Q78, floatToQ78ConvertFunction);
        Convert<float, int16_t>(biasData2, biasData2Q78, floatToQ78ConvertFunction);

        FCParams p1(this->context, {2, 4}, {4});
        CopyToFromTensor(p1.getWeight(), reinterpret_cast<int16_t *>(weightData1Q78.getMat().data), -1, VX_WRITE_ONLY);
        CopyToFromTensor(p1.getBias(), reinterpret_cast<int16_t *>(biasData1Q78.getMat().data), -1, VX_WRITE_ONLY);

        FCParams p2(this->context, {4, 1}, {1});
        CopyToFromTensor(p2.getWeight(), reinterpret_cast<int16_t *>(weightData2Q78.getMat().data), -1, VX_WRITE_ONLY);
        CopyToFromTensor(p2.getBias(), reinterpret_cast<int16_t *>(biasData2Q78.getMat().data), -1, VX_WRITE_ONLY);

        TensorLayer inTensor(this->context, {2});
        TensorLayer l2(this->context, {4});
        TensorLayer l3(this->context, {4});
        TensorLayer l4(this->context, {1});
        TensorLayer outTensor(this->context, {1});

        std::vector<vx_node> nodes;
        nodes.emplace_back(
                vxFullyConnectedLayer(this->graph, inTensor.getTensor(), p1.getWeight(), p1.getBias(), VX_CONVERT_POLICY_WRAP,
                                      VX_ROUND_POLICY_TO_NEAREST_EVEN,
                                      l2.getTensor()));

        nodes.emplace_back(
                vxActivationLayer(this->graph, l2.getTensor(), VX_NN_ACTIVATION_HYPERBOLIC_TAN, 1.0f, 1.0f, l3.getTensor()));

        nodes.emplace_back(
                vxFullyConnectedLayer(this->graph, l3.getTensor(), p2.getWeight(), p2.getBias(), VX_CONVERT_POLICY_WRAP,
                                      VX_ROUND_POLICY_TO_ZERO,
                                      l4.getTensor()));

        nodes.emplace_back(
                vxActivationLayer(this->graph, l4.getTensor(), VX_NN_ACTIVATION_LOGISTIC, 1.0f, 1.0f, outTensor.getTensor()));

        const auto &c = floatToQ78ConvertFunction;
        int16_t inData[4][2] = {{c(0.0f), c(0.0f)},
                                {c(0.0f), c(1.0f)},
                                {c(1.0f), c(0.0f)},
                                {c(1.0f), c(1.0f)}};
        const float outData[4] = {0.0f,
                                  1.0f,
                                  1.0f,
                                  1.0f};   // the last result should be 0 but is 1 -> somewhere there is an error

        for (int i = 0; i < 4; i++) {
            CopyToFromTensor(inTensor.getTensor(), inData[i], -1, VX_WRITE_ONLY);
            this->Run();
            int16_t outValueQ78;
            CopyToFromTensor(outTensor.getTensor(), &outValueQ78, -1, VX_READ_ONLY);
            ASSERT_NEAR(q78ToFloatConvertFunction(outValueQ78), outData[i], 0.3);
        }

        for (vx_node node : nodes) {
            vxReleaseNode(&node);
        }
    }
#endif
}
