/* ********************************************************************************************************* *
*
* Copyright (c) 2019 NXP
*
* SPDX-License-Identifier: BSD-3-Clause
* The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
* or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
*
* ********************************************************************************************************* */

#include <utility>
#include <fstream>
#include "../sourceFilesMap.hpp"

namespace Io::VisionSDK::Studio::Libs::Validation {
    using Io::VisionSDK::Studio::Libs::Primitives::BaseGraph;
    using Io::VisionSDK::Studio::Libs::Primitives::BaseContext;
    using Io::VisionSDK::Studio::Libs::Primitives::BaseVisualGraph;
    using Io::VisionSDK::Studio::Libs::Utils::StopWatch;
    using Io::VisionSDK::Studio::Libs::Media::Image;
    using Io::VisionSDK::Studio::Libs::Media::File;
    using Io::VisionSDK::Studio::Libs::Interfaces::IIOCom;

    class VxNNTest : public testing::Test {
     public:
        static const char *getTestRoot() { return "test/resource/data/Io/VisionSDK/Studio/Libs/Validation"; }

     protected:
        void SetUp() override {
        }

        void TearDown() override {
        }
    };

#if VX_VERSION == VX_VERSION_1_2

    class TestGraphNN : public BaseGraph {
     public:
        explicit TestGraphNN(BaseContext *context)
                : BaseGraph(context) {}

     protected:
        vx_status create() override {
            vx_status status = vxGetStatus((vx_reference)this->getVxGraph());
            if (status == VX_SUCCESS) {
                vx_size vxData0Dims[] = {2};
                this->vxDataMap["vxData0"] = (vx_reference)vxCreateTensor(this->getParent()->getVxContext(), 1, vxData0Dims,
                                                                          VX_TYPE_INT16, 8);
                status = this->getParent()->Check(this->vxDataMap["vxData0"]);
            }
            if (status == VX_SUCCESS) {
                vx_size vxData1Dims[] = {2, 4};
                this->vxDataMap["vxData1"] = (vx_reference)vxCreateTensor(this->getParent()->getVxContext(), 2, vxData1Dims,
                                                                          VX_TYPE_INT16, 8);
                status = this->getParent()->Check(this->vxDataMap["vxData1"]);
            }
            if (status == VX_SUCCESS) {
                vx_size vxData2Dims[] = {4};
                this->vxDataMap["vxData2"] = (vx_reference)vxCreateTensor(this->getParent()->getVxContext(), 1, vxData2Dims,
                                                                          VX_TYPE_INT16, 8);
                status = this->getParent()->Check(this->vxDataMap["vxData2"]);
            }
            if (status == VX_SUCCESS) {
                vx_size vxData3Dims[] = {4, 1};
                this->vxDataMap["vxData3"] = (vx_reference)vxCreateTensor(this->getParent()->getVxContext(), 2, vxData3Dims,
                                                                          VX_TYPE_INT16, 8);
                status = this->getParent()->Check(this->vxDataMap["vxData3"]);
            }
            if (status == VX_SUCCESS) {
                vx_size vxData4Dims[] = {1};
                this->vxDataMap["vxData4"] = (vx_reference)vxCreateTensor(this->getParent()->getVxContext(), 1, vxData4Dims,
                                                                          VX_TYPE_INT16, 8);
                status = this->getParent()->Check(this->vxDataMap["vxData4"]);
            }
            if (status == VX_SUCCESS) {
                vx_size vxData5Dims[] = {1};
                this->vxDataMap["vxData5"] = (vx_reference)vxCreateTensor(this->getParent()->getVxContext(), 1, vxData5Dims,
                                                                          VX_TYPE_INT16, 8);
                status = this->getParent()->Check(this->vxDataMap["vxData5"]);
            }
            if (status == VX_SUCCESS) {
                vx_size vxData6Dims[] = {4};
                this->vxDataMap["vxData6"] = (vx_reference)vxCreateTensor(this->getParent()->getVxContext(), 1, vxData6Dims,
                                                                          VX_TYPE_INT16, 8);
                status = this->getParent()->Check(this->vxDataMap["vxData6"]);
            }
            if (status == VX_SUCCESS) {
                vx_size vxData7Dims[] = {4};
                this->vxDataMap["vxData7"] = (vx_reference)vxCreateTensor(this->getParent()->getVxContext(), 1, vxData7Dims,
                                                                          VX_TYPE_INT16, 8);
                status = this->getParent()->Check(this->vxDataMap["vxData7"]);
            }
            if (status == VX_SUCCESS) {
                vx_size vxData8Dims[] = {1};
                this->vxDataMap["vxData8"] = (vx_reference)vxCreateTensor(this->getParent()->getVxContext(), 1, vxData8Dims,
                                                                          VX_TYPE_INT16, 8);
                status = this->getParent()->Check(this->vxDataMap["vxData8"]);
            }
            if (status == VX_SUCCESS) {
                this->vxNodesMap["vxNode0"] = (vx_reference)vxFullyConnectedLayer(this->getVxGraph(), (vx_tensor)this->vxDataMap["vxData0"],
                                                                                  (vx_tensor)this->vxDataMap["vxData1"],
                                                                                  (vx_tensor)this->vxDataMap["vxData2"],
                                                                                  VX_CONVERT_POLICY_WRAP, VX_ROUND_POLICY_TO_NEAREST_EVEN,
                                                                                  (vx_tensor)this->vxDataMap["vxData6"]);
                status = this->getParent()->Check(this->vxNodesMap["vxNode0"]);
            }
            if (status == VX_SUCCESS) {
                this->vxNodesMap["vxNode1"] = (vx_reference)vxActivationLayer(this->getVxGraph(), (vx_tensor)this->vxDataMap["vxData6"],
                                                                              VX_NN_ACTIVATION_HYPERBOLIC_TAN, 1.0f, 1.0f,
                                                                              (vx_tensor)this->vxDataMap["vxData7"]);
                status = this->getParent()->Check(this->vxNodesMap["vxNode1"]);
            }
            if (status == VX_SUCCESS) {
                this->vxNodesMap["vxNode2"] = (vx_reference)vxFullyConnectedLayer(this->getVxGraph(), (vx_tensor)this->vxDataMap["vxData7"],
                                                                                  (vx_tensor)this->vxDataMap["vxData3"],
                                                                                  (vx_tensor)this->vxDataMap["vxData4"],
                                                                                  VX_CONVERT_POLICY_WRAP, VX_ROUND_POLICY_TO_NEAREST_EVEN,
                                                                                  (vx_tensor)this->vxDataMap["vxData8"]);
                status = this->getParent()->Check(this->vxNodesMap["vxNode2"]);
            }
            if (status == VX_SUCCESS) {
                this->vxNodesMap["vxNode3"] = (vx_reference)vxActivationLayer(this->getVxGraph(), (vx_tensor)this->vxDataMap["vxData8"],
                                                                              VX_NN_ACTIVATION_LOGISTIC, 1.0f, 1.0f,
                                                                              (vx_tensor)this->vxDataMap["vxData5"]);
                status = this->getParent()->Check(this->vxNodesMap["vxNode3"]);
            }
            return status;
        }

        vx_status process(const std::function<vx_status(int)> &$handler) override {
            vx_status status = vxGetStatus((vx_reference)this->getVxGraph());
            if (status == VX_SUCCESS) {
                status = BaseGraph::process([&](int $iteration) -> vx_status {
                    if (status == VX_SUCCESS && !this->getParent()->getParent()->IsNoFileIo()) {
                        auto ioCom0 = this->getParent()->getParent()->getIoCom("ioCom0");
                        if ($iteration == 0) {
                            auto vxData0Ref = this->getData("vxData0");
                            status = this->getParent()->Check(ioCom0->getData(vxData0Ref, true));
                        }
                    }
                    if (status == VX_SUCCESS && !this->getParent()->getParent()->IsNoFileIo()) {
                        auto ioCom1 = this->getParent()->getParent()->getIoCom("ioCom1");
                        if ($iteration == 0) {
                            auto vxData1Ref = this->getData("vxData1");
                            status = this->getParent()->Check(ioCom1->getData(vxData1Ref, true));
                        }
                    }
                    if (status == VX_SUCCESS && !this->getParent()->getParent()->IsNoFileIo()) {
                        auto ioCom2 = this->getParent()->getParent()->getIoCom("ioCom2");
                        if ($iteration == 0) {
                            auto vxData2Ref = this->getData("vxData2");
                            status = this->getParent()->Check(ioCom2->getData(vxData2Ref, true));
                        }
                    }
                    if (status == VX_SUCCESS && !this->getParent()->getParent()->IsNoFileIo()) {
                        auto ioCom3 = this->getParent()->getParent()->getIoCom("ioCom3");
                        if ($iteration == 0) {
                            auto vxData3Ref = this->getData("vxData3");
                            status = this->getParent()->Check(ioCom3->getData(vxData3Ref, true));
                        }
                    }
                    if (status == VX_SUCCESS && !this->getParent()->getParent()->IsNoFileIo()) {
                        auto ioCom4 = this->getParent()->getParent()->getIoCom("ioCom4");
                        if ($iteration == 0) {
                            auto vxData4Ref = this->getData("vxData4");
                            status = this->getParent()->Check(ioCom4->getData(vxData4Ref, true));
                        }
                    }
                    if (status == VX_SUCCESS) {
                        status = this->getParent()->Check(vxProcessGraph(this->getVxGraph()));
                    }
                    if (status == VX_SUCCESS && !this->getParent()->getParent()->IsNoFileIo()) {
                        auto ioCom5 = this->getParent()->getParent()->getIoCom("ioCom5");
                        auto vxData5Ref = this->getData("vxData5");
                        status = this->getParent()->Check(ioCom5->setData(vxData5Ref));
                    }
                    return status;
                });
            }
            return status;
        }
    };

    class TestContextNN : public BaseContext {
     public:
        explicit TestContextNN(BaseVisualGraph *parent)
                : BaseContext(parent) {
            this->graphsMap["TestGraphNN"] = new TestGraphNN(this);
        }

     protected:
        vx_status process(const std::function<vx_status(void)> &$handler) override {
            return BaseContext::process([&]() -> vx_status {
                vx_status status = vxGetStatus((vx_reference)this->getVxContext());
                auto graph0 = this->getGraph("TestGraphNN");
                if (status == VX_SUCCESS && graph0 != nullptr) {
                    status = const_cast<BaseGraph *>(graph0)->Process();
                    if (status != VX_SUCCESS) {
                        const_cast<TestContextNN *>(this)->removeGraph("TestGraphNN");
                    }
                } else {
                    status = VX_FAILURE;
                }
                return status;
            });
        }
    };

    class VisualGraphNN : public BaseVisualGraph {
     public:
        std::map<string, shared_ptr<IIOCom>> *getIoComMap() {
            return &this->ioComMap;
        }

     protected:
        vx_status create() override {
            this->contextsMap["TestContextNN"] = new TestContextNN(this);
            return vx_status_e::VX_SUCCESS;
        }

        vx_status process(const std::function<vx_status(int)> &$handler) override {
            return BaseVisualGraph::process([&](int $iterator) -> vx_status {
                vx_status status = VX_FAILURE;
                BaseContext *context0 = const_cast<BaseContext *>(this->getContext("TestContextNN"));
                if (context0 != nullptr) {
                    status = VX_SUCCESS;
                }
                BaseGraph *graph_0_0 = nullptr;
                if (status == VX_SUCCESS) {
                    graph_0_0 = const_cast<BaseGraph *>(context0->getGraph("TestGraphNN"));
                    if (graph_0_0 != nullptr) {
                        status = VX_SUCCESS;
                    } else {
                        status = VX_FAILURE;
                    }
                }
                if ((status == VX_SUCCESS) && this->firstRun) {
                    this->firstRun = false;
                    this->ioComMap.emplace("ioCom0", std::make_shared<File>(
                            string(VxNNTest::getTestRoot()) + "/xorNNInput_0.txt"));
                    this->ioComMap.emplace("ioCom1", std::make_shared<File>(
                            string(VxNNTest::getTestRoot()) + "/vxWeight0Data.txt"));
                    this->ioComMap.emplace("ioCom2", std::make_shared<File>(
                            string(VxNNTest::getTestRoot()) + "/vxBias0Data.txt"));
                    this->ioComMap.emplace("ioCom3", std::make_shared<File>(
                            string(VxNNTest::getTestRoot()) + "/vxWeight1Data.txt"));
                    this->ioComMap.emplace("ioCom4", std::make_shared<File>(
                            string(VxNNTest::getTestRoot()) + "/vxBias1Data.txt"));
                    this->ioComMap.emplace("ioCom5", std::make_shared<File>(string(VxNNTest::getTestRoot()) + "/xorNNOutput_0.txt"));
                }
                if (status == VX_SUCCESS) {
                    status = context0->Process();
                }
                return status;
            });
        }

     private:
        bool firstRun = true;
    };

    TEST_F(VxNNTest, XorNN) {
        auto getData = [](const string &$path) -> string {
            std::ifstream ifstream($path);
            return string((std::istreambuf_iterator<char>(ifstream)), std::istreambuf_iterator<char>());
        };

        string output = string(VxNNTest::getTestRoot()) + "/xorNNOutput_0.txt";
        VisualGraphNN visualGraph{};
        ASSERT_EQ(visualGraph.Create(), vx_status_e::VX_SUCCESS);
        ASSERT_EQ(visualGraph.Validate(), vx_status_e::VX_SUCCESS);

        ASSERT_EQ(visualGraph.Process(), vx_status_e::VX_SUCCESS);
        ASSERT_STREQ(getData(output).c_str(), "0.863281");

        std::map<string, shared_ptr<IIOCom>> *ioComMap = visualGraph.getIoComMap();

        output = string(VxNNTest::getTestRoot()) + "/xorNNOutput_1.txt";
        ioComMap->at("ioCom0").reset(new File(string(VxNNTest::getTestRoot()) + "/xorNNInput_1.txt"));
        ioComMap->at("ioCom5").reset(new File(output));
        ASSERT_EQ(visualGraph.Process(), vx_status_e::VX_SUCCESS);
        ASSERT_STREQ(getData(output).c_str(), "0.992188");

        output = string(VxNNTest::getTestRoot()) + "/xorNNOutput_2.txt";
        ioComMap->at("ioCom0").reset(new File(string(VxNNTest::getTestRoot()) + "/xorNNInput_2.txt"));
        ioComMap->at("ioCom5").reset(new File(output));
        ASSERT_EQ(visualGraph.Process(), vx_status_e::VX_SUCCESS);
        ASSERT_STREQ(getData(output).c_str(), "0");

        output = string(VxNNTest::getTestRoot()) + "/xorNNOutput_3.txt";
        ioComMap->at("ioCom0").reset(new File(string(VxNNTest::getTestRoot()) + "/xorNNInput_3.txt"));
        ioComMap->at("ioCom5").reset(new File(output));
        ASSERT_EQ(visualGraph.Process(), vx_status_e::VX_SUCCESS);
        ASSERT_STREQ(getData(output).c_str(), "0.996094");
    }

#endif
}
