/* ********************************************************************************************************* *
*
* Copyright (c) 2019 NXP
*
* SPDX-License-Identifier: BSD-3-Clause
* The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
* or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
*
* ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Io::VisionSDK::Studio::Libs::Validation {
    using Io::VisionSDK::Studio::Libs::Primitives::BaseGraph;
    using Io::VisionSDK::Studio::Libs::Primitives::BaseContext;
    using Io::VisionSDK::Studio::Libs::Primitives::BaseVisualGraph;
    using Io::VisionSDK::Studio::Libs::Utils::StopWatch;
    using Io::VisionSDK::Studio::Libs::Media::Image;

    string testRoot;

    class VisualGraphTest : public testing::Test {
     protected:
        void SetUp() override {
            testRoot = "test/resource/data/Io/VisionSDK/Studio/Libs/Validation";
        }

        void TearDown() override {
        }
    };

    class TestGraph : public BaseGraph {
     public:
        explicit TestGraph(BaseContext *context) : BaseGraph(context) {}

     protected:
        vx_status create() override {
            vx_status status = vxGetStatus((vx_reference)this->getVxGraph());
            if (status == VX_SUCCESS) {
                this->vxDataMap["vxData0"] = this->createImage(this->getParent()->getVxContext(), 640, 480, VX_DF_IMAGE_U8);
                this->getParent()->Check(this->vxDataMap["vxData0"]);
                vx_uint32 vxData1Value = 4;
                this->vxDataMap["vxData1"] = (vx_reference)vxCreateScalar(this->getParent()->getVxContext(), VX_TYPE_UINT32, &vxData1Value);
                this->getParent()->Check(this->vxDataMap["vxData1"]);
                this->vxDataMap["vxData2"] = this->createImage(this->getParent()->getVxContext(),
                                                               BaseGraph::getImageWidth(this->vxDataMap["vxData0"]),
                                                               BaseGraph::getImageHeight(this->vxDataMap["vxData0"]), VX_DF_IMAGE_S16);
                this->getParent()->Check(this->vxDataMap["vxData2"]);
                vx_float32 vxData3Value = 0.500f;
                this->vxDataMap["vxData3"] = (vx_reference)vxCreateScalar(this->getParent()->getVxContext(), VX_TYPE_FLOAT32,
                                                                          &vxData3Value);
                this->getParent()->Check(this->vxDataMap["vxData3"]);
                this->vxDataMap["vxData4"] = this->createImage(this->getParent()->getVxContext(),
                                                               BaseGraph::getImageWidth(this->vxDataMap["vxData0"]),
                                                               BaseGraph::getImageHeight(this->vxDataMap["vxData0"]), VX_DF_IMAGE_U8);
                this->getParent()->Check(this->vxDataMap["vxData4"]);
                this->vxDataMap["vxData5"] = this->createImage(this->getParent()->getVxContext(),
                                                               BaseGraph::getImageWidth(this->vxDataMap["vxData0"]),
                                                               BaseGraph::getImageHeight(this->vxDataMap["vxData0"]), VX_DF_IMAGE_S16);
                this->getParent()->Check(this->vxDataMap["vxData5"]);
                this->vxNodesMap["acc_squ1"] = (vx_reference)vxAccumulateSquareImageNode(this->getVxGraph(),
                                                                                         (vx_image)this->vxDataMap["vxData0"],
                                                                                         (vx_scalar)this->vxDataMap["vxData1"],
                                                                                         (vx_image)this->vxDataMap["vxData2"]);
                this->getParent()->Check(this->vxNodesMap["acc_squ1"]);
                this->vxNodesMap["acc_wei1"] = (vx_reference)vxAccumulateWeightedImageNode(this->getVxGraph(),
                                                                                           (vx_image)this->vxDataMap["vxData0"],
                                                                                           (vx_scalar)this->vxDataMap["vxData3"],
                                                                                           (vx_image)this->vxDataMap["vxData4"]);
                this->getParent()->Check(this->vxNodesMap["acc_wei1"]);
                this->vxNodesMap["acc"] = (vx_reference)vxAccumulateImageNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData0"],
                                                                              (vx_image)this->vxDataMap["vxData5"]);
                this->getParent()->Check(this->vxNodesMap["acc"]);
            }
            return status;
        }

        vx_status process(const std::function<vx_status(int)> &$handler) override {
            return BaseGraph::process([&](int $iteration) -> vx_status {
                vx_status status = vxGetStatus((vx_reference)this->getVxGraph());
                if (status == VX_SUCCESS && !this->getParent()->getParent()->IsNoFileIo()) {
                    auto ioCom0 = this->getParent()->getParent()->getIoCom("ioCom0");
                    if ($iteration == 0) {
                        vx_image vxData0Ref = (vx_image)this->getData("vxData0");
                        status = ioCom0->getFrame(vxData0Ref, true);
                    }
                }
                if (status == VX_SUCCESS) {
                    status = this->getParent()->Check(vxProcessGraph(this->getVxGraph()));
                }
                if (status == VX_SUCCESS) {
                    auto ioCom1 = this->getParent()->getParent()->getIoCom("ioCom1");
                    vx_image vxData2Ref = (vx_image)this->getData("vxData2");
                    ioCom1->setFrame(vxData2Ref);
                }
                if (status == VX_SUCCESS) {
                    auto ioCom2 = this->getParent()->getParent()->getIoCom("ioCom2");
                    vx_image vxData4Ref = (vx_image)this->getData("vxData4");
                    ioCom2->setFrame(vxData4Ref);
                }
                if (status == VX_SUCCESS) {
                    auto ioCom3 = this->getParent()->getParent()->getIoCom("ioCom3");
                    vx_image vxData5Ref = (vx_image)this->getData("vxData5");
                    ioCom3->setFrame(vxData5Ref);
                }
                return status;
            });
        }
    };

    class TestContext : public BaseContext {
     public:
        explicit TestContext(BaseVisualGraph *parent) : BaseContext(parent) {}

     protected:
        vx_status create() override {
            this->graphsMap["TestGraph"] = new TestGraph(this);
            return vx_status_e::VX_SUCCESS;
        }

        vx_status process(const std::function<vx_status(void)> &$handler) override {
            return BaseContext::process([&]() -> vx_status {
                vx_status status = vxGetStatus((vx_reference)this->getVxContext());
                auto graph0 = this->getGraph("TestGraph");
                if (status == VX_SUCCESS && graph0 != nullptr) {
                    status = const_cast<BaseGraph *>(graph0)->Process();
                    if (status != VX_SUCCESS) {
                        const_cast<TestContext *>(this)->removeGraph("TestGraph");
                    }
                } else {
                    status = VX_FAILURE;
                }
                return status;
            });
        }
    };

    class VisualGraph : public BaseVisualGraph {
     protected:
        vx_status create() override {
            this->contextsMap["TestContext"] = new TestContext(this);
            return vx_status_e::VX_SUCCESS;
        }

        vx_status process(const std::function<vx_status(int)> &$handler) override {
            return BaseVisualGraph::process([&](int $iterator) -> vx_status {
                vx_status status = VX_FAILURE;
                BaseContext *context0 = const_cast<BaseContext *>(this->getContext("TestContext"));
                if (context0 != nullptr) {
                    status = VX_SUCCESS;
                }
                BaseGraph *graph_0_0 = nullptr;
                if (status == VX_SUCCESS) {
                    graph_0_0 = const_cast<BaseGraph *>(context0->getGraph("TestGraph"));
                    if (graph_0_0 != nullptr) {
                        status = VX_SUCCESS;
                    } else {
                        status = VX_FAILURE;
                    }
                }
                if (status == VX_SUCCESS) {
                    this->ioComMap.emplace("ioCom0", std::make_shared<Image>(
                            testRoot + "/bikegray_640x480.png",
                            context0->getVxContext()));
                }
                if (status == VX_SUCCESS) {
                    this->ioComMap.emplace("ioCom1", std::make_shared<Image>(testRoot + "/obikeaccq_640x480_P400_16b.png",
                                                                             context0->getVxContext()));
                }
                if (status == VX_SUCCESS) {
                    this->ioComMap.emplace("ioCom2", std::make_shared<Image>(testRoot + "/obikeaccw_640x480_P400_16b.png",
                                                                             context0->getVxContext()));
                }
                if (status == VX_SUCCESS) {
                    this->ioComMap.emplace("ioCom3", std::make_shared<Image>(testRoot + "/obikeaccu_640x480_P400_16b.png",
                                                                             context0->getVxContext()));
                }
                if (status == VX_SUCCESS) {
                    status = context0->Process();
                }
                return status;
            });
        }
    };

    TEST_F(VisualGraphTest, Complex) {
        VisualGraph visualGraph{};
        ASSERT_EQ(visualGraph.Create(), vx_status_e::VX_SUCCESS);
        ASSERT_EQ(visualGraph.Validate(), vx_status_e::VX_SUCCESS);
        ASSERT_EQ(visualGraph.Process(), vx_status_e::VX_SUCCESS);
    }
}
