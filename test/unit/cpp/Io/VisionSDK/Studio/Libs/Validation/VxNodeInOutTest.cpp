/* ********************************************************************************************************* *
*
* Copyright (c) 2018-2019 NXP
*
* SPDX-License-Identifier: BSD-3-Clause
* The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
* or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
*
* ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

#include "opencv2/video/tracking.hpp"
#include "opencv2/features2d.hpp"

namespace Io::VisionSDK::Studio::Libs::Validation {
    using Io::VisionSDK::Studio::Libs::Pattern;
    using Io::VisionSDK::Studio::Libs::Utils::Convert;

    void VX_CALLBACK logHandler(vx_context $context, vx_reference $ref, vx_status $status, const vx_char $string[]) {
        std::cout << "log: " << $status << "; " << $string << std::endl;
    }

    class VxNodeInOutTest : public testing::Test {
     public:
        Pattern<char> src1Data = {{5, 5, 5, 5},
                                  {5, 5, 5, 5},
                                  {5, 5, 5, 5}};
        Pattern<char> src2Data = {{2, 2, 2, 2},
                                  {2, 2, 2, 2},
                                  {2, 2, 2, 2}};
        vx_context context{};
        vx_graph graph{};

        void Run() {
            vx_status status = vxVerifyGraph(this->graph);
            if (status == vx_status_e::VX_SUCCESS) {
                status = vxProcessGraph(this->graph);
                if (status != vx_status_e::VX_SUCCESS) {
                    GTEST_FATAL_FAILURE_(("Graph process failed: " + std::to_string(status)).c_str());
                }
            } else {
                GTEST_FATAL_FAILURE_(("Graph verify failed: " + std::to_string(status)).c_str());
            }
        }

     protected:
        void SetUp() override {
            vx_status status;

            this->context = vxCreateContext();
            vxRegisterLogCallback(this->context, logHandler, _vx_bool_e::vx_false_e);

            this->graph = vxCreateGraph(this->context);
            status = vxGetStatus((vx_reference)this->graph);
            if (status != vx_status_e::VX_SUCCESS) {
                GTEST_FATAL_FAILURE_("Testing VxGraph can not be initialized.");
            }
        }

        void TearDown() override {
            vxReleaseGraph(&this->graph);
            vxReleaseContext(&this->context);
        }
    };

    TEST_F(VxNodeInOutTest, testVxAbsoluteDifference) {
        Pattern<uint8_t> in1(4, 3);
        Pattern<uint8_t> in2(4, 3);

        cv::Mat exp;
        cv::absdiff(in1.getMat(), in2.getMat(), exp);
        Pattern<uint8_t> expected(exp);

        vx_image image1 = vxCreateImage(this->context, in1.getVxWidth(), in1.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);
        vx_image image2 = vxCreateImage(this->context, in2.getVxWidth(), in2.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);
        vx_image output = vxCreateImage(this->context, expected.getVxWidth(), expected.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);

        ASSERT_EQ(Convert::CvToVx(in1.getMat(), image1, this->context, true), vx_status_e::VX_SUCCESS);
        ASSERT_EQ(Convert::CvToVx(in2.getMat(), image2, this->context, true), vx_status_e::VX_SUCCESS);
        ASSERT_EQ(vxGetStatus((vx_reference)vxAbsDiffNode(this->graph, image1, image2, output)), vx_status_e::VX_SUCCESS);

        this->Run();

        cv::Mat actual;
        Convert::VxToCv(output, actual, this->context);

        ASSERT_EQ(expected, actual);
    }

    TEST_F(VxNodeInOutTest, textVxArithmeticAddition) {
        Pattern<uint8_t> in1(4, 3);
        Pattern<uint8_t> in2(4, 3);

        cv::Mat exp;
        cv::add(in1.getMat(), in2.getMat(), exp);
        Pattern<uint8_t> expected(exp);

        vx_image image1 = vxCreateImage(this->context, in1.getVxWidth(), in1.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);
        vx_image image2 = vxCreateImage(this->context, in2.getVxWidth(), in2.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);
        vx_image output = vxCreateImage(this->context, expected.getVxWidth(), expected.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);

        Convert::CvToVx(in1.getMat(), image1, this->context, true);
        Convert::CvToVx(in2.getMat(), image2, this->context, true);
        vxAddNode(this->graph, image1, image2, VX_CONVERT_POLICY_SATURATE, output);

        this->Run();

        cv::Mat actual;
        Convert::VxToCv(output, actual, this->context);

        ASSERT_EQ(expected, actual);
    }

    TEST_F(VxNodeInOutTest, testVxArithmeticSubtraction) {
        Pattern<uint8_t> in1(4, 3);
        Pattern<uint8_t> in2(4, 3);

        cv::Mat exp;
        cv::subtract(in1.getMat(), in2.getMat(), exp);
        Pattern<uint8_t> expected(exp);

        vx_image image1 = vxCreateImage(this->context, in1.getVxWidth(), in1.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);
        vx_image image2 = vxCreateImage(this->context, in2.getVxWidth(), in2.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);
        vx_image output = vxCreateImage(this->context, expected.getVxWidth(), expected.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);
        Convert::CvToVx(in1.getMat(), image1, this->context, true);
        Convert::CvToVx(in2.getMat(), image2, this->context, true);

        vxSubtractNode(this->graph, image1, image2, VX_CONVERT_POLICY_SATURATE, output);

        this->Run();

        cv::Mat actual;
        Convert::VxToCv(output, actual, this->context);

        ASSERT_EQ(expected, actual);
    }

    TEST_F(VxNodeInOutTest, testVxBitwiseAnd) {
        Pattern<uint8_t> in1(4, 3);
        Pattern<uint8_t> in2(4, 3);

        cv::Mat exp;
        cv::bitwise_and(in1.getMat(), in2.getMat(), exp);
        Pattern<uint8_t> expected(exp);

        vx_image image1 = vxCreateImage(this->context, in1.getVxWidth(), in1.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);
        vx_image image2 = vxCreateImage(this->context, in2.getVxWidth(), in2.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);
        vx_image output = vxCreateImage(this->context, expected.getVxWidth(), expected.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);
        Convert::CvToVx(in1.getMat(), image1, this->context, true);
        Convert::CvToVx(in2.getMat(), image2, this->context, true);

        vxAndNode(this->graph, image1, image2, output);

        this->Run();

        cv::Mat actual;
        Convert::VxToCv(output, actual, this->context);

        ASSERT_EQ(expected, actual);
    }

    TEST_F(VxNodeInOutTest, testVxBitwiseNot) {
        Pattern<uint8_t> in1(4, 3);

        cv::Mat exp;
        cv::bitwise_not(in1.getMat(), exp);
        Pattern<uint8_t> expected(exp);

        vx_image image1 = vxCreateImage(this->context, in1.getVxWidth(), in1.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);
        vx_image output = vxCreateImage(this->context, expected.getVxWidth(), expected.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);
        Convert::CvToVx(in1.getMat(), image1, this->context, true);

        vxNotNode(this->graph, image1, output);

        this->Run();

        cv::Mat actual;
        Convert::VxToCv(output, actual, this->context);

        ASSERT_EQ(expected, actual);
    }

    TEST_F(VxNodeInOutTest, testVxBitwiseOr) {
        Pattern<uint8_t> in1(4, 3);
        Pattern<uint8_t> in2(4, 3);

        cv::Mat exp;
        cv::bitwise_or(in1.getMat(), in1.getMat(), exp);
        Pattern<uint8_t> expected(exp);

        vx_image image1 = vxCreateImage(this->context, in1.getVxWidth(), in1.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);
        vx_image image2 = vxCreateImage(this->context, in2.getVxWidth(), in2.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);
        vx_image output = vxCreateImage(this->context, expected.getVxWidth(), expected.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);
        Convert::CvToVx(in1.getMat(), image1, this->context, true);
        Convert::CvToVx(in2.getMat(), image2, this->context, true);

        vxOrNode(this->graph, image1, image2, output);

        this->Run();

        cv::Mat actual;
        Convert::VxToCv(output, actual, this->context);

        ASSERT_EQ(expected, actual);
    }

    TEST_F(VxNodeInOutTest, testVxBitwiseXor) {
        Pattern<uint8_t> in1(4, 3);
        Pattern<uint8_t> in2(4, 3);

        cv::Mat exp;
        cv::bitwise_xor(in1.getMat(), in2.getMat(), exp);
        Pattern<uint8_t> expected(exp);

        vx_image image1 = vxCreateImage(this->context, in1.getVxWidth(), in1.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);
        vx_image image2 = vxCreateImage(this->context, in2.getVxWidth(), in2.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);
        vx_image output = vxCreateImage(this->context, expected.getVxWidth(), expected.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);
        Convert::CvToVx(in1.getMat(), image1, this->context, true);
        Convert::CvToVx(in2.getMat(), image2, this->context, true);

        vxXorNode(this->graph, image1, image2, output);

        this->Run();

        cv::Mat actual;
        Convert::VxToCv(output, actual, this->context);

        ASSERT_EQ(expected, actual);
    }

    TEST_F(VxNodeInOutTest, testVxAccumulate) {
        Pattern<uint8_t> in1(4, 3);

        cv::Mat exp = cv::Mat::zeros(in1.getSize(), CV_32FC1);
        cv::accumulate(in1.getMat(), exp);
        exp.convertTo(exp, CV_16SC1);
        Pattern<int16_t> expected(exp);

        vx_image image1 = vxCreateImage(this->context, in1.getVxWidth(), in1.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);
        vx_image output = vxCreateImage(this->context, expected.getVxWidth(), expected.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_S16);
        Convert::CvToVx(cv::Mat::zeros(in1.getSize(), CV_16SC1), output, this->context, true);
        Convert::CvToVx(in1.getMat(), image1, this->context, true);

        vxAccumulateImageNode(this->graph, image1, output);

        this->Run();

        cv::Mat actual;
        Convert::VxToCv(output, actual, this->context);

        ASSERT_EQ(expected, actual);
    }

    TEST_F(VxNodeInOutTest, testVxAccumulateSquared) {
        Pattern<uint8_t> in1(4, 3);

        cv::Mat exp = cv::Mat::zeros(in1.getSize(), CV_32FC1);
        cv::accumulateSquare(in1.getMat(), exp);
        exp.convertTo(exp, CV_16SC1);
        Pattern<int16_t> expected(exp);

        vx_image image1 = vxCreateImage(this->context, in1.getVxWidth(), in1.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);
        vx_image output = vxCreateImage(this->context, expected.getVxWidth(), expected.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_S16);
        vx_uint32 value = 0;
        vx_scalar shift = vxCreateScalar(this->context, VX_TYPE_UINT32, &value);

        Convert::CvToVx(cv::Mat::zeros(in1.getSize(), CV_16SC1), output, this->context, true);
        Convert::CvToVx(in1.getMat(), image1, this->context, true);

        vxAccumulateSquareImageNode(this->graph, image1, shift, output);

        this->Run();

        cv::Mat actual;
        Convert::VxToCv(output, actual, this->context);

        ASSERT_EQ(expected, actual);
    }

    TEST_F(VxNodeInOutTest, testVxAccumulateWeighed) {
        Pattern<uint8_t> in1(4, 3);

        cv::Mat exp = cv::Mat::ones(in1.getSize(), CV_32FC1);
        cv::accumulateWeighted(in1.getMat(), exp, 1.0f);
        exp.convertTo(exp, CV_8UC1);
        Pattern<uint8_t> expected(exp);

        vx_image image1 = vxCreateImage(this->context, in1.getVxWidth(), in1.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);
        vx_image output = vxCreateImage(this->context, expected.getVxWidth(), expected.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);
        vx_float32 value = 1.0f;
        vx_scalar alpha = vxCreateScalar(this->context, VX_TYPE_FLOAT32, &value);
        Convert::CvToVx(cv::Mat::ones(in1.getSize(), CV_8UC1), output, this->context, true);
        Convert::CvToVx(in1.getMat(), image1, this->context, true);

        vxAccumulateWeightedImageNode(this->graph, image1, alpha, output);

        this->Run();

        cv::Mat actual;
        Convert::VxToCv(output, actual, this->context);

        ASSERT_EQ(expected, actual);
    }

    TEST_F(VxNodeInOutTest, testVxMagnitude) {
        Pattern<uint8_t> in1(4, 3);
        Pattern<uint8_t> in2(4, 3);

        cv::Mat src1 = in1.getMat();
        cv::Mat src2 = in2.getMat();

        src1.convertTo(src1, CV_64FC1);
        src2.convertTo(src2, CV_64FC1);

        cv::Mat exp = cv::Mat(in1.getSize(), CV_64FC1);

        cv::magnitude(src1, src2, exp);
        exp.convertTo(exp, CV_16SC1);
        Pattern<int16_t> expected(exp);

        src1.convertTo(src1, CV_16SC1);
        src2.convertTo(src2, CV_16SC1);

        vx_image image1 = vxCreateImage(this->context, in1.getVxWidth(), in1.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_S16);
        vx_image image2 = vxCreateImage(this->context, in2.getVxWidth(), in2.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_S16);
        vx_image output = vxCreateImage(this->context, expected.getVxWidth(), expected.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_S16);
        Convert::CvToVx(src1, image1, this->context, true);
        Convert::CvToVx(src2, image2, this->context, true);

        vxMagnitudeNode(this->graph, image1, image2, output);

        this->Run();

        cv::Mat actual;
        Convert::VxToCv(output, actual, this->context);

        ASSERT_EQ(expected, actual);
    }

    TEST_F(VxNodeInOutTest, testVxMeanAndStandard) {
        Pattern<uint8_t> data = {{1, 2,  3,  4},
                                 {5, 6,  7,  8},
                                 {9, 10, 11, 12}};
        cv::Mat expMean;
        cv::Mat expStandard;
        expMean.convertTo(expMean, CV_64FC1);
        expStandard.convertTo(expStandard, CV_64FC1);

        cv::meanStdDev(data.getMat(), expMean, expStandard);
        expMean.convertTo(expMean, CV_32FC1);
        expStandard.convertTo(expStandard, CV_32FC1);

        Pattern<float> expectedMean(expMean);
        Pattern<float> expectedStandard(expStandard);

        vx_image input = vxCreateImage(this->context, data.getVxWidth(), data.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);
        Convert::CvToVx(data.getMat(), input, this->context, true);
        vx_float32 val = 0;
        vx_scalar mean = vxCreateScalar(this->context, VX_TYPE_FLOAT32, &val);
        vx_float32 actMean;
        vx_scalar standard = vxCreateScalar(this->context, VX_TYPE_FLOAT32, &val);
        vx_float32 actStandard;

        vxMeanStdDevNode(this->graph, input, mean, standard);

        this->Run();

        vxCopyScalar(mean, &actMean, VX_READ_ONLY, VX_MEMORY_TYPE_HOST);
        vxCopyScalar(standard, &actStandard, VX_READ_ONLY, VX_MEMORY_TYPE_HOST);

        Pattern<float> actualMean = {{actMean}};
        Pattern<float> actualStandard = {{actStandard}};

        ASSERT_EQ(expectedMean, actualMean);
        ASSERT_EQ(expectedStandard, actualStandard);
    }

    TEST_F(VxNodeInOutTest, testVxMinAndMax) {
        Pattern<uint8_t> data = {{1, 2,  3,  4},
                                 {5, 6,  7,  8},
                                 {9, 10, 11, 12}};

        double expMin;
        double expMax;
        cv::minMaxLoc(data.getMat(), &expMin, &expMax);

        vx_image input = vxCreateImage(this->context, data.getVxWidth(), data.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);
        vx_scalar min = vxCreateScalar(this->context, VX_TYPE_UINT8, nullptr);
        vx_scalar max = vxCreateScalar(this->context, VX_TYPE_UINT8, nullptr);

        Convert::CvToVx(data.getMat(), input, this->context, true);
        // TODO(nxa33894) check also other outputs
        vxMinMaxLocNode(this->graph, input, min, max, nullptr, nullptr, nullptr, nullptr);

        this->Run();

        vx_uint32 minVal = 0;
        vxCopyScalar(min, &minVal, VX_READ_ONLY, VX_MEMORY_TYPE_HOST);
        vx_uint32 maxVal = 0;
        vxCopyScalar(max, &maxVal, VX_READ_ONLY, VX_MEMORY_TYPE_HOST);

        ASSERT_EQ(minVal, expMin);
        ASSERT_EQ(maxVal, expMax);
    }

    TEST_F(VxNodeInOutTest, testVxMedianFilter) {
        Pattern<uint8_t> data(4, 3);

        cv::Mat exp = cv::Mat::zeros(data.getSize(), CV_8UC1);
        cv::medianBlur(data.getMat(), exp, 3);
        Pattern<uint8_t> expected(exp);

        vx_image input = vxCreateImage(this->context, data.getVxWidth(), data.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);
        vx_image output = vxCreateImage(this->context, expected.getVxWidth(), expected.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);
        Convert::CvToVx(data.getMat(), input, this->context, true);
        Convert::CvToVx(cv::Mat::zeros(expected.getSize(), CV_8UC1), output, this->context, true);

        vx_node node = vxMedian3x3Node(this->graph, input, output);
        vx_border_t border;
        border.mode = vx_border_e::VX_BORDER_REPLICATE;
        ASSERT_EQ(vx_status_e::VX_SUCCESS, vxSetNodeAttribute(node, VX_NODE_BORDER, &border, sizeof(border)));

        this->Run();

        cv::Mat actual;
        Convert::VxToCv(output, actual, this->context);

        ASSERT_EQ(expected, actual);
    }

    TEST_F(VxNodeInOutTest, testVxWarpPerspective) {
        Pattern<uint8_t> data(4, 3);

        // identity
        // TODO(nxa33894) add conversions to Pattern<>
        vx_float32 m[3][3] = {{1.0, 0.0, 0.0},
                              {0.0, 1.0, 0.0},
                              {0.0, 0.0, 1.0}};
        Pattern<double> mCv = {{1.0, 0.0, 0.0},
                               {0.0, 1.0, 0.0},
                               {0.0, 0.0, 1.0}};

        cv::Mat exp;
        cv::warpPerspective(data.getMat(), exp, mCv.getMat(), data.getMat().size());
        Pattern<uint8_t> expected(exp);

        vx_image image = vxCreateImage(this->context, data.getVxWidth(), data.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);
        Convert::CvToVx(data.getMat(), image, this->context, true);

        vx_image output = vxCreateImage(this->context, data.getVxWidth(), data.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);
        Convert::CvToVx(cv::Mat::zeros(data.getSize(), CV_8UC1), output, this->context, true);

        vx_matrix M = vxCreateMatrix(this->context, VX_TYPE_FLOAT32, 3, 3);
        vxCopyMatrix(M, &m, VX_WRITE_ONLY, VX_MEMORY_TYPE_HOST);

        vxWarpPerspectiveNode(this->graph, image, M, VX_INTERPOLATION_NEAREST_NEIGHBOR, output);

        this->Run();

        cv::Mat actual;
        Convert::VxToCv(output, actual, this->context);

        ASSERT_EQ(expected, actual);
    }

    TEST_F(VxNodeInOutTest, testVxWarpAffine) {
        Pattern<uint8_t> data(4, 3);

        // identity
        vx_float32 m[3][2] = {{1.0, 0.0},
                              {0.0, 1.0},
                              {0.0, 0.0}};
        Pattern<double> mCv = {{1.0, 0.0, 0.0},
                               {0.0, 1.0, 0.0}};

        cv::Mat exp;
        cv::warpAffine(data.getMat(), exp, mCv.getMat(), data.getMat().size());
        Pattern<uint8_t> expected(exp);

        vx_image image = vxCreateImage(this->context, data.getVxWidth(), data.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);
        Convert::CvToVx(data.getMat(), image, this->context, true);
        vx_image output = vxCreateImage(this->context, expected.getVxWidth(), expected.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);
        Convert::CvToVx(cv::Mat::zeros(expected.getSize(), CV_8UC1), output, this->context, true);

        vx_matrix M = vxCreateMatrix(this->context, VX_TYPE_FLOAT32, 2, 3);
        vxCopyMatrix(M, &m, VX_WRITE_ONLY, VX_MEMORY_TYPE_HOST);

        vxWarpAffineNode(this->graph, image, M, VX_INTERPOLATION_NEAREST_NEIGHBOR, output);

        this->Run();

        cv::Mat actual;
        Convert::VxToCv(output, actual, this->context);

        ASSERT_EQ(expected, actual);
    }

    TEST_F(VxNodeInOutTest, testVxThresholding) {
        Pattern<uint8_t> data(4, 3, 0, 15);
        cv::Mat exp;
        cv::threshold(data.getMat(), exp, 7, 10, data.getMat().type());
        Pattern<uint8_t> expected(exp);

        vx_image input = vxCreateImage(this->context, data.getVxWidth(), data.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);
        Convert::CvToVx(data.getMat(), input, this->context, true);
        vx_image output = vxCreateImage(this->context, expected.getVxWidth(), expected.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);

        vx_int32 val = 7;
        vx_int32 trueValue = 10;
        vx_int32 falseValue = 0;

#if VX_VERSION == VX_VERSION_1_1
        vx_threshold threshold = vxCreateThreshold(this->context, VX_THRESHOLD_TYPE_BINARY, VX_TYPE_UINT8);
        vxSetThresholdAttribute(threshold, VX_THRESHOLD_THRESHOLD_VALUE, &val, sizeof(val));
        vxSetThresholdAttribute(threshold, VX_THRESHOLD_FALSE_VALUE, &falseValue, sizeof(falseValue));
        vxSetThresholdAttribute(threshold, VX_THRESHOLD_TRUE_VALUE, &trueValue, sizeof(trueValue));
#elif VX_VERSION == VX_VERSION_1_2
        vx_threshold threshold = vxCreateThresholdForImage(this->context, vx_threshold_type_e::VX_THRESHOLD_TYPE_BINARY,
                                                           vx_df_image_e::VX_DF_IMAGE_U8, vx_df_image_e::VX_DF_IMAGE_U8);
        vx_pixel_value_t p1, p2;
        p1.U8 = trueValue;
        p2.U8 = falseValue;
        vxCopyThresholdOutput(threshold, &p1, &p2, vx_accessor_e::VX_WRITE_ONLY, vx_memory_type_e::VX_MEMORY_TYPE_HOST);
        p1.U8 = val;
        vxCopyThresholdValue(threshold, &p1, vx_accessor_e::VX_WRITE_ONLY, vx_memory_type_e::VX_MEMORY_TYPE_HOST);
#endif
        vxThresholdNode(this->graph, input, threshold, output);
        this->Run();

        cv::Mat actual = cv::Mat();
        Convert::VxToCv(output, actual, this->context);

        ASSERT_EQ(expected, actual);
    }

    TEST_F(VxNodeInOutTest, testVxPhase) {
        Pattern<uint8_t> in1(4, 3);
        Pattern<uint8_t> in2(4, 3);

        cv::Mat src1;
        cv::Mat src2;
        in1.getMat().convertTo(src1, CV_32F);
        in1.getMat().convertTo(src2, CV_32F);
        cv::Mat angle = cv::Mat::zeros(in1.getSize(), in1.getType());

        cv::phase(src1, src2, angle);

        // scale to openvx phase range
        angle *= 255 / (2 * M_PI);
        for (int i = 0; i < angle.rows * angle.cols; i++) {
            angle.at<float>(i) = std::round(std::round((std::round(angle.at<float>(i) * 100) / 100) * 10) / 10);
        }
        angle.convertTo(angle, CV_8UC1);
        Pattern<uint8_t> expected(angle);

        src1.convertTo(src1, CV_16SC1);
        src2.convertTo(src2, CV_16SC1);
        vx_image image1 = vxCreateImage(this->context, in1.getVxWidth(), in1.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_S16);
        vx_image image2 = vxCreateImage(this->context, in2.getVxWidth(), in2.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_S16);
        vx_image output = vxCreateImage(this->context, expected.getVxWidth(), expected.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);
        Convert::CvToVx(src1, image1, this->context, true);
        Convert::CvToVx(src2, image2, this->context, true);

        vxPhaseNode(this->graph, image1, image2, output);

        this->Run();

        cv::Mat actual;
        Convert::VxToCv(output, actual, this->context);

        ASSERT_EQ(expected, actual);
    }

    TEST_F(VxNodeInOutTest, testVxIntegral) {
        Pattern<uint8_t> data(4, 4);

        cv::Mat exp;
        cv::integral(data.getMat(), exp);
        exp.convertTo(exp, CV_32SC1);
        // crop first col/row because ocv adds them
        Pattern<int32_t> expected(cv::Mat(exp, cv::Rect(1, 1, data.getWidth(), data.getHeight())));

        vx_image input = vxCreateImage(this->context, data.getVxWidth(), data.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);
        vx_image integral = vxCreateImage(this->context, expected.getVxWidth(), expected.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U32);

        Convert::CvToVx(data.getMat(), input, this->context, true);
        Convert::CvToVx(cv::Mat::zeros(expected.getSize(), CV_32SC1), integral, this->context, true);

        vx_node node = vxIntegralImageNode(this->graph, input, integral);
        vx_border_t border;
        border.mode = vx_border_e::VX_BORDER_REPLICATE;
        ASSERT_EQ(vx_status_e::VX_SUCCESS, vxSetNodeAttribute(node, VX_NODE_BORDER, &border, sizeof(border)));

        this->Run();

        cv::Mat actual;
        Convert::VxToCv(integral, actual, this->context);

        ASSERT_EQ(expected, actual);
    }

    TEST_F(VxNodeInOutTest, testVxPixelWiseMultiplication) {
        Pattern<uint8_t> in1(4, 3);
        Pattern<uint8_t> in2(4, 3);

        double scale = 2;
        cv::Mat exp;
        cv::multiply(in1.getMat(), in2.getMat(), exp, scale);
        Pattern<uint8_t> expected(exp);

        vx_image image1 = vxCreateImage(this->context, in1.getVxWidth(), in1.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);
        vx_image image2 = vxCreateImage(this->context, in2.getVxWidth(), in2.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);
        vx_image output = vxCreateImage(this->context, expected.getVxWidth(), expected.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);
        auto val = (vx_float32)scale;
        vx_scalar vxScale = vxCreateScalar(this->context, VX_TYPE_FLOAT32, &val);
        Convert::CvToVx(in1.getMat(), image1, this->context, true);
        Convert::CvToVx(in2.getMat(), image2, this->context, true);

        vxMultiplyNode(this->graph, image1, image2, vxScale, VX_CONVERT_POLICY_SATURATE, VX_ROUND_POLICY_TO_NEAREST_EVEN, output);

        this->Run();

        cv::Mat actual;
        Convert::VxToCv(output, actual, this->context);

        ASSERT_EQ(expected, actual);
    }

    TEST_F(VxNodeInOutTest, testVxErode) {
        Pattern<uint8_t> data(4, 3);

        cv::Mat exp;
        cv::erode(data.getMat(), exp, cv::noArray(), {-1, -1}, 1, cv::BORDER_REPLICATE);
        Pattern<uint8_t> expected(exp);

        vx_image input = vxCreateImage(this->context, data.getVxWidth(), data.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);
        vx_image output = vxCreateImage(this->context, expected.getVxWidth(), expected.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);

        Convert::CvToVx(data.getMat(), input, this->context, true);
        Convert::CvToVx(cv::Mat::zeros(expected.getSize(), CV_8UC1), output, this->context, true);

        vx_node node = vxErode3x3Node(this->graph, input, output);
        vx_border_t border;
        border.mode = vx_border_e::VX_BORDER_REPLICATE;
        ASSERT_EQ(vx_status_e::VX_SUCCESS, vxSetNodeAttribute(node, VX_NODE_BORDER, &border, sizeof(border)));

        this->Run();

        cv::Mat actual;
        Convert::VxToCv(output, actual, this->context);

        ASSERT_EQ(expected, actual);
    }

    TEST_F(VxNodeInOutTest, testVxDilate) {
        Pattern<uint8_t> data(4, 3);

        cv::Mat exp;
        cv::dilate(data.getMat(), exp, cv::noArray(), {-1, -1}, 1, cv::BORDER_REPLICATE);
        Pattern<uint8_t> expected(exp);

        vx_image input = vxCreateImage(this->context, data.getVxWidth(), data.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);
        vx_image output = vxCreateImage(this->context, expected.getVxWidth(), expected.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);

        Convert::CvToVx(data.getMat(), input, this->context, true);
        Convert::CvToVx(cv::Mat::zeros(expected.getSize(), CV_8UC1), output, this->context, true);

        vx_node node = vxDilate3x3Node(this->graph, input, output);
        vx_border_t border;
        border.mode = vx_border_e::VX_BORDER_REPLICATE;
        ASSERT_EQ(vx_status_e::VX_SUCCESS, vxSetNodeAttribute(node, VX_NODE_BORDER, &border, sizeof(border)));

        this->Run();

        cv::Mat actual = cv::Mat();
        Convert::VxToCv(output, actual, this->context);

        ASSERT_EQ(expected, actual);
    }

    TEST_F(VxNodeInOutTest, testVxLUT) {
        Pattern<uint8_t> data(4, 3);

        std::size_t count = 256;
        std::vector<std::vector<uint8_t>> lut = {std::vector<uint8_t>(count)};
        for (std::size_t i = 0; i < count; i++) {
            lut[0][i] = static_cast<uint8_t>(i >= 80 ? 11 : 0);
        }

        Pattern<uint8_t> LUT(lut);

        cv::Mat exp;
        cv::LUT(data.getMat(), LUT.getMat(), exp);

        Pattern<uint8_t> expected(exp);

        vx_image input = vxCreateImage(this->context, data.getVxWidth(), data.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);
        vx_image output = vxCreateImage(this->context, expected.getVxWidth(), expected.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);

        vx_lut tab = vxCreateLUT(this->context, VX_TYPE_UINT8, count);
        vxCopyLUT(tab, &lut[0][0], VX_WRITE_ONLY, VX_MEMORY_TYPE_HOST);

        Convert::CvToVx(data.getMat(), input, this->context, true);
        Convert::CvToVx(cv::Mat::zeros(data.getSize(), CV_8UC1), output, this->context, true);

        vxTableLookupNode(this->graph, input, tab, output);

        this->Run();

        cv::Mat actual;
        Convert::VxToCv(output, actual, this->context);

        ASSERT_EQ(expected, actual);
    }

    TEST_F(VxNodeInOutTest, testVxSobel) {
        // TODO(nxa33894) not working for random - looks like saturation
        Pattern<uint8_t> data = {{1, 2,  3,  4},
                                 {5, 6,  7,  8},
                                 {9, 10, 11, 12}};

        cv::Mat expX;
        cv::Mat expY;
        cv::Sobel(data.getMat(), expX, data.getMat().depth(), 1, 0, 3, 1, 0, cv::BORDER_REPLICATE);
        cv::Sobel(data.getMat(), expY, data.getMat().depth(), 0, 1, 3, 1, 0, cv::BORDER_REPLICATE);
        expX.convertTo(expX, CV_16SC1);
        expY.convertTo(expY, CV_16SC1);
        Pattern<int16_t> expectedX(expX);
        Pattern<int16_t> expectedY(expY);

        vx_image input = vxCreateImage(this->context, data.getVxWidth(), data.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);
        vx_image outputX = vxCreateImage(this->context, expectedX.getVxWidth(), expectedX.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_S16);
        vx_image outputY = vxCreateImage(this->context, expectedY.getVxWidth(), expectedY.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_S16);

        Convert::CvToVx(data.getMat(), input, this->context, true);
        Convert::CvToVx(cv::Mat::zeros(expectedX.getSize(), CV_16SC1), outputX, this->context, true);
        Convert::CvToVx(cv::Mat::zeros(expectedY.getSize(), CV_16SC1), outputY, this->context, true);

        vx_node node = vxSobel3x3Node(this->graph, input, outputX, outputY);
        vx_border_t border;
        border.mode = vx_border_e::VX_BORDER_REPLICATE;
        ASSERT_EQ(vx_status_e::VX_SUCCESS, vxSetNodeAttribute(node, VX_NODE_BORDER, &border, sizeof(border)));

        this->Run();

        cv::Mat actualX;
        cv::Mat actualY;
        Convert::VxToCv(outputX, actualX, this->context);
        Convert::VxToCv(outputY, actualY, this->context);

        ASSERT_EQ(expectedX, actualX);
        ASSERT_EQ(expectedY, actualY);
    }

    TEST_F(VxNodeInOutTest, testVxHistogram) {
        // equalize produces different values for 2 so replaced by 3
        Pattern<uint8_t> data = {{0, 1, 0, 3},
                                 {1, 3, 1, 0},
                                 {3, 1, 3, 3}};

        cv::Mat expHist;
        cv::Mat expEqualized;
        vx_size hbins = 4;
        vx_uint32 range = 4;
        {
            int histSize[] = {static_cast<int>(hbins), 1};
            float hranges[] = {0, static_cast<float>(range)};
            float sranges[] = {0, static_cast<float>(range)};
            const float *ranges[] = {hranges, sranges};
            int channels[] = {0};

            expHist.create(cv::Size(static_cast<int>(hbins), 1), data.getMat().type());
            cv::calcHist(&data.getMat(), 1, channels, cv::Mat(), expHist, 1, histSize, ranges);
            expHist.convertTo(expHist, CV_8UC1);

            expEqualized.create(data.getMat().size(), data.getMat().type());
            cv::equalizeHist(data.getMat(), expEqualized);
        }

        Pattern<uint8_t> expectedEqualized(expEqualized);

        vx_image input = vxCreateImage(this->context, data.getVxWidth(), data.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);
        vx_image output = vxCreateImage(this->context, expectedEqualized.getVxWidth(), expectedEqualized.getVxHeight(),
                                        vx_df_image_e::VX_DF_IMAGE_U8);

        vx_distribution distribution = vxCreateDistribution(this->context, hbins, 0, range);

        Convert::CvToVx(data.getMat(), input, this->context, true);

        vxHistogramNode(this->graph, input, distribution);
        vxEqualizeHistNode(this->graph, input, output);

        this->Run();

        vx_uint32 distVal[hbins];  // NOLINT(runtime/arrays)
        vxCopyDistribution(distribution, &distVal, VX_READ_ONLY, VX_MEMORY_TYPE_HOST);

        for (vx_size i = 0; i < hbins; i++) {
            ASSERT_EQ((vx_uint32)expHist.at<uint8_t>(i), distVal[i]) << "Item on index: " << i << " not match." << std::endl;
        }

        cv::Mat equalized;
        Convert::VxToCv(output, equalized, this->context);

        ASSERT_EQ(expectedEqualized, equalized);
    }

    TEST_F(VxNodeInOutTest, testVxChannelCombine) {
        Pattern<uint8_t> r(4, 4);
        Pattern<uint8_t> g(4, 4);
        Pattern<uint8_t> b(4, 4);
        Pattern<uint8_t> a(4, 4);

        cv::Mat exp(r.getSize(), CV_8UC4);
        std::vector<cv::Mat> channels = {b.getMat(), g.getMat(), r.getMat(), a.getMat()};
        cv::merge(channels, exp);
        Pattern<uint8_t> expected(exp);

        vx_image channel0 = vxCreateImage(this->context, r.getVxWidth(), r.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);
        vx_image channel1 = vxCreateImage(this->context, b.getVxWidth(), g.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);
        vx_image channel2 = vxCreateImage(this->context, b.getVxWidth(), b.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);
        vx_image channel3 = vxCreateImage(this->context, a.getVxWidth(), a.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);
        vx_image output = vxCreateImage(this->context, expected.getVxWidth(), expected.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_RGBX);

        Convert::CvToVx(r.getMat(), channel0, this->context, true);
        Convert::CvToVx(g.getMat(), channel1, this->context, true);
        Convert::CvToVx(b.getMat(), channel2, this->context, true);
        Convert::CvToVx(a.getMat(), channel3, this->context, true);

        vxChannelCombineNode(this->graph, channel0, channel1, channel2, channel3, output);

        this->Run();

        cv::Mat actual;
        Convert::VxToCv(output, actual, this->context);

        ASSERT_EQ(expected, actual);
    }

    TEST_F(VxNodeInOutTest, testVxChannelExtract) {
        Pattern<uint8_t> r(4, 3);
        Pattern<uint8_t> g(4, 3);
        Pattern<uint8_t> b(4, 3);

        std::vector<cv::Mat> vectData = {b.getMat(), g.getMat(), r.getMat()};

        cv::Mat data;
        cv::merge(vectData, data);

        vx_image input = vxCreateImage(this->context, static_cast<vx_uint32>(data.cols), static_cast<vx_uint32>(data.rows),
                                       vx_df_image_e::VX_DF_IMAGE_RGB);
        vx_image outputR = vxCreateImage(this->context, r.getVxWidth(), r.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);
        vx_image outputB = vxCreateImage(this->context, b.getVxWidth(), b.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);

        Convert::CvToVx(data, input, this->context, true);

        vxChannelExtractNode(this->graph, input, vx_channel_e::VX_CHANNEL_R, outputR);
        vxChannelExtractNode(this->graph, input, vx_channel_e::VX_CHANNEL_B, outputB);

        this->Run();

        cv::Mat actualR, actualB;
        Convert::VxToCv(outputR, actualR, this->context);
        Convert::VxToCv(outputB, actualB, this->context);

        ASSERT_EQ(r, actualR);
        ASSERT_EQ(b, actualB);
    }

    TEST_F(VxNodeInOutTest, testVxRemap) {
        Pattern<uint8_t> data(4, 3);
        cv::Mat mapX = cv::Mat(data.getSize(), CV_32FC1);
        cv::Mat mapY = cv::Mat(data.getSize(), CV_32FC1);

        for (int j = 0; j < mapX.rows; j++) {
            for (int i = 0; i < mapX.cols; i++) {
                mapX.at<float>(j, i) = i * j;
            }
        }

        for (int j = 0; j < mapY.rows; j++) {
            for (int i = 0; i < mapY.cols; i++) {
                mapY.at<float>(j, i) = i * j;
            }
        }

        cv::Mat exp;
        cv::remap(data.getMat(), exp, mapX, mapY, CV_INTER_LINEAR, cv::BORDER_REPLICATE);

        Pattern<uint8_t> expected(exp);
        vx_image input = vxCreateImage(this->context, data.getVxWidth(), data.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);
        Convert::CvToVx(data.getMat(), input, this->context, true);

        vx_image output = vxCreateImage(this->context, expected.getVxWidth(), expected.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);
        vx_remap table = vxCreateRemap(this->context, data.getVxWidth(), data.getVxHeight(), expected.getVxWidth(), expected.getVxHeight());

#if VX_VERSION == VX_VERSION_1_1
        for (vx_uint32 x = 0; x < data.getVxWidth(); x++) {
            for (vx_uint32 y = 0; y < data.getVxHeight(); y++) {
                vx_float32 rx = x * y;
                vx_float32 ry = x * y;
                vxSetRemapPoint(table, x, y, rx, ry);
            }
        }
#elif VX_VERSION == VX_VERSION_1_2
        vx_rectangle_t rect = {.start_x = 0, .start_y = 0, .end_x = data.getVxWidth(), .end_y = data.getVxHeight()};
        auto *coords = new vx_coordinates2df_t[data.getVxWidth() * data.getVxHeight()];
        for (vx_uint32 j = 0; j < data.getVxHeight(); j++) {
            for (vx_uint32 i = 0; i < data.getVxWidth(); i++) {
                coords[i + (j * data.getVxWidth())] = {.x = static_cast<vx_float32>(i * j), .y = static_cast<vx_float32>(i * j)};
            }
        }
        vxCopyRemapPatch(table, &rect, sizeof(vx_coordinates2d_t) * data.getVxWidth(), coords, vx_type_e::VX_TYPE_COORDINATES2DF,
                         vx_accessor_e::VX_WRITE_ONLY, vx_memory_type_e::VX_MEMORY_TYPE_HOST);

        delete[] coords;
#endif
        vx_node node = vxRemapNode(this->graph, input, table, VX_INTERPOLATION_NEAREST_NEIGHBOR, output);
        vx_border_t border;
        border.mode = vx_border_e::VX_BORDER_REPLICATE;
        ASSERT_EQ(vx_status_e::VX_SUCCESS, vxSetNodeAttribute(node, VX_NODE_BORDER, &border, sizeof(border)));

        this->Run();

        cv::Mat actual;
        Convert::VxToCv(output, actual, this->context);

        ASSERT_EQ(expected, actual);
    }

    TEST_F(VxNodeInOutTest, testVxConvertBitDepth) {
        Pattern<int16_t> data = {{100, 200,  300,  400},
                                 {500, 600,  700,  800},
                                 {900, 1000, 2000, 3000}};

        cv::Mat exp;
        data.getMat().convertTo(exp, CV_8UC1);
        Pattern<uint8_t> expected(exp);

        vx_image input = vxCreateImage(this->context, data.getVxWidth(), data.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_S16);
        Convert::CvToVx(data.getMat(), input, this->context, true);
        vx_int32 val = 0;
        vx_scalar shift = vxCreateScalar(this->context, VX_TYPE_INT32, &val);
        vx_image output = vxCreateImage(this->context, expected.getVxWidth(), expected.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);

        vxConvertDepthNode(this->graph, input, output, VX_CONVERT_POLICY_SATURATE, shift);

        this->Run();

        cv::Mat actual;
        Convert::VxToCv(output, actual, this->context);

        ASSERT_EQ(expected, actual);
    }

    TEST_F(VxNodeInOutTest, testVxCustomConvolution) {
        Pattern<uint8_t> data(10, 10, 0, 10);

        // not working properly for kernels with size greater than 3x3
//        vx_int16 kern[5][5] = {{1, 2, 1, 2, 3},
//                               {2, 4, 2, 1, 2},
//                               {1, 2, 1, 2, 2},
//                               {1, 2, 1, 2, 2},
//                               {1, 2, 1, 2, 2}};
//
//        Pattern<float> kernCv = {{1, 2, 1, 2, 3},
//                                 {2, 4, 2, 1, 2},
//                                 {1, 2, 1, 2, 2},
//                                 {1, 2, 1, 2, 2},
//                                 {1, 2, 1, 2, 2}};

        vx_int16 kern[3][3] = {{1, 2,  1},
                               {2, 40, 2},
                               {1, 2,  1}};

        Pattern<float> kernCv = {{1, 2,  1},
                                 {2, 40, 2},
                                 {1, 2,  1}};

        Pattern<float> datx = data.ConvertTo<float>();

        cv::Mat exp = cv::Mat(cv::Size{datx.getWidth() / 2, datx.getHeight() / 2}, CV_32FC1);
        cv::filter2D(datx.getMat(), exp, datx.getMat().depth(), kernCv.getMat() / 16, {-1, -1}, 0, cv::BORDER_REPLICATE);
        Pattern<float> expected(exp);

        vx_image input = vxCreateImage(this->context, data.getVxWidth(), data.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);
        vx_convolution convolution = vxCreateConvolution(this->context, kernCv.getVxWidth(), kernCv.getVxHeight());
        vxCopyConvolutionCoefficients(convolution, &kern, VX_WRITE_ONLY, VX_MEMORY_TYPE_HOST);
        vx_uint32 scale = 16;
        vxSetConvolutionAttribute(convolution, VX_CONVOLUTION_SCALE, &scale, sizeof(scale));
        vx_image output = vxCreateImage(this->context, expected.getVxWidth(), expected.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);

        Convert::CvToVx(data.getMat(), input, this->context, true);
        Convert::CvToVx(cv::Mat::zeros(expected.getSize(), CV_8UC1), output, this->context, true);

        vx_node node = vxConvolveNode(this->graph, input, convolution, output);

        vx_border_t border;
        border.mode = vx_border_e::VX_BORDER_REPLICATE;
        ASSERT_EQ(vx_status_e::VX_SUCCESS, vxSetNodeAttribute(node, VX_NODE_BORDER, &border, sizeof(border)));

        this->Run();

        cv::Mat actual;
        Convert::VxToCv(output, actual, this->context);
        ASSERT_EQ(expected.CastTo<uint8_t>(), actual);
    }

    TEST_F(VxNodeInOutTest, testVxColorConvert) {
        // TODO(nxa33894) node input size > [16,16] ONLY
        Pattern<uint8_t> r(16, 16);
        Pattern<uint8_t> g(16, 16);
        Pattern<uint8_t> b(16, 16);

        std::vector<cv::Mat> vectData = {b.getMat(), g.getMat(), r.getMat()};
        cv::Mat data;
        cv::merge(vectData, data);

        vx_image input = vxCreateImage(this->context, r.getVxWidth(), r.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_RGB);
        vx_image output = vxCreateImage(this->context, r.getVxWidth(), r.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_YUV4);
        vx_image outY = vxCreateImage(this->context, r.getVxWidth(), r.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);
        Convert::CvToVx(data, input, this->context, true);

        vxColorConvertNode(this->graph, input, output);
        vxChannelExtractNode(this->graph, output, vx_channel_e::VX_CHANNEL_Y, outY);

        this->Run();

        cv::Mat actual;
        Convert::VxToCv(outY, actual, this->context);

        cv::Mat exp;
        cv::cvtColor(data, exp, CV_BGR2YUV);
        cv::extractChannel(exp, data, 0);
        Pattern<char> expected(data);
        ASSERT_EQ(expected, actual);
    }

    TEST_F(VxNodeInOutTest, testVxGaussianFilter) {
        Pattern<uint8_t> data(10, 10);

        Pattern<float> kernCv = {{1.0f, 2.0f, 1.0f},
                                 {2.0f, 4.0f, 2.0f},
                                 {1.0f, 2.0f, 1.0f}};

        Pattern<float> dataCv = data.ConvertTo<float>();
        cv::Mat exp = cv::Mat(cv::Size{dataCv.getWidth() / 2, dataCv.getHeight() / 2}, CV_32F);
        cv::filter2D(dataCv.getMat(), exp, dataCv.getMat().depth(), kernCv.getMat() / 16, {-1, -1}, 0, cv::BORDER_REPLICATE);
        Pattern<float> expected(exp);

        vx_image input = vxCreateImage(this->context, data.getVxWidth(), data.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);
        vx_image output = vxCreateImage(this->context, expected.getVxWidth(), expected.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);

        Convert::CvToVx(data.getMat(), input, this->context, true);
        Convert::CvToVx(cv::Mat::zeros(expected.getSize(), CV_8UC1), output, this->context, true);

        vx_node node = vxGaussian3x3Node(this->graph, input, output);
        vx_border_t border;
        border.mode = vx_border_e::VX_BORDER_REPLICATE;
        ASSERT_EQ(vx_status_e::VX_SUCCESS, vxSetNodeAttribute(node, VX_NODE_BORDER, &border, sizeof(border)));

        this->Run();

        cv::Mat actual;
        Convert::VxToCv(output, actual, this->context);

        ASSERT_EQ(expected.CastTo<uint8_t>(), actual);
    }

    TEST_F(VxNodeInOutTest, testVxBoxFiler) {
        // TODO(nxa33894) filters in ocv are calculated internally and than casted while cv rounded from float
        Pattern<uint8_t> data = {{1, 2,  3,  4},
                                 {5, 6,  7,  8},
                                 {9, 10, 11, 12}};

        Pattern<float> dataCv = data.ConvertTo<float>();
        cv::Mat exp = cv::Mat::zeros(dataCv.getMat().size(), dataCv.getMat().type());
        cv::boxFilter(dataCv.getMat(), exp, dataCv.getMat().depth(), {3, 3}, {-1, -1}, true, cv::BORDER_REPLICATE);
        Pattern<float> expected(exp);

        vx_image input = vxCreateImage(this->context, data.getVxWidth(), data.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);
        vx_image output = vxCreateImage(this->context, expected.getVxWidth(), expected.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);
        Convert::CvToVx(data.getMat(), input, this->context, true);
        Convert::CvToVx(cv::Mat::zeros(expected.getSize(), CV_8UC1), output, this->context, true);

        vx_node node = vxBox3x3Node(this->graph, input, output);
        vx_border_t border;
        border.mode = vx_border_e::VX_BORDER_REPLICATE;
        ASSERT_EQ(vx_status_e::VX_SUCCESS, vxSetNodeAttribute(node, VX_NODE_BORDER, &border, sizeof(border)));

        this->Run();

        cv::Mat actual;
        Convert::VxToCv(output, actual, this->context);

        ASSERT_EQ(expected.CastTo<uint8_t>(), actual);
    }

    TEST_F(VxNodeInOutTest, testVxScale) {
        Pattern<uint8_t> data = {{1,  2,  3,  4},
                                 {5,  6,  7,  8},
                                 {9,  10, 11, 12},
                                 {13, 14, 15, 16}};

        cv::Mat exp = cv::Mat(cv::Size{2, 2}, CV_32F);
        cv::resize(data.ConvertTo<float>().getMat(), exp, exp.size(), 0, 0, cv::INTER_LINEAR);
        Pattern<float> expected(exp);

        vx_image input = vxCreateImage(this->context, data.getVxWidth(), data.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);
        Convert::CvToVx(data.getMat(), input, this->context, true);

        vx_image output = vxCreateImage(this->context, expected.getVxWidth(), expected.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);
        vxScaleImageNode(this->graph, input, output, vx_interpolation_type_e::VX_INTERPOLATION_BILINEAR);

        this->Run();

        cv::Mat actual;
        Convert::VxToCv(output, actual, this->context);

        ASSERT_EQ(expected.CastTo<uint8_t>(), actual);
    }

    TEST_F(VxNodeInOutTest, testVxFastCorners) {
        // needs circle of 16pixels around corner
        // TODO(nxa33894) input must be greater than 6x6
        Pattern<uint8_t> data = {{10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10},
                                 {10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10},
                                 {10, 10, 10, 10, 10, 8,  10, 10, 10, 10, 10, 10},
                                 {10, 10, 10, 10, 8,  10, 8,  10, 10, 10, 10, 10},
                                 {10, 10, 10, 10, 8,  10, 8,  10, 10, 10, 10, 10},
                                 {10, 10, 10, 8,  10, 10, 10, 8,  10, 10, 10, 10},
                                 {10, 10, 10, 8,  10, 10, 10, 8,  10, 10, 10, 10},
                                 {10, 10, 4,  8,  10, 10, 10, 10, 4,  10, 10, 10},
                                 {10, 10, 4,  10, 10, 10, 10, 10, 4,  10, 10, 10},
                                 {10, 3,  4,  10, 10, 10, 10, 10, 4,  3,  10, 10},
                                 {10, 3,  10, 10, 10, 10, 10, 10, 10, 3,  10, 10},
                                 {10, 3,  10, 10, 10, 10, 10, 10, 10, 10, 3,  10}};

        std::vector<cv::KeyPoint> expected;
        cv::FAST(data.getMat(), expected, 3, false);

        vx_image input = vxCreateImage(this->context, data.getVxWidth(), data.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);
        Convert::CvToVx(data.getMat(), input, this->context, true);

        vx_float32 strengthThreshVal = 3.0f;
        vx_scalar strengthThresh = vxCreateScalar(this->context, VX_TYPE_FLOAT32, &strengthThreshVal);

        vx_array corners = vxCreateArray(this->context, VX_TYPE_KEYPOINT, 100);

        vxFastCornersNode(this->graph, input, strengthThresh, _vx_bool_e::vx_false_e, corners, nullptr);

        this->Run();

        vx_size numItems = 0;
        vxQueryArray(corners, VX_ARRAY_NUMITEMS, &numItems, sizeof(numItems));

        ASSERT_EQ(expected.size(), (unsigned)numItems);

        vx_map_id mapId;
        vx_size stride = sizeof(vx_keypoint_t);
        void *base = nullptr;
        vxMapArrayRange(corners, 0, numItems, &mapId, &stride, &base, vx_accessor_e::VX_READ_ONLY,
                        vx_memory_type_e::VX_MEMORY_TYPE_HOST, VX_NOGAP_X);

        for (vx_size i = 0; i < numItems; i++) {
            auto key = reinterpret_cast<vx_keypoint_t *>(base)[i];
            auto expKey = expected[i];

            ASSERT_EQ(expKey.pt.x, key.x);
            ASSERT_EQ(expKey.pt.y, key.y);
        }
    }

    TEST_F(VxNodeInOutTest, DISABLED_testVxCannyEdgeDetector) {
        // TODO(nxa33894) opencv uses gaussian filter inside canny
        Pattern<uint8_t> dat = {{1, 1, 1, 5},
                                {1, 1, 5, 1},
                                {1, 5, 1, 1}};

        cv::Mat exp;
        cv::Canny(dat.getMat(), exp, 3, 10);
        Pattern<uint8_t> expected(exp);

        vx_image input = vxCreateImage(this->context, 4, 3, vx_df_image_e::VX_DF_IMAGE_U8);
        Convert::CvToVx(dat.getMat(), input, this->context, true);

        vx_image output = vxCreateImage(this->context, 4, 3, vx_df_image_e::VX_DF_IMAGE_U8);
        vx_uint8 lo = 3;
        vx_uint8 hi = 10;

#if VX_VERSION == VX_VERSION_1_1
        vx_threshold hyst = vxCreateThreshold(this->context, VX_THRESHOLD_TYPE_RANGE, VX_TYPE_UINT8);

        vxSetThresholdAttribute(hyst, VX_THRESHOLD_THRESHOLD_LOWER, &lo, sizeof(lo));
        vxSetThresholdAttribute(hyst, VX_THRESHOLD_THRESHOLD_UPPER, &hi, sizeof(hi));
#elif VX_VERSION == VX_VERSION_1_2
        vx_threshold hyst = vxCreateThresholdForImage(this->context, vx_threshold_type_e::VX_THRESHOLD_TYPE_RANGE,
                                                      vx_df_image_e::VX_DF_IMAGE_U8, vx_df_image_e::VX_DF_IMAGE_U8);
        vx_pixel_value_t p1, p2;
        p1.U8 = lo;
        p2.U8 = hi;
        vxCopyThresholdRange(hyst, &p1, &p2, vx_accessor_e::VX_WRITE_ONLY, vx_memory_type_e::VX_MEMORY_TYPE_HOST);
#endif
        vx_int32 gradient_size = 3;
        vx_enum norm_type = VX_NORM_L1;

        vxCannyEdgeDetectorNode(this->graph, input, hyst, gradient_size, norm_type, output);

        this->Run();

        cv::Mat actual;
        Convert::VxToCv(output, actual, this->context);

        ASSERT_EQ(expected, actual);
    }

    TEST_F(VxNodeInOutTest, DISABLED_testVxHarris) {
        Pattern<uint8_t> data = {{10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10},
                                 {10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10},
                                 {10, 10, 10, 10, 10, 8,  10, 10, 10, 10, 10, 10},
                                 {10, 10, 10, 10, 8,  10, 8,  10, 10, 10, 10, 10},
                                 {10, 10, 10, 10, 8,  10, 8,  10, 10, 10, 10, 10},
                                 {10, 10, 10, 8,  10, 10, 10, 8,  10, 10, 10, 10},
                                 {10, 10, 10, 8,  10, 10, 10, 8,  10, 10, 10, 10},
                                 {10, 10, 4,  8,  10, 10, 10, 10, 4,  10, 10, 10},
                                 {10, 10, 4,  10, 10, 10, 10, 10, 4,  10, 10, 10},
                                 {10, 3,  4,  10, 10, 10, 10, 10, 4,  3,  10, 10},
                                 {10, 3,  10, 10, 10, 10, 10, 10, 10, 3,  10, 10},
                                 {10, 3,  10, 10, 10, 10, 10, 10, 10, 10, 3,  10}};

        cv::Mat exp;
        cv::cornerHarris(data.getMat(), exp, 3, 3, 0.04, cv::BORDER_REPLICATE);
        Pattern<uint8_t> expected(exp);

        vx_image input = vxCreateImage(this->context, data.getVxWidth(), data.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);
        Convert::CvToVx(data.getMat(), input, this->context, true);

        vx_float32 strengthThreshVal = 100.0f;
        vx_float32 minDistanceVal = 2.0f;
        vx_float32 sensitivityVal = 0.04f;

        vx_size numCornersVal = 100;

        vx_scalar strengthThresh = vxCreateScalar(this->context, VX_TYPE_FLOAT32, &strengthThreshVal);
        vx_scalar minDistance = vxCreateScalar(this->context, VX_TYPE_FLOAT32, &minDistanceVal);
        vx_scalar sensitivity = vxCreateScalar(this->context, VX_TYPE_FLOAT32, &sensitivityVal);
        vx_scalar numCorners = vxCreateScalar(this->context, VX_TYPE_SIZE, &numCornersVal);

        vx_array corners = vxCreateArray(this->context, VX_TYPE_KEYPOINT, numCornersVal);

        vxHarrisCornersNode(this->graph, input, strengthThresh, minDistance, sensitivity, 3, 3, corners, nullptr);

        this->Run();

        vxCopyScalar(numCorners, &numCornersVal, vx_accessor_e::VX_READ_ONLY, vx_memory_type_e::VX_MEMORY_TYPE_HOST);

        std::cout << numCornersVal << std::endl;

        vx_size num_items = 0;
        vxQueryArray(corners, VX_ARRAY_NUMITEMS, &num_items, sizeof(num_items));
        std::cout << num_items << std::endl;


        std::cout << expected << std::endl;



//        cv::Mat actual;
//        Convert::VxToCv(output, actual, this->context);

//        ASSERT_EQ(expected, 0);
    }

    TEST_F(VxNodeInOutTest, DISABLED_testVxOpticalFlowLK) {
        Pattern<uint8_t> dat0 = {{10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10},
                                 {10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10},
                                 {10, 10, 10, 10, 10, 8,  10, 10, 10, 10, 10, 10},
                                 {10, 10, 10, 10, 8,  10, 8,  10, 10, 10, 10, 10},
                                 {10, 10, 10, 10, 8,  10, 8,  10, 10, 10, 10, 10},
                                 {10, 10, 10, 8,  10, 10, 10, 8,  10, 10, 10, 10},
                                 {10, 10, 10, 8,  10, 10, 10, 8,  10, 10, 10, 10},
                                 {10, 10, 4,  8,  10, 10, 10, 10, 4,  10, 10, 10},
                                 {10, 10, 4,  10, 10, 10, 10, 10, 4,  10, 10, 10},
                                 {10, 3,  4,  10, 10, 10, 10, 10, 4,  3,  10, 10},
                                 {10, 3,  10, 10, 10, 10, 10, 10, 10, 3,  10, 10},
                                 {10, 3,  10, 10, 10, 10, 10, 10, 10, 10, 3,  10}};
        // same pattern moved +1 column, -1 row
        Pattern<uint8_t> dat1 = {{10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10},
                                 {10, 10, 10, 10, 10, 10, 8,  10, 10, 10, 10, 10},
                                 {10, 10, 10, 10, 10, 8,  10, 8,  10, 10, 10, 10},
                                 {10, 10, 10, 10, 10, 8,  10, 8,  10, 10, 10, 10},
                                 {10, 10, 10, 10, 8,  10, 10, 10, 8,  10, 10, 10},
                                 {10, 10, 10, 10, 8,  10, 10, 10, 8,  10, 10, 10},
                                 {10, 10, 10, 4,  8,  10, 10, 10, 10, 4,  10, 10},
                                 {10, 10, 10, 4,  10, 10, 10, 10, 10, 4,  10, 10},
                                 {10, 10, 3,  4,  10, 10, 10, 10, 10, 4,  3,  10},
                                 {10, 10, 3,  10, 10, 10, 10, 10, 10, 10, 3,  10},
                                 {10, 10, 3,  10, 10, 10, 10, 10, 10, 10, 10, 3},
                                 {10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10}};

        std::vector<cv::Point2f> exp = {{1.99262333f, 7.87791443f}};

        cv::Mat actual;

        std::vector<cv::Point2f> prevPts = {};
        std::vector<cv::Point2f> nextPts = {{}};

        cv::goodFeaturesToTrack(dat0.getMat(), prevPts, 500, 0.01, 10);

        std::vector<uint8_t> status;
        std::vector<float> error;

        cv::calcOpticalFlowPyrLK(dat0.getMat(), dat1.getMat(), prevPts, nextPts, status, error);

        ASSERT_EQ(exp.size(), nextPts.size());
        for (std::size_t i = 0; i < exp.size(); i++) {
            ASSERT_EQ(exp[i].x, nextPts[i].x);
            ASSERT_EQ(exp[i].y, nextPts[i].y);
        }
    }

    TEST_F(VxNodeInOutTest, DISABLED_testVxGaussianImagePyramid) {
        // TODO(nxa33894) uses kernel size 5x5, so size must be greater
//        Pattern<char> data = {{2,2,2,2,2,2,2,2,2,2,2,2},
//                              {2,2,2,2,2,2,2,2,2,2,2,2},
//                              {2,2,2,2,2,2,2,2,2,2,2,2},
//                              {2,2,2,2,2,2,2,2,2,2,2,2},
//                              {2,2,2,2,2,2,2,2,2,2,2,2},
//                              {2,2,2,2,2,2,2,2,2,2,2,2},
//                              {2,2,2,2,2,2,2,2,2,2,2,2},
//                              {2,2,2,2,2,2,2,2,2,2,2,2},
//                              {2,2,2,2,2,2,2,2,2,2,2,2},
//                              {2,2,2,2,2,2,2,2,2,2,2,2},
//                              {2,2,2,2,2,2,2,2,2,2,2,2},
//                              {2,2,2,2,2,2,2,2,2,2,2,2}};
        Pattern<uint8_t> data(12, 12, 0, 10);

        Pattern<float> dataCv = data.ConvertTo<float>();

        // TODO(nxa33894) count gaussian pyramide step-by-step according to ovx...
        // because convolution and scale are computed internally without any control of rounding
        Pattern<float> convKernel = {{1, 4,  6,  4,  1},
                                     {4, 16, 24, 16, 4},
                                     {6, 24, 36, 24, 6},
                                     {4, 16, 24, 16, 4},
                                     {1, 4,  6,  4,  1}};
        convKernel /= 256.0f;

        auto getLevel = [&](const Pattern<float> &$input) -> Pattern<uint8_t> {
            cv::Mat tmp;
            cv::filter2D($input.getMat(), tmp, $input.getMat().depth(), convKernel.getMat(), {-1, -1}, 0, cv::BORDER_REPLICATE);

            Pattern<float> mid(tmp);
            auto cast = mid.CastTo<uint8_t>();

            cv::resize(cast.ConvertTo<float>().getMat(), tmp, {$input.getWidth() / 2, $input.getHeight() / 2}, 0, 0, cv::INTER_NEAREST);

            return Pattern<float>(tmp).CastTo<uint8_t>();
        };

        cv::Mat exp2L;
        cv::pyrDown(dataCv.getMat(), exp2L, {dataCv.getWidth() / 2, dataCv.getHeight() / 2}, cv::BORDER_REPLICATE);
        cv::Mat exp3L;
        cv::pyrDown(exp2L, exp3L, {dataCv.getWidth() / 4, dataCv.getHeight() / 4}, cv::BORDER_REPLICATE);

        Pattern<uint8_t> expected2L = getLevel(data.ConvertTo<float>());
        Pattern<uint8_t> expected3L = getLevel(expected2L.ConvertTo<float>());

        vx_image input = vxCreateImage(this->context, data.getVxWidth(), data.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);
        Convert::CvToVx(data.getMat(), input, this->context, true);

        vx_size levels = 3;
        vx_float32 scale = VX_SCALE_PYRAMID_HALF;
        vx_pyramid pyramid = vxCreatePyramid(this->context, levels, scale, data.getVxWidth(), data.getVxHeight(),
                                             vx_df_image_e::VX_DF_IMAGE_U8);

        ASSERT_EQ(vx_status_e::VX_SUCCESS, vxGetStatus((vx_reference)pyramid));

        vx_node node = vxGaussianPyramidNode(this->graph, input, pyramid);

        vx_border_t border;
        border.mode = vx_border_e::VX_BORDER_REPLICATE;
        ASSERT_EQ(vx_status_e::VX_SUCCESS, vxSetNodeAttribute(node, VX_NODE_BORDER, &border, sizeof(border)));

        this->Run();

        vx_image level1img = vxGetPyramidLevel(pyramid, 0);
        vx_image level2img = vxGetPyramidLevel(pyramid, 1);
        vx_image level3img = vxGetPyramidLevel(pyramid, 2);

        cv::Mat level1mat, level2mat, level3mat, level4mat;
        Convert::VxToCv(level1img, level1mat, this->context);
        Convert::VxToCv(level2img, level2mat, this->context);
        Convert::VxToCv(level3img, level3mat, this->context);

//        std::cout << level1mat << std::endl;
        std::cout << exp2L << std::endl;
        std::cout << expected2L << std::endl;
        std::cout << level2mat << std::endl;

        std::cout << std::endl;
        std::cout << expected3L << std::endl;
        std::cout << level3mat << std::endl;
    }

#if VX_VERSION == VX_VERSION_1_2

    TEST_F(VxNodeInOutTest, testVxDataObjectCopy) {
        Pattern<uint8_t> in1(4, 3);
        Pattern<uint8_t> in2(4, 3);

        in1.getMat().copyTo(in2.getMat());

        vx_image image1 = vxCreateImage(this->context, in1.getVxWidth(), in1.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);
        vx_image image2 = vxCreateImage(this->context, in2.getVxWidth(), in2.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);

        auto reference = (vx_reference)vxCopyNode(this->graph, (vx_reference)image1, (vx_reference)image2);
        vx_status status = vxGetStatus(reference);

        this->Run();

        ASSERT_EQ(Convert::CvToVx(in1.getMat(), image1, this->context, true), vx_status_e::VX_SUCCESS);
        ASSERT_EQ(Convert::CvToVx(in2.getMat(), image2, this->context, true), vx_status_e::VX_SUCCESS);
        ASSERT_EQ(status, vx_status_e::VX_SUCCESS);
    }

    TEST_F(VxNodeInOutTest, testVxNonMaximaSuppressionU8) {
        Pattern<uint8_t> in(4, 3);
        Pattern<uint8_t> m(4, 3);
        int winSize = 3;

        // doesn't seem like there's an actual implementation of this in ocv

        Pattern<uint8_t> expected(4, 3);

        vx_image image = vxCreateImage(this->context, in.getVxWidth(), in.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);
        vx_image mask = vxCreateImage(this->context, m.getVxWidth(), m.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);
        vx_image output = vxCreateImage(this->context, expected.getVxWidth(), expected.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);

        auto reference = (vx_reference)vxNonMaxSuppressionNode(this->graph, image, mask, (vx_int32)winSize, output);
        vx_status status = vxGetStatus(reference);

        ASSERT_EQ(Convert::CvToVx(in.getMat(), image, this->context, true), vx_status_e::VX_SUCCESS);
        ASSERT_EQ(Convert::CvToVx(m.getMat(), mask, this->context, true), vx_status_e::VX_SUCCESS);
        ASSERT_EQ(Convert::CvToVx(expected.getMat(), output, this->context, true), vx_status_e::VX_SUCCESS);
        ASSERT_EQ(status, vx_status_e::VX_SUCCESS);

        this->Run();

//        cv::Mat actual;
//        Convert::VxToCv(output, actual, this->context);
//
//        ASSERT_EQ(expected, actual);
    }

    TEST_F(VxNodeInOutTest, testVxNonMaximaSuppressionS16) {
        Pattern<int16_t> in(10, 10);
        Pattern<uint8_t> m(10, 10);
        int winSize = 9;

        // doesn't seem like there's an actual implementation of this in ocv

        Pattern<uint8_t> expected(10, 10);

        vx_image image = vxCreateImage(this->context, in.getVxWidth(), in.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_S16);
        vx_image mask = vxCreateImage(this->context, m.getVxWidth(), m.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);
        vx_image output = vxCreateImage(this->context, expected.getVxWidth(), expected.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_S16);

        auto reference = (vx_reference)vxNonMaxSuppressionNode(this->graph, image, mask, (vx_int32)winSize, output);
        vx_status status = vxGetStatus(reference);

        ASSERT_EQ(Convert::CvToVx(in.getMat(), image, this->context, true), vx_status_e::VX_SUCCESS);
        ASSERT_EQ(Convert::CvToVx(m.getMat(), mask, this->context, true), vx_status_e::VX_SUCCESS);
        ASSERT_EQ(Convert::CvToVx(expected.getMat(), output, this->context, true), vx_status_e::VX_SUCCESS);
        ASSERT_EQ(status, vx_status_e::VX_SUCCESS);

        this->Run();

//        cv::Mat actual;
//        Convert::VxToCv(output, actual, this->context);
//
//        ASSERT_EQ(expected, actual);
    }

    TEST_F(VxNodeInOutTest, testVxMinU8) {
        Pattern<uint8_t> in1(4, 3);
        Pattern<uint8_t> in2(4, 3);

        cv::Mat exp(cv::Size(3, 4), CV_8U);
        Pattern<uint8_t> expected(exp);

        cv::min(in1.getMat(), in2.getMat(), exp);

        vx_image image1 = vxCreateImage(this->context, in1.getVxWidth(), in1.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);
        vx_image image2 = vxCreateImage(this->context, in2.getVxWidth(), in2.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);
        vx_image output = vxCreateImage(this->context, expected.getVxWidth(), expected.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);

        auto reference = (vx_reference)vxMinNode(this->graph, image1, image2, output);
        vx_status status = vxGetStatus(reference);

        ASSERT_EQ(Convert::CvToVx(in1.getMat(), image1, this->context, true), vx_status_e::VX_SUCCESS);
        ASSERT_EQ(Convert::CvToVx(in2.getMat(), image2, this->context, true), vx_status_e::VX_SUCCESS);
        ASSERT_EQ(Convert::CvToVx(expected.getMat(), output, this->context, true), vx_status_e::VX_SUCCESS);
        ASSERT_EQ(status, vx_status_e::VX_SUCCESS);

        this->Run();

        cv::Mat actual;
        ASSERT_EQ(Convert::VxToCv(output, actual, this->context), vx_status_e::VX_SUCCESS);

        ASSERT_EQ(expected, actual);
    }

    TEST_F(VxNodeInOutTest, testVxMinS16) {
        Pattern<int16_t> in1(4, 3);
        Pattern<int16_t> in2(4, 3);

        cv::Mat exp(cv::Size(3, 4), CV_16S);
        Pattern<int16_t> expected(exp);

        cv::min(in1.getMat(), in2.getMat(), exp);

        vx_image image1 = vxCreateImage(this->context, in1.getVxWidth(), in1.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_S16);
        vx_image image2 = vxCreateImage(this->context, in2.getVxWidth(), in2.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_S16);
        vx_image output = vxCreateImage(this->context, expected.getVxWidth(), expected.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_S16);

        auto reference = (vx_reference)vxMinNode(this->graph, image1, image2, output);
        vx_status status = vxGetStatus(reference);

        ASSERT_EQ(Convert::CvToVx(in1.getMat(), image1, this->context, true), vx_status_e::VX_SUCCESS);
        ASSERT_EQ(Convert::CvToVx(in2.getMat(), image2, this->context, true), vx_status_e::VX_SUCCESS);
        ASSERT_EQ(Convert::CvToVx(expected.getMat(), output, this->context, true), vx_status_e::VX_SUCCESS);
        ASSERT_EQ(status, vx_status_e::VX_SUCCESS);

        this->Run();

        cv::Mat actual;
        ASSERT_EQ(Convert::VxToCv(output, actual, this->context), vx_status_e::VX_SUCCESS);

        ASSERT_EQ(expected, actual);
    }

    TEST_F(VxNodeInOutTest, testVxMaxU8) {
        Pattern<uint8_t> in1(4, 3);
        Pattern<uint8_t> in2(4, 3);

        cv::Mat exp(cv::Size(3, 4), CV_8U);
        Pattern<uint8_t> expected(exp);

        cv::max(in1.getMat(), in2.getMat(), exp);

        vx_image image1 = vxCreateImage(this->context, in1.getVxWidth(), in1.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);
        vx_image image2 = vxCreateImage(this->context, in2.getVxWidth(), in2.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);
        vx_image output = vxCreateImage(this->context, expected.getVxWidth(), expected.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);

        auto reference = (vx_reference)vxMaxNode(this->graph, image1, image2, output);
        vx_status status = vxGetStatus(reference);

        ASSERT_EQ(Convert::CvToVx(in1.getMat(), image1, this->context, true), vx_status_e::VX_SUCCESS);
        ASSERT_EQ(Convert::CvToVx(in2.getMat(), image2, this->context, true), vx_status_e::VX_SUCCESS);
        ASSERT_EQ(status, vx_status_e::VX_SUCCESS);

        this->Run();

        cv::Mat actual;
        ASSERT_EQ(Convert::VxToCv(output, actual, this->context), vx_status_e::VX_SUCCESS);

        ASSERT_EQ(expected, actual);
    }

    TEST_F(VxNodeInOutTest, testVxMaxS16) {
        Pattern<int16_t> in1(4, 3);
        Pattern<int16_t> in2(4, 3);

        cv::Mat exp(cv::Size(3, 4), CV_16S);
        Pattern<int16_t> expected(exp);

        cv::max(in1.getMat(), in2.getMat(), exp);

        vx_image image1 = vxCreateImage(this->context, in1.getVxWidth(), in1.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_S16);
        vx_image image2 = vxCreateImage(this->context, in2.getVxWidth(), in2.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_S16);
        vx_image output = vxCreateImage(this->context, expected.getVxWidth(), expected.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_S16);

        auto reference = (vx_reference)vxMaxNode(this->graph, image1, image2, output);
        vx_status status = vxGetStatus(reference);

        ASSERT_EQ(Convert::CvToVx(in1.getMat(), image1, this->context, true), vx_status_e::VX_SUCCESS);
        ASSERT_EQ(Convert::CvToVx(in2.getMat(), image2, this->context, true), vx_status_e::VX_SUCCESS);
        ASSERT_EQ(status, vx_status_e::VX_SUCCESS);

        this->Run();

        cv::Mat actual;
        ASSERT_EQ(Convert::VxToCv(output, actual, this->context), vx_status_e::VX_SUCCESS);

        ASSERT_EQ(expected, actual);
    }

    TEST_F(VxNodeInOutTest, testVxBilateralFilter) {
        Pattern<uint8_t> in(4, 3);
        Pattern<uint8_t> out(4, 3);

        /*
         * from openCV docs:
         * Filter size: Large filters (d > 5) are very slow, so it is recommended to use d=5 for real-time applications,
         * and perhaps d=9 for offline applications that need heavy noise filtering.
         *
         * Sigma values: For simplicity, you can set the 2 sigma values to be the same. If they are small (< 10),
         * the filter will not have much effect, whereas if they are large (> 150),
         * they will have a very strong effect, making the image look “cartoonish”.
         *
         */
        int diameter = 5;
        double sigmaSpace = 10;
        double sigmaValue = 10;

        cv::bilateralFilter(in.getMat(), out.getMat(), diameter, sigmaSpace, sigmaValue);

        // TODO(Marta to Martin): do that for openVX on tensors
    }

    TEST_F(VxNodeInOutTest, testVxMatchTemplate) {
        Pattern<uint8_t> in(10, 10);
        Pattern<uint8_t> temp(5, 5);

        cv::Mat res(cv::Size(6, 6), CV_32FC1);
        Pattern<float> result(res);

        cv::Mat exp(cv::Size(6, 6), CV_16S);
        Pattern<int16_t> expected(exp);

        Pattern<float> mask(5, 5);

        cv::matchTemplate(in.getMat(), temp.getMat(), res, cv::TemplateMatchModes::TM_CCORR_NORMED, mask.getMat());

        vx_enum matchingMethod = vx_comp_metric_e::VX_COMPARE_CCORR_NORM;

        vx_image image = vxCreateImage(this->context, in.getVxWidth(), in.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);
        vx_image templateImage = vxCreateImage(this->context, temp.getVxWidth(), temp.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);
        vx_image output = vxCreateImage(this->context, expected.getVxWidth(), expected.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_S16);

        auto reference = (vx_reference)vxMatchTemplateNode(this->graph, image, templateImage, matchingMethod, output);
        vx_status status = vxGetStatus(reference);

        ASSERT_EQ(Convert::CvToVx(in.getMat(), image, this->context, true), vx_status_e::VX_SUCCESS);
        ASSERT_EQ(Convert::CvToVx(temp.getMat(), templateImage, this->context, true), vx_status_e::VX_SUCCESS);
        ASSERT_EQ(status, vx_status_e::VX_SUCCESS);

        this->Run();

        cv::Mat actual;
        ASSERT_EQ(Convert::VxToCv(output, actual, this->context), vx_status_e::VX_SUCCESS);

        // some conversion magic
        cv::Mat res1;
        res.convertTo(res1, CV_32FC1, 65535.0f / 2);
        result = res1;

        // it looks like some issue in ovx implementation because some values are (expected - 1) and who knows why
        // so check ovx to ocv with 1 LSB tolerance
        double minVal, maxVal;
        cv::minMaxLoc(result.ConvertTo<int16_t>().getMat() - actual, &minVal, &maxVal);
        ASSERT_EQ(1.0, maxVal);
        ASSERT_EQ(1.0, maxVal - minVal);
    }

    TEST_F(VxNodeInOutTest, testVxLBP) {
        Pattern<uint8_t> in(10, 10);

        cv::Mat exp(cv::Size(10, 10), CV_8U);
        Pattern<uint8_t> expected(exp);

        vx_enum format = vx_lbp_format_e::VX_LBP;
        int kernelSize = 3;

        vx_image image = vxCreateImage(this->context, in.getVxWidth(), in.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);
        vx_image output = vxCreateImage(this->context, expected.getVxWidth(), expected.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);

        auto reference = (vx_reference)vxLBPNode(this->graph, image, format, kernelSize, output);
        vx_status status = vxGetStatus(reference);

        ASSERT_EQ(Convert::CvToVx(in.getMat(), image, this->context, true), vx_status_e::VX_SUCCESS);
        ASSERT_EQ(status, vx_status_e::VX_SUCCESS);

        this->Run();

//        cv::Mat actual;
//        ASSERT_EQ(Convert::VxToCv(output, actual, this->context), vx_status_e::VX_SUCCESS);
//
//        ASSERT_EQ(expected, actual);
    }

    TEST_F(VxNodeInOutTest, testVxHoughLinesP) {
        Pattern<uint8_t> in(4, 3);

        vx_hough_lines_p_t params = {
                (vx_float32)1,
                (vx_float32)30 * CV_PI / 180,
                (vx_int32)50,
                (vx_int32)50,
                (vx_int32)10
        };

        cv::Mat exp;
        Pattern<uint8_t> expected(exp);

        cv::HoughLinesP(in.getMat(), exp, 1, 30 * CV_PI / 180, 50, 50, 10);

        vx_image image = vxCreateImage(this->context, in.getVxWidth(), in.getVxHeight(), vx_df_image_e::VX_DF_IMAGE_U8);
        vx_array lines = vxCreateArray(this->context, VX_TYPE_LINE_2D, 1024);
        vx_size value = 0;
        vx_scalar linesCount = vxCreateScalar(this->context, VX_TYPE_SIZE, &value);

        auto reference = (vx_reference)vxHoughLinesPNode(this->graph, image, &params, lines, linesCount);
        vx_status status = vxGetStatus(reference);

        ASSERT_EQ(Convert::CvToVx(in.getMat(), image, this->context, true), vx_status_e::VX_SUCCESS);
        ASSERT_EQ(status, vx_status_e::VX_SUCCESS);

        this->Run();

//        cv::Mat actual;
//        Convert::VxToCv(lines, actual, this->context);
//
//        ASSERT_EQ(expected, actual);
    }

#endif
}
