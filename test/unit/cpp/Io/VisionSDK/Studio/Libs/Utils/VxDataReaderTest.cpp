/* ********************************************************************************************************* *
*
* Copyright (c) 2017-2019 NXP
*
* SPDX-License-Identifier: BSD-3-Clause
* The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
* or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
*
* ********************************************************************************************************* */

#include <fstream>
#include "../sourceFilesMap.hpp"

namespace Io::VisionSDK::Studio::Libs::Utils {

    class VxDataReaderTest : public testing::Test {
     public:
        // TODO(B58341) in all these examples the return value of creating a VX object is not checked - todo
        template<typename T>
        static void createIntegralTypeArray(std::map<string, vx_reference> &$vxDataMap, vx_context $context, size_t $size,
                                            const string &$name, vx_enum $type) {
            $vxDataMap[$name] = (vx_reference)vxCreateArray($context, $type, $size);
            std::vector<T> arr;
            arr.resize($size);
            for (unsigned i = 0; i < $size; i++) {
                arr[i] = static_cast<T>(i);
            }

            vxAddArrayItems((vx_array)$vxDataMap[$name], $size, (const void *)arr.data(), sizeof(T));
        }

        static void createKeyPointsArray(std::map<string, vx_reference> &$vxDataMap, vx_context $context, size_t $size,
                                         const string &$name) {
            $vxDataMap[$name] = (vx_reference)vxCreateArray($context, VX_TYPE_KEYPOINT, $size);
            std::vector<vx_keypoint_t> arr;
            arr.resize($size);
            for (unsigned i = 0; i < $size; i++) {
                arr[i] = vx_keypoint_t{1, 1, static_cast<vx_float32>(1.2f + i), static_cast<vx_float32>(1.2f + i),
                                       static_cast<vx_float32>(1.2f + i), 1, static_cast<vx_float32>(1.2f + i)};
            }

            vxAddArrayItems((vx_array)$vxDataMap[$name], $size, (const void *)arr.data(), sizeof(vx_keypoint_t));
        }

        static void createRectangleArray(std::map<string, vx_reference> &$vxDataMap, vx_context $context, size_t $size,
                                         const string &$name) {
            $vxDataMap[$name] = (vx_reference)vxCreateArray($context, VX_TYPE_RECTANGLE, $size);
            auto *arr = new vx_rectangle_t[$size];
            for (unsigned i = 0; i < $size; i++) {
                arr[i] = vx_rectangle_t{i, i, i, i};
            }

            vx_status stat = vxAddArrayItems((vx_array)$vxDataMap[$name], $size, (const void *)arr, sizeof(vx_rectangle_t));
            delete[] arr;
        }

        template<typename T>
        static void createLUT(std::map<string, vx_reference> &$vxDataMap, vx_context $context, size_t $size, const string &$name,
                              vx_enum $type) {
            std::vector<T> data;
            data.resize($size);
            for (size_t i = 0; i < $size; i++) {
                data[i] = static_cast<T>(i);
            }

            $vxDataMap[$name] = (vx_reference)vxCreateLUT($context, $type, $size);
#if VX_VERSION == VX_VERSION_1_0
            vxAccessLUT((vx_lut)$vxDataMap[$name], reinterpret_cast<void **>(&data[0]),
                        VX_WRITE_ONLY);
            vxCommitLUT((vx_lut)$vxDataMap[$name], reinterpret_cast<const void *>(data.data()));
#elif VX_VERSION == VX_VERSION_1_1 || VX_VERSION == VX_VERSION_1_2
            vxCopyLUT((vx_lut)$vxDataMap[$name], reinterpret_cast<void **>(&data[0]), VX_WRITE_ONLY, VX_MEMORY_TYPE_HOST);
#else
#error "createLUT not available for used OpenVX version"
#endif
        }

        static void createScalar(std::map<string, vx_reference> &$vxDataMap, vx_context $context, const string &$name, vx_enum $type,
                                 void *$ptr) {
#if VX_VERSION == VX_VERSION_1_0 || VX_VERSION == VX_VERSION_1_1
            if ($type > VX_TYPE_INVALID && $type < VX_TYPE_SCALAR_MAX) {
#elif VX_VERSION == VX_VERSION_1_2
            if ($type > VX_TYPE_INVALID && $type < VX_TYPE_VENDOR_STRUCT_END) {
#endif
                $vxDataMap[$name] = (vx_reference)vxCreateScalar($context, $type, $ptr);
            }
        }

        template<typename T>
        static void createMatrix(std::map<string, vx_reference> &$vxDataMap, vx_context $context, vx_size $cols, vx_size $rows,
                                 const string &$name, vx_enum $type) {
            vx_matrix mtx = vxCreateMatrix($context, $type, $cols, $rows);

            T *data = new T[$rows * $cols];
            for (vx_size i = 0; i < $rows * $cols; i++) {
                data[i] = (T)i;
            }

#if VX_VERSION == VX_VERSION_1_0
            vxWriteMatrix(mtx, (const void *)data);
#elif VX_VERSION == VX_VERSION_1_1 || VX_VERSION == VX_VERSION_1_2
            vxCopyMatrix(mtx, reinterpret_cast<void *>(data), VX_WRITE_ONLY, VX_MEMORY_TYPE_HOST);
#else
#error "createMatrix not availible for used OpenVX version"
#endif
            $vxDataMap[$name] = (vx_reference)mtx;

            delete[] data;
        }

        template<typename T>
        static void createConvolution(std::map<string, vx_reference> &$vxDataMap, vx_context $context, vx_size $cols, vx_size $rows,
                                      const string &$name, vx_enum $type) {
            vx_convolution conv = vxCreateConvolution($context, $cols, $rows);

            T *data = new T[$rows * $cols];
            for (vx_size i = 0; i < $rows * $cols; i++) {
                data[i] = (T)i;
            }

#if VX_VERSION == VX_VERSION_1_0
            vxWriteMatrix(conv, (const void *)data);
#elif VX_VERSION == VX_VERSION_1_1 || VX_VERSION == VX_VERSION_1_2
            vxCopyConvolutionCoefficients(conv, reinterpret_cast<void *>(data), VX_WRITE_ONLY, VX_MEMORY_TYPE_HOST);
#else
#error "createMatrix not availible for used OpenVX version"
#endif
            $vxDataMap[$name] = (vx_reference)conv;

            delete[] data;
        }

        template<typename T>
        static void createDistribution(std::map<string, vx_reference> &$vxDataMap, vx_context $context, vx_size $bins,
                                       const string &$name) {
            vx_distribution dist = vxCreateDistribution($context, $bins, 0, (vx_uint32)$bins);

            T *data = new T[$bins];
            for (vx_size i = 0; i < $bins; i++) {
                data[i] = (T)i;
            }

#if VX_VERSION == VX_VERSION_1_1 || VX_VERSION == VX_VERSION_1_2
            vxCopyDistribution(dist, reinterpret_cast<void *>(data), VX_WRITE_ONLY, VX_MEMORY_TYPE_HOST);
#else
#error "createDistribution not availible for used OpenVX version"
#endif
            $vxDataMap[$name] = (vx_reference)dist;
            delete[] data;
        }

        static void createRemap(std::map<string, vx_reference> &$vxDataMap, vx_context $context, vx_uint32 $w, vx_uint32 $h,
                                const string &$name) {
            vx_remap remap = vxCreateRemap($context, $w, $h, $w, $h);

#if VX_VERSION == VX_VERSION_1_0 || VX_VERSION == VX_VERSION_1_1
            for (vx_uint32 j = 0; j < $h; j++) {
                for (vx_uint32 i = 0; i < $w; i++) {
                    vxSetRemapPoint(remap, i, j, i * j, i * j);
                }
            }
#elif VX_VERSION == VX_VERSION_1_2
            vx_rectangle_t rect = {.start_x = 0, .start_y = 0, .end_x = $w, .end_y = $h};
            auto *coords = new vx_coordinates2df_t[$w * $h];
            for (vx_uint32 j = 0; j < $h; j++) {
                for (vx_uint32 i = 0; i < $w; i++) {
                    coords[i + (j * $w)] = {.x = static_cast<vx_float32>(i * j), .y = static_cast<vx_float32>(i * j)};
                }
            }
            vxCopyRemapPatch(remap, &rect, sizeof(vx_coordinates2d_t) * $w, coords, vx_type_e::VX_TYPE_COORDINATES2DF,
                             vx_accessor_e::VX_WRITE_ONLY, vx_memory_type_e::VX_MEMORY_TYPE_HOST);

            delete[] coords;
#else
#error "createRemap not availible for used OpenVX version"
#endif
            $vxDataMap[$name] = (vx_reference)remap;
        }

        static json readJsonFromFile(const string &$path) {
            std::ifstream data($path);
            try {
                return json::parse(data);
            } catch (const std::exception &ex) {
                return {};
            }
        }

#if VX_VERSION == VX_VERSION_1_0
        const string testDir = "test/resource/data/Io/VisionSDK/Studio/Libs/Utils/openvx_1.0";
#elif VX_VERSION == VX_VERSION_1_1
        const string testDir = "test/resource/data/Io/VisionSDK/Studio/Libs/Utils/openvx_1.1";
#elif VX_VERSION == VX_VERSION_1_2
        const string testDir = "test/resource/data/Io/VisionSDK/Studio/Libs/Utils/openvx_1.2";
#else
#error "Specify path to test resources for used OpenVX version"
#endif

        vx_context context{};

     protected:
        void SetUp() override {
            vx_status status;

            this->context = vxCreateContext();

            status = vxGetStatus((vx_reference)this->context);
            if (status != VX_SUCCESS) {
                GTEST_FATAL_FAILURE_((": Can not load context due to error: " + std::to_string(status)).c_str());
            }
        }

        void TearDown() override {
            vxReleaseContext(&this->context);
        }
    };  // NOLINT(readability/braces) FALSE-POSITIVE?

    TEST_F(VxDataReaderTest, ReadArray) {
        std::map<string, vx_reference> vxDataMap = {};
        const size_t numItems = 10;

        createIntegralTypeArray<int8_t>(vxDataMap, this->context, numItems, "vxArray0", VX_TYPE_INT8);
        createIntegralTypeArray<int16_t>(vxDataMap, this->context, numItems, "vxArray1", VX_TYPE_INT16);
        createIntegralTypeArray<int32_t>(vxDataMap, this->context, numItems, "vxArray2", VX_TYPE_INT32);
        createIntegralTypeArray<int64_t>(vxDataMap, this->context, numItems, "vxArray3", VX_TYPE_INT64);
        createIntegralTypeArray<vx_char>(vxDataMap, this->context, numItems, "vxArray4", VX_TYPE_CHAR);
        createIntegralTypeArray<vx_float32>(vxDataMap, this->context, numItems, "vxArray5", VX_TYPE_FLOAT32);
        createIntegralTypeArray<vx_float64>(vxDataMap, this->context, numItems, "vxArray6", VX_TYPE_FLOAT64);
        createIntegralTypeArray<uint8_t>(vxDataMap, this->context, numItems, "vxArray7", VX_TYPE_UINT8);
        createIntegralTypeArray<uint16_t>(vxDataMap, this->context, numItems, "vxArray8", VX_TYPE_UINT16);
        createIntegralTypeArray<uint32_t>(vxDataMap, this->context, numItems, "vxArray9", VX_TYPE_UINT32);
        createIntegralTypeArray<uint64_t>(vxDataMap, this->context, numItems, "vxArray10", VX_TYPE_UINT64);
        createIntegralTypeArray<size_t>(vxDataMap, this->context, numItems, "vxArray11", VX_TYPE_SIZE);

        ASSERT_EQ_JSON(VxDataReader::Process("vxArray0", vxDataMap), readJsonFromFile(this->testDir + "/vxArray/vxArray0.json"));
        ASSERT_EQ_JSON(VxDataReader::Process("vxArray1", vxDataMap), readJsonFromFile(this->testDir + "/vxArray/vxArray1.json"));
        ASSERT_EQ_JSON(VxDataReader::Process("vxArray2", vxDataMap), readJsonFromFile(this->testDir + "/vxArray/vxArray2.json"));
        ASSERT_EQ_JSON(VxDataReader::Process("vxArray3", vxDataMap), readJsonFromFile(this->testDir + "/vxArray/vxArray3.json"));
        ASSERT_EQ_JSON(VxDataReader::Process("vxArray4", vxDataMap), readJsonFromFile(this->testDir + "/vxArray/vxArray4.json"));
        ASSERT_EQ_JSON(VxDataReader::Process("vxArray5", vxDataMap), readJsonFromFile(this->testDir + "/vxArray/vxArray5.json"));
        ASSERT_EQ_JSON(VxDataReader::Process("vxArray6", vxDataMap), readJsonFromFile(this->testDir + "/vxArray/vxArray6.json"));
        ASSERT_EQ_JSON(VxDataReader::Process("vxArray7", vxDataMap), readJsonFromFile(this->testDir + "/vxArray/vxArray7.json"));
        ASSERT_EQ_JSON(VxDataReader::Process("vxArray8", vxDataMap), readJsonFromFile(this->testDir + "/vxArray/vxArray8.json"));
        ASSERT_EQ_JSON(VxDataReader::Process("vxArray9", vxDataMap), readJsonFromFile(this->testDir + "/vxArray/vxArray9.json"));
        ASSERT_EQ_JSON(VxDataReader::Process("vxArray10", vxDataMap), readJsonFromFile(this->testDir + "/vxArray/vxArray10.json"));
        ASSERT_EQ_JSON(VxDataReader::Process("vxArray11", vxDataMap), readJsonFromFile(this->testDir + "/vxArray/vxArray11.json"));

        for (auto elem : vxDataMap) {
            vxReleaseArray(reinterpret_cast<vx_array *>(&elem.second));
        }
    }

    TEST_F(VxDataReaderTest, ReadKeypointsArray) {
        std::map<string, vx_reference> vxDataMap = {};
        const size_t numItems = 10;

        createKeyPointsArray(vxDataMap, this->context, numItems, "vxArray12");

        ASSERT_EQ_JSON(VxDataReader::Process("vxArray12", vxDataMap), readJsonFromFile(this->testDir + "/vxArray/vxArray12.json"));

        for (auto elem : vxDataMap) {
            vxReleaseArray(reinterpret_cast<vx_array *>(&elem.second));
        }
    }

    TEST_F(VxDataReaderTest, ReadRectangleArray) {
        std::map<string, vx_reference> vxDataMap = {};
        const size_t numItems = 10;

        createRectangleArray(vxDataMap, this->context, numItems, "vxArray13");

        ASSERT_EQ_JSON(VxDataReader::Process("vxArray13", vxDataMap), readJsonFromFile(this->testDir + "/vxArray/vxArray13.json"));

        for (auto elem : vxDataMap) {
            vxReleaseArray(reinterpret_cast<vx_array *>(&elem.second));
        }
    }

    TEST_F(VxDataReaderTest, ReadLUT) {
        std::map<string, vx_reference> vxDataMap = {};

        createLUT<vx_uint8>(vxDataMap, this->context, 20, "vxLut0", VX_TYPE_UINT8);
        createLUT<vx_int16>(vxDataMap, this->context, 20, "vxLut1", VX_TYPE_INT16);

        ASSERT_EQ_JSON(VxDataReader::Process("vxLut0", vxDataMap), readJsonFromFile(this->testDir + "/vxLut/vxLut0.json"));
        ASSERT_EQ_JSON(VxDataReader::Process("vxLut1", vxDataMap), readJsonFromFile(this->testDir + "/vxLut/vxLut1.json"));

        for (auto elem : vxDataMap) {
            vxReleaseLUT(reinterpret_cast<vx_lut *>(&elem.second));
        }
    }

    TEST_F(VxDataReaderTest, ReadScalar) {
        std::map<string, vx_reference> vxDataMap = {};

        vx_int32 val0 = 44;
        createScalar(vxDataMap, this->context, "vxScalar0", VX_TYPE_INT32, &val0);
        vx_float32 val1 = 442.55f;
        createScalar(vxDataMap, this->context, "vxScalar1", VX_TYPE_FLOAT32, &val1);
        vx_char val2 = 'b';
        createScalar(vxDataMap, this->context, "vxScalar2", VX_TYPE_CHAR, &val2);
        vx_float64 val3 = 12.22;
        createScalar(vxDataMap, this->context, "vxScalar3", VX_TYPE_FLOAT64, &val3);
        vx_uint64 val4 = 22;
        createScalar(vxDataMap, this->context, "vxScalar4", VX_TYPE_UINT64, &val4);
        vx_bool val5 = _vx_bool_e::vx_true_e;
        createScalar(vxDataMap, this->context, "vxScalar5", VX_TYPE_BOOL, &val5);

        ASSERT_EQ_JSON(VxDataReader::Process("vxScalar0", vxDataMap),
                       R"({"type":"VX_TYPE_SCALAR","value":44,"valueType":"VX_TYPE_INT32"})"_json);
        ASSERT_EQ_JSON(VxDataReader::Process("vxScalar1", vxDataMap),
                       R"({"type":"VX_TYPE_SCALAR","value":442.549987792969,"valueType":"VX_TYPE_FLOAT32"})"_json);
        ASSERT_EQ_JSON(VxDataReader::Process("vxScalar2", vxDataMap),
                       R"({"type":"VX_TYPE_SCALAR","value":98,"valueType":"VX_TYPE_CHAR"})"_json);
        ASSERT_EQ_JSON(VxDataReader::Process("vxScalar3", vxDataMap),
                       R"({"type":"VX_TYPE_SCALAR","value":12.22,"valueType":"VX_TYPE_FLOAT64"})"_json);
        ASSERT_EQ_JSON(VxDataReader::Process("vxScalar4", vxDataMap),
                       R"({"type":"VX_TYPE_SCALAR","value":22,"valueType":"VX_TYPE_UINT64"})"_json);
        ASSERT_EQ_JSON(VxDataReader::Process("vxScalar5", vxDataMap),
                       R"({"type":"VX_TYPE_SCALAR","value":1,"valueType":"VX_TYPE_BOOL"})"_json);

        for (auto elem : vxDataMap) {
            vxReleaseScalar(reinterpret_cast<vx_scalar *>(&elem.second));
        }
    }

    TEST_F(VxDataReaderTest, ReadThreshold) {
        std::map<string, vx_reference> vxDataMap = {};

        vx_threshold_attribute_e thresholdAttribute;
#if VX_VERSION == VX_VERSION_1_0
        thresholdAttribute = VX_THRESHOLD_ATTRIBUTE_THRESHOLD_UPPER;
#elif VX_VERSION == VX_VERSION_1_1
        thresholdAttribute = VX_THRESHOLD_THRESHOLD_UPPER;
#endif

#if VX_VERSION == VX_VERSION_1_0 || VX_VERSION == VX_VERSION_1_1
        vx_threshold thr = vxCreateThreshold(this->context, VX_THRESHOLD_TYPE_BINARY, VX_TYPE_UINT8);
        vxDataMap["vxThr0"] = (vx_reference)thr;
        vx_threshold thr1 = vxCreateThreshold(this->context, VX_THRESHOLD_TYPE_RANGE, VX_TYPE_UINT8);
        vx_int32 value = 15;
        vxSetThresholdAttribute(thr1, thresholdAttribute, &value, sizeof(value));
        vxDataMap["vxThr1"] = (vx_reference)thr1;
#else
        vx_threshold thr = vxCreateThresholdForImage(this->context, vx_threshold_type_e::VX_THRESHOLD_TYPE_BINARY,
                                                     vx_df_image_e::VX_DF_IMAGE_U8, vx_df_image_e::VX_DF_IMAGE_U8);
        vxDataMap["vxThr0"] = (vx_reference)thr;

        vx_threshold thr1 = vxCreateThresholdForImage(this->context, vx_threshold_type_e::VX_THRESHOLD_TYPE_RANGE,
                                                      vx_df_image_e::VX_DF_IMAGE_U8, vx_df_image_e::VX_DF_IMAGE_U8);
        vx_pixel_value_t pv1, pv2;
        pv1.U8 = 0;
        pv2.U8 = 15;
        vxCopyThresholdRange(thr1, &pv1, &pv2, vx_accessor_e::VX_WRITE_ONLY, vx_memory_type_e::VX_MEMORY_TYPE_HOST);
        vxDataMap["vxThr1"] = (vx_reference)thr1;

        vx_threshold thr2 = vxCreateThresholdForImage(this->context, vx_threshold_type_e::VX_THRESHOLD_TYPE_BINARY,
                                                      vx_df_image_e::VX_DF_IMAGE_S16, vx_df_image_e::VX_DF_IMAGE_S16);
        pv1.S16 = 1010;
        pv2.S16 = 25252;
        vxCopyThresholdOutput(thr2, &pv2, &pv1, vx_accessor_e::VX_WRITE_ONLY, vx_memory_type_e::VX_MEMORY_TYPE_HOST);
        pv1.S16 = 666;
        vxCopyThresholdValue(thr2, &pv1, vx_accessor_e::VX_WRITE_ONLY, vx_memory_type_e::VX_MEMORY_TYPE_HOST);
        vxDataMap["vxThr2"] = (vx_reference)thr2;

        vx_threshold thr3 = vxCreateThresholdForImage(this->context, vx_threshold_type_e::VX_THRESHOLD_TYPE_RANGE,
                                                      vx_df_image_e::VX_DF_IMAGE_S16, vx_df_image_e::VX_DF_IMAGE_S16);
        pv1.S16 = 0;
        pv2.S16 = 888;
        vxCopyThresholdRange(thr3, &pv1, &pv2, vx_accessor_e::VX_WRITE_ONLY, vx_memory_type_e::VX_MEMORY_TYPE_HOST);
        vxDataMap["vxThr3"] = (vx_reference)thr3;
#endif
        ASSERT_EQ_JSON(VxDataReader::Process("vxThr0", vxDataMap), readJsonFromFile(this->testDir + "/vxThr/vxThr0.json"));
        ASSERT_EQ_JSON(VxDataReader::Process("vxThr1", vxDataMap), readJsonFromFile(this->testDir + "/vxThr/vxThr1.json"));
        ASSERT_EQ_JSON(VxDataReader::Process("vxThr2", vxDataMap), readJsonFromFile(this->testDir + "/vxThr/vxThr2.json"));
        ASSERT_EQ_JSON(VxDataReader::Process("vxThr3", vxDataMap), readJsonFromFile(this->testDir + "/vxThr/vxThr3.json"));

        for (auto elem : vxDataMap) {
            vxReleaseThreshold(reinterpret_cast<vx_threshold *>(&elem.second));
        }
    }

    TEST_F(VxDataReaderTest, ReadMatrix) {
        std::map<string, vx_reference> vxDataMap = {};

        createMatrix<vx_uint8>(vxDataMap, this->context, 4, 4, "vxMat0", VX_TYPE_UINT8);
        createMatrix<vx_int32>(vxDataMap, this->context, 5, 5, "vxMat1", VX_TYPE_INT32);
        createMatrix<vx_float32>(vxDataMap, this->context, 6, 6, "vxMat2", VX_TYPE_FLOAT32);

        ASSERT_EQ_JSON(VxDataReader::Process("vxMat0", vxDataMap), readJsonFromFile(this->testDir + "/vxMat/vxMat0.json"));
        ASSERT_EQ_JSON(VxDataReader::Process("vxMat1", vxDataMap), readJsonFromFile(this->testDir + "/vxMat/vxMat1.json"));
        ASSERT_EQ_JSON(VxDataReader::Process("vxMat2", vxDataMap), readJsonFromFile(this->testDir + "/vxMat/vxMat2.json"));

        for (auto elem : vxDataMap) {
            vxReleaseMatrix(reinterpret_cast<vx_matrix *>(&elem.second));
        }
    }

    TEST_F(VxDataReaderTest, ReadConvolution) {
        std::map<string, vx_reference> vxDataMap = {};

        createConvolution<vx_int16>(vxDataMap, this->context, 3, 3, "vxConv0", VX_TYPE_UINT16);

        ASSERT_EQ_JSON(VxDataReader::Process("vxConv0", vxDataMap), readJsonFromFile(this->testDir + "/vxConv/vxConv0.json"));

        for (auto elem : vxDataMap) {
            vxReleaseMatrix(reinterpret_cast<vx_matrix *>(&elem.second));
        }
    }

    TEST_F(VxDataReaderTest, ReadDistribution) {
        std::map<string, vx_reference> vxDataMap = {};

        createDistribution<vx_uint32>(vxDataMap, this->context, 4, "vxDist0");

        ASSERT_EQ_JSON(VxDataReader::Process("vxDist0", vxDataMap), readJsonFromFile(this->testDir + "/vxDist/vxDist0.json"));

        for (auto elem : vxDataMap) {
            vxReleaseMatrix(reinterpret_cast<vx_matrix *>(&elem.second));
        }
    }

    TEST_F(VxDataReaderTest, DISABLED_ReadPyramid) {
        std::map<string, vx_reference> vxDataMap = {};

        vxDataMap["vxPyr0"] = (vx_reference)vxCreatePyramid(this->context, 4, VX_SCALE_PYRAMID_HALF, 512, 512, VX_DF_IMAGE_U8);

        ASSERT_EQ_JSON(VxDataReader::Process("vxPyr0", vxDataMap), readJsonFromFile(this->testDir + "/vxPyr/vxPyr0.json"));

        for (auto elem : vxDataMap) {
            vxReleasePyramid(reinterpret_cast<vx_pyramid *>(&elem.second));
        }
    }

    TEST_F(VxDataReaderTest, ReadRemap) {
        std::map<string, vx_reference> vxDataMap = {};

        createRemap(vxDataMap, this->context, 4, 3, "vxRemap0");

        ASSERT_EQ_JSON(VxDataReader::Process("vxRemap0", vxDataMap), readJsonFromFile(this->testDir + "/vxRemap/vxRemap0.json"));

        for (auto elem : vxDataMap) {
            vxReleaseRemap(reinterpret_cast<vx_remap *>(&elem.second));
        }
    }

    TEST_F(VxDataReaderTest, ReadUnknownKey) {
        std::map<string, vx_reference> vxDataMap = {};

        ASSERT_TRUE(VxDataReader::Process("vxRemap21", vxDataMap).empty());

        for (auto elem : vxDataMap) {
            vxReleaseRemap(reinterpret_cast<vx_remap *>(&elem.second));
        }
    }
}
