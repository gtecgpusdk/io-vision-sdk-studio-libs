/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include <future>
#include <thread>
#include "../sourceFilesMap.hpp"

namespace Io::VisionSDK::Studio::Libs::Utils {
    using Io::VisionSDK::Studio::Libs::Primitives::BaseGraph;
    using Io::VisionSDK::Studio::Libs::Primitives::BaseContext;
    using Io::VisionSDK::Studio::Libs::Primitives::BaseVisualGraph;
    using Io::VisionSDK::Studio::Libs::Media::Image;

    string testRoot;

    class TestGraph : public BaseGraph {
     public:
        explicit TestGraph(BaseContext *context) : BaseGraph(context) {}

     protected:
        vx_status create() override {
            vx_status status = vxGetStatus((vx_reference)this->getVxGraph());
            if (status == VX_SUCCESS) {
                this->vxDataMap["vxData0"] = this->createImage(this->getParent()->getVxContext(), 640, 480, VX_DF_IMAGE_U8);
                this->getParent()->Check(this->vxDataMap["vxData0"]);
                vx_uint32 vxData1Value = 4;
                this->vxDataMap["vxData1"] = (vx_reference)vxCreateScalar(this->getParent()->getVxContext(), VX_TYPE_UINT32, &vxData1Value);
                this->getParent()->Check(this->vxDataMap["vxData1"]);
                this->vxDataMap["vxData2"] = this->createImage(this->getParent()->getVxContext(),
                                                               BaseGraph::getImageWidth(this->vxDataMap["vxData0"]),
                                                               BaseGraph::getImageHeight(this->vxDataMap["vxData0"]), VX_DF_IMAGE_S16);
                this->getParent()->Check(this->vxDataMap["vxData2"]);
                vx_float32 vxData3Value = 0.500f;
                this->vxDataMap["vxData3"] = (vx_reference)vxCreateScalar(this->getParent()->getVxContext(), VX_TYPE_FLOAT32,
                                                                          &vxData3Value);
                this->getParent()->Check(this->vxDataMap["vxData3"]);
                this->vxDataMap["vxData4"] = this->createImage(this->getParent()->getVxContext(),
                                                               BaseGraph::getImageWidth(this->vxDataMap["vxData0"]),
                                                               BaseGraph::getImageHeight(this->vxDataMap["vxData0"]), VX_DF_IMAGE_U8);
                this->getParent()->Check(this->vxDataMap["vxData4"]);
                this->vxDataMap["vxData5"] = this->createImage(this->getParent()->getVxContext(),
                                                               BaseGraph::getImageWidth(this->vxDataMap["vxData0"]),
                                                               BaseGraph::getImageHeight(this->vxDataMap["vxData0"]), VX_DF_IMAGE_S16);
                vx_int32 vxDataTestValue = 0;
                this->vxDataMap["vxDataTest"] = (vx_reference)vxCreateScalar(this->getParent()->getVxContext(), VX_TYPE_INT32,
                                                                             &vxDataTestValue);

                this->getParent()->Check(this->vxDataMap["vxData5"]);
                this->vxNodesMap["acc_squ1"] = (vx_reference)vxAccumulateSquareImageNode(this->getVxGraph(),
                                                                                         (vx_image)this->vxDataMap["vxData0"],
                                                                                         (vx_scalar)this->vxDataMap["vxData1"],
                                                                                         (vx_image)this->vxDataMap["vxData2"]);
                this->getParent()->Check(this->vxNodesMap["acc_squ1"]);
                this->vxNodesMap["acc_wei1"] = (vx_reference)vxAccumulateWeightedImageNode(this->getVxGraph(),
                                                                                           (vx_image)this->vxDataMap["vxData0"],
                                                                                           (vx_scalar)this->vxDataMap["vxData3"],
                                                                                           (vx_image)this->vxDataMap["vxData4"]);
                this->getParent()->Check(this->vxNodesMap["acc_wei1"]);
                this->vxNodesMap["acc"] = (vx_reference)vxAccumulateImageNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData0"],
                                                                              (vx_image)this->vxDataMap["vxData5"]);
                this->getParent()->Check(this->vxNodesMap["acc"]);
            }
            return status;
        }

        vx_status process(const std::function<vx_status(int)> &$handler) override {
            return BaseGraph::process([&](int $iteration) -> vx_status {
                vx_status status = vxGetStatus((vx_reference)this->getVxGraph());
                if (status == VX_SUCCESS && !this->getParent()->getParent()->IsNoFileIo()) {
                    auto ioCom0 = this->getParent()->getParent()->getIoCom("ioCom0");
                    if ($iteration == 0) {
                        vx_image vxData0Ref = (vx_image)this->getData("vxData0");
                        ioCom0->getFrame(vxData0Ref, true);
                    }
                }
                if (status == VX_SUCCESS) {
                    status = this->getParent()->Check(vxProcessGraph(this->getVxGraph()));
                    // change scalar value
                    vx_int32 value = $iteration;
                    vxCopyScalar((vx_scalar)this->getData("vxDataTest"), &value, vx_accessor_e::VX_WRITE_ONLY,
                                 vx_memory_type_e::VX_MEMORY_TYPE_HOST);
                }
                if (status == VX_SUCCESS && !this->getParent()->getParent()->IsNoFileIo()) {
                    auto ioCom1 = this->getParent()->getParent()->getIoCom("ioCom1");
                    vx_image vxData2Ref = (vx_image)this->getData("vxData2");
                    ioCom1->setFrame(vxData2Ref);
                }
                if (status == VX_SUCCESS && !this->getParent()->getParent()->IsNoFileIo()) {
                    auto ioCom2 = this->getParent()->getParent()->getIoCom("ioCom2");
                    vx_image vxData4Ref = (vx_image)this->getData("vxData4");
                    ioCom2->setFrame(vxData4Ref);
                }
                if (status == VX_SUCCESS && !this->getParent()->getParent()->IsNoFileIo()) {
                    auto ioCom3 = this->getParent()->getParent()->getIoCom("ioCom3");
                    vx_image vxData5Ref = (vx_image)this->getData("vxData5");
                    ioCom3->setFrame(vxData5Ref);
                }
                return status;
            });
        }

        bool loopCondition(int $loopCnt) const override {
            if (!this->getParent()->getParent()->IsLooped()) {
                return $loopCnt < 10;
            }
            return true;
        }
    };

    class TestContext : public BaseContext {
     public:
        explicit TestContext(BaseVisualGraph *parent) : BaseContext(parent) {}

     protected:
        vx_status create() override {
            this->graphsMap["TestGraph"] = new TestGraph(this);
            return vx_status_e::VX_SUCCESS;
        }

        vx_status process(const std::function<vx_status(void)> &$handler) override {
            return BaseContext::process([&]() -> vx_status {
                vx_status status = vxGetStatus((vx_reference)this->getVxContext());
                auto graph0 = this->getGraph("TestGraph");
                if (status == VX_SUCCESS && graph0 != nullptr) {
                    status = const_cast<BaseGraph *>(graph0)->Process();
                    if (status != VX_SUCCESS) {
                        const_cast<TestContext *>(this)->removeGraph("TestGraph");
                    }
                } else {
                    status = VX_FAILURE;
                }
                return status;
            });
        }
    };

    class VisualGraph : public BaseVisualGraph {
     protected:
        vx_status create() override {
            this->contextsMap["TestContext"] = new TestContext(this);
            return vx_status_e::VX_SUCCESS;
        }

        vx_status process(const std::function<vx_status(int)> &$handler) override {
            return BaseVisualGraph::process([&](int $iterator) -> vx_status {
                vx_status status = VX_FAILURE;
                BaseContext *context0 = const_cast<BaseContext *>(this->getContext("TestContext"));
                if (context0 != nullptr) {
                    status = VX_SUCCESS;
                }
                BaseGraph *graph_0_0 = nullptr;
                if (status == VX_SUCCESS) {
                    graph_0_0 = const_cast<BaseGraph *>(context0->getGraph("TestGraph"));
                    if (graph_0_0 != nullptr) {
                        status = VX_SUCCESS;
                    } else {
                        status = VX_FAILURE;
                    }
                }
                if (status == VX_SUCCESS) {
                    this->ioComMap.emplace("ioCom0", std::make_shared<Image>(
                            testRoot + "/bikegray_640x480.png",
                            context0->getVxContext()));
                }
                if (status == VX_SUCCESS) {
                    this->ioComMap.emplace("ioCom1", std::make_shared<Image>(testRoot + "/obikeaccq_640x480_P400_16b.png",
                                                                             context0->getVxContext()));
                }
                if (status == VX_SUCCESS) {
                    this->ioComMap.emplace("ioCom2", std::make_shared<Image>(testRoot + "/obikeaccw_640x480_P400_16b.png",
                                                                             context0->getVxContext()));
                }
                if (status == VX_SUCCESS) {
                    this->ioComMap.emplace("ioCom3", std::make_shared<Image>(testRoot + "/obikeaccu_640x480_P400_16b.png",
                                                                             context0->getVxContext()));
                }
                if (status == VX_SUCCESS) {
                    status = context0->Process();
                }
                return status;
            });
        }
    };

    class MockManager : public Manager {
     public:
        MockManager() {
            this->setVisualGraph(new VisualGraph());
        }

        void setCreated(bool $value) override {
            Manager::setCreated($value);
        }

        void setValidated(bool $value) override {
            Manager::setValidated($value);
        }

        void setRunning(bool $value) override {
            Manager::setRunning($value);
        }

        bool IsRunning() override {
            return Manager::IsRunning();
        }

        bool IsCreated() override {
            return Manager::IsCreated();
        }

        bool IsValidated() override {
            return Manager::IsValidated();
        }
    };

    class ManagerTest : public testing::Test {
     public:
        MockManager manager{};

     protected:
        void SetUp() override {
            testRoot = "test/resource/data/Io/VisionSDK/Studio/Libs/Validation";
        }

        void TearDown() override {
        }
    };

    TEST_F(ManagerTest, Translate) {
        int size = 30;
        char *buffer = new char[size];
        this->manager.Translate(0, buffer, &size);
        ASSERT_EQ(size, 22);
        ASSERT_STREQ(string(buffer, size).c_str(), "VX_SUCCESS (No error.)");

        size = 14;
        delete[] buffer;
        buffer = new char[size];
        this->manager.Translate(0, buffer, &size);
        ASSERT_EQ(size, 22);

        size = 0;
        this->manager.Translate(0, buffer, &size);
        ASSERT_EQ(size, 22);

        delete[] buffer;
        buffer = nullptr;
        this->manager.Translate(0, buffer, &size);
        ASSERT_EQ(size, 22);
        ASSERT_EQ(buffer, nullptr);
    }

    TEST_F(ManagerTest, Process) {
        ASSERT_FALSE(this->manager.IsCreated());
        ASSERT_EQ(this->manager.Create(), vx_status_e::VX_SUCCESS);
        ASSERT_TRUE(this->manager.IsCreated());
        ASSERT_FALSE(this->manager.IsValidated());
        ASSERT_EQ(this->manager.Validate(), vx_status_e::VX_SUCCESS);
        ASSERT_TRUE(this->manager.IsValidated());
        ASSERT_FALSE(this->manager.IsRunning());
        ASSERT_EQ(this->manager.Process(), vx_status_e::VX_SUCCESS);
    }

    TEST_F(ManagerTest, ReadDataFailure) {
        int size = 1024;
        char *buffer = new char[size];
        this->manager.ReadData("NoContext.NoGraph.NoData", buffer, &size);
        ASSERT_EQ(size, vx_status_e::VX_FAILURE);
        this->manager.ReadData(nullptr, buffer, &size);
        ASSERT_EQ(size, vx_status_e::VX_FAILURE);

        size = 1024;
        ASSERT_EQ(this->manager.Create(), vx_status_e::VX_SUCCESS);
        this->manager.ReadData("TestContext.TestGraph.vxData_UNKNOWN", buffer, &size);
        ASSERT_EQ_JSON(json::parse(string(buffer, size)), R"(null)"_json);

        size = 1024;
        this->manager.ReadData("TestContext.TestGraph_UNKNOWN.vxData_UNKNOWN", buffer, &size);
        ASSERT_EQ_JSON(json::parse(string(buffer, size)), R"(null)"_json);

        size = 1024;
        this->manager.ReadData("TestContext_UNKNOWN.TestGraph_UNKNOWN.vxData_UNKNOWN", buffer, &size);
        ASSERT_EQ_JSON(json::parse(string(buffer, size)), R"(null)"_json);

        size = 1024;
        this->manager.ReadData("NOT_VALID_PATH", buffer, &size);
        ASSERT_EQ_JSON(json::parse(string(buffer, size)), R"(null)"_json);

        delete[] buffer;
    }

    TEST_F(ManagerTest, WriteDataFailure) {
        ASSERT_EQ(this->manager.WriteData("NoContext.NoGraph.NoData", nullptr), vx_status_e::VX_FAILURE);
        ASSERT_EQ(this->manager.WriteData("NoContext.NoGraph.NoData", ""), vx_status_e::VX_FAILURE);
        ASSERT_EQ(this->manager.WriteData(nullptr, ""), vx_status_e::VX_FAILURE);

        ASSERT_EQ(this->manager.Create(), vx_status_e::VX_SUCCESS);

        ASSERT_EQ(this->manager.WriteData("TestContext.TestGraph.vxData_UNKNOWN", ""), vx_status_e::VX_FAILURE);
        ASSERT_EQ(this->manager.WriteData("TestContext.TestGraph_UNKNOWN.vxData_UNKNOWN", ""), vx_status_e::VX_FAILURE);
        ASSERT_EQ(this->manager.WriteData("TestContext_UNKNOWN.TestGraph_UNKNOWN.vxData_UNKNOWN", ""), vx_status_e::VX_FAILURE);
        ASSERT_EQ(this->manager.WriteData("NOT_VALID_PATH", ""), vx_status_e::VX_FAILURE);
    }

    TEST_F(ManagerTest, ReadData) {
        ASSERT_EQ(this->manager.Create(), vx_status_e::VX_SUCCESS);
        ASSERT_EQ(this->manager.Validate(), vx_status_e::VX_SUCCESS);
        ASSERT_EQ(this->manager.Process(), vx_status_e::VX_SUCCESS);

        int size = 1024;
        char *buffer = new char[size];
        this->manager.ReadData("TestContext.TestGraph.vxData3", buffer, &size);
        ASSERT_STREQ(string(buffer, static_cast<size_t>(size)).c_str(),
                     R"({"type":"VX_TYPE_SCALAR","value":0.5,"valueType":"VX_TYPE_FLOAT32"})");

        this->manager.ReadData(nullptr, buffer, &size);
        ASSERT_EQ(size, vx_status_e::VX_FAILURE);
        delete[] buffer;
    }

    TEST_F(ManagerTest, ReadDataLoop) {
        ASSERT_EQ(this->manager.Create(), vx_status_e::VX_SUCCESS);
        ASSERT_EQ(this->manager.Validate(), vx_status_e::VX_SUCCESS);

        std::future<int> future = std::async(std::launch::async, [&]() {
            return this->manager.Process({.looped = true, .noFileIO = true});
        });

        int size = 1024;
        char *buffer = new char[size];
        std::vector<int> values;
        std::this_thread::sleep_for(std::chrono::milliseconds{100});
        for (int i = 0; i < 10; i++) {
            size = 1024;
            this->manager.ReadData("TestContext.TestGraph.vxDataTest", buffer, &size);
            json data = json::parse(string(buffer, size));
            values.emplace_back(data["value"].get<int>());
            std::this_thread::sleep_for(std::chrono::milliseconds{100});
        }
        this->manager.Stop();
        auto status = future.wait_for(std::chrono::seconds(10));
        ASSERT_EQ(status, std::future_status::ready);
        ASSERT_EQ(future.get(), vx_status_e::VX_SUCCESS);

        int lastValue = -1;
        int sameCnt = 0;
        for (size_t i = 0; i < values.size(); i++) {
            if (lastValue == values[i]) {
                sameCnt++;
            }
            lastValue = values[i];
        }
        ASSERT_LE(sameCnt, 2);
    }

    TEST_F(ManagerTest, ReadWriteImage) {
        ASSERT_EQ(this->manager.Create(), vx_status_e::VX_SUCCESS);
        ASSERT_EQ(this->manager.Validate(), vx_status_e::VX_SUCCESS);
        ASSERT_EQ(this->manager.Process(), vx_status_e::VX_SUCCESS);

        int size = 640 * 480 * 4;
        char *buffer = new char[size];
        this->manager.ReadImage("TestContext.TestGraph.vxData0", buffer, &size);
        ASSERT_EQ(size, 193364);

        ASSERT_EQ(this->manager.WriteImage("TestContext.TestGraph.vxData0", buffer, size), vx_status_e::VX_SUCCESS);
        delete[] buffer;
    }

    TEST_F(ManagerTest, WriteData) {
        ASSERT_EQ(this->manager.Create(), vx_status_e::VX_SUCCESS);

        string scalarStr = R"({"type":"VX_TYPE_SCALAR","value":442.5,"valueType":"VX_TYPE_FLOAT32"})";
        ASSERT_EQ(this->manager.WriteData("TestContext.TestGraph.vxData3", scalarStr.c_str()), vx_status_e::VX_SUCCESS);

        int size = 1024;
        char *buffer = new char[size];
        this->manager.ReadData("TestContext.TestGraph.vxData3", buffer, &size);
        ASSERT_STREQ(string(buffer, size).c_str(), scalarStr.c_str());
        delete[] buffer;
    }

    TEST_F(ManagerTest, ReadStats) {
        ASSERT_EQ(this->manager.Create(), vx_status_e::VX_SUCCESS);
        ASSERT_EQ(this->manager.Validate(), vx_status_e::VX_SUCCESS);
        ASSERT_EQ(this->manager.Process(), vx_status_e::VX_SUCCESS);
        int size = 2014;
        char *buffer = new char[size];
        this->manager.ReadStats(buffer, &size);
        ASSERT_GT(size, 0);
        json output = json::parse(string(buffer, size));
        ASSERT_EQ((output["contexts"]["TestContext"]["elapsed"].get<std::vector<int>>()).size(), std::size_t(1));
        ASSERT_EQ((output["contexts"]["TestContext"]["graphs"]["TestGraph"].get<std::vector<int>>()).size(), std::size_t(11));
        delete[] buffer;
    }

    TEST_F(ManagerTest, Handlers) {
        ASSERT_EQ(this->manager.Create(), vx_status_e::VX_SUCCESS);
        ASSERT_EQ(this->manager.Validate(), vx_status_e::VX_SUCCESS);

        int onStartCalled = 0, onStopCalled = 0, onErrorCalled = 0;
        this->manager.RegisterOnStart([&](const string &$context) {
            onStartCalled++;
        });

        this->manager.RegisterOnStop([&](const string &$context, int $elapsed) {
            onStopCalled++;
        });

        this->manager.RegisterOnError([&](const string &$context, const string &$error) {
            onErrorCalled++;
        });

        ASSERT_EQ(this->manager.Process(), vx_status_e::VX_SUCCESS);
        ASSERT_EQ(onStartCalled, 12);
        ASSERT_EQ(onStopCalled, 12);
        ASSERT_EQ(onErrorCalled, 0);
    }
}
