/* ********************************************************************************************************* *
*
* Copyright (c) 2019 NXP
*
* SPDX-License-Identifier: BSD-3-Clause
* The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
* or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
*
* ********************************************************************************************************* */

#include <fstream>
#include "../sourceFilesMap.hpp"

namespace Io::VisionSDK::Studio::Libs::Utils {
    class VxDataWriterTest : public testing::Test {
     public:
        static void createRemap(std::map<string, vx_reference> &$vxDataMap, vx_context $context, vx_uint32 $w, vx_uint32 $h,
                                const string &$name) {
            vx_remap remap = vxCreateRemap($context, $w, $h, $w, $h);

#if VX_VERSION == VX_VERSION_1_0 || VX_VERSION == VX_VERSION_1_1
            for (vx_uint32 j = 0; j < $w; j++) {
                for (vx_uint32 i = 0; i < $w; i++) {
                    vxSetRemapPoint(remap, i, j, i * j, i * j);
                }
            }
#elif VX_VERSION == VX_VERSION_1_2
            vx_rectangle_t rect = {.start_x = 0, .start_y = 0, .end_x = $w, .end_y = $h};
            auto *coords = new vx_coordinates2df_t[$w * $h];
            for (vx_uint32 j = 0; j < $h; j++) {
                for (vx_uint32 i = 0; i < $w; i++) {
                    coords[i + (j * $w)] = {.x = static_cast<vx_float32>(i * j), .y = static_cast<vx_float32>(i * j)};
                }
            }
            vxCopyRemapPatch(remap, &rect, sizeof(vx_coordinates2d_t) * $w, coords, vx_type_e::VX_TYPE_COORDINATES2DF,
                             vx_accessor_e::VX_WRITE_ONLY, vx_memory_type_e::VX_MEMORY_TYPE_HOST);

            delete[] coords;
#else
#error "createRemap not availible for used OpenVX version"
#endif
            $vxDataMap[$name] = (vx_reference)remap;
        }

        static json readJsonFromFile(const string &$path) {
            std::ifstream data($path);
            try {
                return json::parse(data);
            } catch (const std::exception &ex) {
                return {};
            }
        }

#if VX_VERSION == VX_VERSION_1_0
        const string testDir = "test/resource/data/Io/VisionSDK/Studio/Libs/Utils/openvx_1.0";
#elif VX_VERSION == VX_VERSION_1_1
        const string testDir = "test/resource/data/Io/VisionSDK/Studio/Libs/Utils/openvx_1.1";
#elif VX_VERSION == VX_VERSION_1_2
        const string testDir = "test/resource/data/Io/VisionSDK/Studio/Libs/Utils/openvx_1.2";
#else
#error "Specify path to test resources for used OpenVX version"
#endif

        vx_context context{};

     protected:
        void SetUp() override {
            vx_status status;

            this->context = vxCreateContext();

            status = vxGetStatus((vx_reference)this->context);
            if (status != VX_SUCCESS) {
                GTEST_FATAL_FAILURE_((": Can not load context due to error: " + std::to_string(status)).c_str());
            }
        }

        void TearDown() override {
            vxReleaseContext(&this->context);
        }
    };  // NOLINT(readability/braces) FALSE-POSITIVE?

    TEST_F(VxDataWriterTest, WriteArray) {
        std::map<string, vx_reference> vxDataMap = {};
        const size_t numItems = 10;

        vxDataMap["vxArray0"] = (vx_reference)vxCreateArray(this->context, vx_type_e::VX_TYPE_INT8, numItems);
        vxDataMap["vxArray1"] = (vx_reference)vxCreateArray(this->context, vx_type_e::VX_TYPE_INT16, numItems);
        vxDataMap["vxArray2"] = (vx_reference)vxCreateArray(this->context, vx_type_e::VX_TYPE_INT32, numItems);
        vxDataMap["vxArray3"] = (vx_reference)vxCreateArray(this->context, vx_type_e::VX_TYPE_INT64, numItems);
        vxDataMap["vxArray4"] = (vx_reference)vxCreateArray(this->context, vx_type_e::VX_TYPE_CHAR, numItems);
        vxDataMap["vxArray5"] = (vx_reference)vxCreateArray(this->context, vx_type_e::VX_TYPE_FLOAT32, numItems);
        vxDataMap["vxArray6"] = (vx_reference)vxCreateArray(this->context, vx_type_e::VX_TYPE_FLOAT64, numItems);
        vxDataMap["vxArray7"] = (vx_reference)vxCreateArray(this->context, vx_type_e::VX_TYPE_UINT8, numItems);
        vxDataMap["vxArray8"] = (vx_reference)vxCreateArray(this->context, vx_type_e::VX_TYPE_UINT16, numItems);
        vxDataMap["vxArray9"] = (vx_reference)vxCreateArray(this->context, vx_type_e::VX_TYPE_UINT32, numItems);
        vxDataMap["vxArray10"] = (vx_reference)vxCreateArray(this->context, vx_type_e::VX_TYPE_UINT64, numItems);
        vxDataMap["vxArray11"] = (vx_reference)vxCreateArray(this->context, vx_type_e::VX_TYPE_SIZE, numItems);
        vxDataMap["vxArray12"] = (vx_reference)vxCreateArray(this->context, vx_type_e::VX_TYPE_KEYPOINT, numItems);
        vxDataMap["vxArray13"] = (vx_reference)vxCreateArray(this->context, vx_type_e::VX_TYPE_RECTANGLE, numItems);

        auto match = [&](const std::pair<string, vx_reference> &$item) {
            json data = readJsonFromFile(this->testDir + "/vxArray/" + $item.first + ".json");
            ASSERT_EQ(VxDataWriter::Process($item.first, vxDataMap, data), vx_status_e::VX_SUCCESS) << "\nitem: " << $item.first;
            ASSERT_EQ_JSON(VxDataReader::Process($item.first, vxDataMap), data) << "\nitem: " << $item.first;
        };

        for (const auto &item : vxDataMap) {
            match(item);
        }

        for (auto elem : vxDataMap) {
            vxReleaseArray(reinterpret_cast<vx_array *>(&elem.second));
        }
    }

    TEST_F(VxDataWriterTest, WriteLUT) {
        std::map<string, vx_reference> vxDataMap = {};

        vxDataMap["vxLut0"] = (vx_reference)vxCreateLUT(this->context, VX_TYPE_UINT8, 20);
        vxDataMap["vxLut1"] = (vx_reference)vxCreateLUT(this->context, VX_TYPE_INT16, 20);

        auto match = [&](const std::pair<string, vx_reference> &$item) {
            json data = readJsonFromFile(this->testDir + "/vxLut/" + $item.first + ".json");
            ASSERT_EQ(VxDataWriter::Process($item.first, vxDataMap, data), vx_status_e::VX_SUCCESS) << "\nitem: " << $item.first;
            ASSERT_EQ_JSON(VxDataReader::Process($item.first, vxDataMap), data) << "\nitem: " << $item.first;
        };

        for (const auto &item : vxDataMap) {
            match(item);
        }

        for (auto elem : vxDataMap) {
            vxReleaseLUT(reinterpret_cast<vx_lut *>(&elem.second));
        }
    }

    TEST_F(VxDataWriterTest, WriteScalar) {
        std::map<string, vx_reference> vxDataMap = {};

        vx_int32 val0 = 0;
        vxDataMap["vxScalar0"] = (vx_reference)vxCreateScalar(this->context, VX_TYPE_INT32, &val0);
        vx_float32 val1 = 0;
        vxDataMap["vxScalar1"] = (vx_reference)vxCreateScalar(this->context, VX_TYPE_FLOAT32, &val1);
        vx_char val2 = 0;
        vxDataMap["vxScalar2"] = (vx_reference)vxCreateScalar(this->context, VX_TYPE_CHAR, &val2);
        vx_float64 val3 = 0;
        vxDataMap["vxScalar3"] = (vx_reference)vxCreateScalar(this->context, VX_TYPE_FLOAT64, &val3);
        vx_uint64 val4 = 0;
        vxDataMap["vxScalar4"] = (vx_reference)vxCreateScalar(this->context, VX_TYPE_UINT64, &val4);
        vx_bool val5 = _vx_bool_e::vx_false_e;
        vxDataMap["vxScalar5"] = (vx_reference)vxCreateScalar(this->context, VX_TYPE_BOOL, &val5);

        json data = R"({"type":"VX_TYPE_SCALAR","value":44,"valueType":"VX_TYPE_INT32"})"_json;
        ASSERT_EQ(VxDataWriter::Process("vxScalar0", vxDataMap, data), vx_status_e::VX_SUCCESS);
        ASSERT_EQ_JSON(VxDataReader::Process("vxScalar0", vxDataMap), data);

        data = R"({"type":"VX_TYPE_SCALAR","value":442.549987792969,"valueType":"VX_TYPE_FLOAT32"})"_json;
        ASSERT_EQ(VxDataWriter::Process("vxScalar1", vxDataMap, data), vx_status_e::VX_SUCCESS);
        ASSERT_EQ_JSON(VxDataReader::Process("vxScalar1", vxDataMap), data);

        data = R"({"type":"VX_TYPE_SCALAR","value":98,"valueType":"VX_TYPE_CHAR"})"_json;
        ASSERT_EQ(VxDataWriter::Process("vxScalar2", vxDataMap, data), vx_status_e::VX_SUCCESS);
        ASSERT_EQ_JSON(VxDataReader::Process("vxScalar2", vxDataMap), data);

        data = R"({"type":"VX_TYPE_SCALAR","value":12.22,"valueType":"VX_TYPE_FLOAT64"})"_json;
        ASSERT_EQ(VxDataWriter::Process("vxScalar3", vxDataMap, data), vx_status_e::VX_SUCCESS);
        ASSERT_EQ_JSON(VxDataReader::Process("vxScalar3", vxDataMap), data);

        data = R"({"type":"VX_TYPE_SCALAR","value":22,"valueType":"VX_TYPE_UINT64"})"_json;
        ASSERT_EQ(VxDataWriter::Process("vxScalar4", vxDataMap, data), vx_status_e::VX_SUCCESS);
        ASSERT_EQ_JSON(VxDataReader::Process("vxScalar4", vxDataMap), data);

        data = R"({"type":"VX_TYPE_SCALAR","value":1,"valueType":"VX_TYPE_BOOL"})"_json;
        ASSERT_EQ(VxDataWriter::Process("vxScalar5", vxDataMap, data), vx_status_e::VX_SUCCESS);
        ASSERT_EQ_JSON(VxDataReader::Process("vxScalar5", vxDataMap), data);

        for (auto elem : vxDataMap) {
            vxReleaseScalar(reinterpret_cast<vx_scalar *>(&elem.second));
        }
    }

    TEST_F(VxDataWriterTest, WriteThreshold) {
        std::map<string, vx_reference> vxDataMap = {};
#if VX_VERSION == VX_VERSION_1_1
        vxDataMap["vxThr0"] = (vx_reference)vxCreateThreshold(this->context, vx_threshold_type_e::VX_THRESHOLD_TYPE_BINARY,
                                                              vx_type_e::VX_TYPE_UINT8);
        vxDataMap["vxThr1"] = (vx_reference)vxCreateThreshold(this->context, vx_threshold_type_e::VX_THRESHOLD_TYPE_RANGE,
                                                              vx_type_e::VX_TYPE_UINT8);
#elif VX_VERSION == VX_VERSION_1_2
        vxDataMap["vxThr0"] = (vx_reference)vxCreateThresholdForImage(this->context, VX_THRESHOLD_TYPE_BINARY,
                                                                      vx_df_image_e::VX_DF_IMAGE_U8, vx_df_image_e::VX_DF_IMAGE_U8);
        vxDataMap["vxThr1"] = (vx_reference)vxCreateThresholdForImage(this->context, VX_THRESHOLD_TYPE_RANGE,
                                                                      vx_df_image_e::VX_DF_IMAGE_U8, vx_df_image_e::VX_DF_IMAGE_U8);
        vxDataMap["vxThr2"] = (vx_reference)vxCreateThresholdForImage(this->context, VX_THRESHOLD_TYPE_BINARY,
                                                                      vx_df_image_e::VX_DF_IMAGE_S16, vx_df_image_e::VX_DF_IMAGE_S16);
        vxDataMap["vxThr3"] = (vx_reference)vxCreateThresholdForImage(this->context, VX_THRESHOLD_TYPE_RANGE,
                                                                      vx_df_image_e::VX_DF_IMAGE_S16, vx_df_image_e::VX_DF_IMAGE_S16);
#else
#error "VxDataWriterTest::WriteThreshold test is not implemented for demand OVX version"
#endif
        auto match = [&](const std::pair<string, vx_reference> &$item) {
            json data = readJsonFromFile(this->testDir + "/vxThr/" + $item.first + ".json");
            ASSERT_EQ(VxDataWriter::Process($item.first, vxDataMap, data), vx_status_e::VX_SUCCESS) << "\nitem: " << $item.first;
            ASSERT_EQ_JSON(VxDataReader::Process($item.first, vxDataMap), data) << "\nitem: " << $item.first;
        };

        for (const auto &item : vxDataMap) {
            match(item);
        }

        for (auto elem : vxDataMap) {
            vxReleaseThreshold(reinterpret_cast<vx_threshold *>(&elem.second));
        }
    }

    TEST_F(VxDataWriterTest, WriteMatrix) {
        std::map<string, vx_reference> vxDataMap = {};

        vxDataMap["vxMat0"] = (vx_reference)vxCreateMatrix(this->context, VX_TYPE_UINT8, 4, 4);
        vxDataMap["vxMat1"] = (vx_reference)vxCreateMatrix(this->context, VX_TYPE_INT32, 5, 5);
        vxDataMap["vxMat2"] = (vx_reference)vxCreateMatrix(this->context, VX_TYPE_FLOAT32, 6, 6);

        auto match = [&](const std::pair<string, vx_reference> &$item) {
            json data = readJsonFromFile(this->testDir + "/vxMat/" + $item.first + ".json");
            ASSERT_EQ(VxDataWriter::Process($item.first, vxDataMap, data), vx_status_e::VX_SUCCESS) << "\nitem: " << $item.first;
            ASSERT_EQ_JSON(VxDataReader::Process($item.first, vxDataMap), data) << "\nitem: " << $item.first;
        };

        for (const auto &item : vxDataMap) {
            match(item);
        }

        for (auto elem : vxDataMap) {
            vxReleaseMatrix(reinterpret_cast<vx_matrix *>(&elem.second));
        }
    }

    TEST_F(VxDataWriterTest, WriteConvolution) {
        std::map<string, vx_reference> vxDataMap = {};

        vxDataMap["vxConv0"] = (vx_reference)vxCreateConvolution(this->context, 3, 3);

        auto match = [&](const std::pair<string, vx_reference> &$item) {
            json data = readJsonFromFile(this->testDir + "/vxConv/" + $item.first + ".json");
            ASSERT_EQ(VxDataWriter::Process($item.first, vxDataMap, data), vx_status_e::VX_SUCCESS) << "\nitem: " << $item.first;
            ASSERT_EQ_JSON(VxDataReader::Process($item.first, vxDataMap), data) << "\nitem: " << $item.first;
        };

        for (const auto &item : vxDataMap) {
            match(item);
        }

        for (auto elem : vxDataMap) {
            vxReleaseConvolution(reinterpret_cast<vx_convolution *>(&elem.second));
        }
    }

    TEST_F(VxDataWriterTest, WriteDistribution) {
        std::map<string, vx_reference> vxDataMap = {};

        vxDataMap["vxDist0"] = (vx_reference)vxCreateDistribution(this->context, 4, 0, 4);

        auto match = [&](const std::pair<string, vx_reference> &$item) {
            json data = readJsonFromFile(this->testDir + "/vxDist/" + $item.first + ".json");
            ASSERT_EQ(VxDataWriter::Process($item.first, vxDataMap, data), vx_status_e::VX_SUCCESS) << "\nitem: " << $item.first;
            ASSERT_EQ_JSON(VxDataReader::Process($item.first, vxDataMap), data) << "\nitem: " << $item.first;
        };

        for (const auto &item : vxDataMap) {
            match(item);
        }

        for (auto elem : vxDataMap) {
            vxReleaseMatrix(reinterpret_cast<vx_matrix *>(&elem.second));
        }
    }

    TEST_F(VxDataWriterTest, DISABLED_WritePyramid) {
        std::map<string, vx_reference> vxDataMap = {};

        ASSERT_TRUE(false) << "Missing implementation";
        for (auto elem : vxDataMap) {
            vxReleasePyramid(reinterpret_cast<vx_pyramid *>(&elem.second));
        }
    }

    TEST_F(VxDataWriterTest, WriteRemap) {
        std::map<string, vx_reference> vxDataMap = {};

        createRemap(vxDataMap, this->context, 4, 3, "vxRemap0");

        auto match = [&](const std::pair<string, vx_reference> &$item) {
            json data = readJsonFromFile(this->testDir + "/vxRemap/" + $item.first + ".json");
            ASSERT_EQ(VxDataWriter::Process($item.first, vxDataMap, data), vx_status_e::VX_SUCCESS) << "\nitem: " << $item.first;
            ASSERT_EQ_JSON(VxDataReader::Process($item.first, vxDataMap), data) << "\nitem: " << $item.first;
        };

        for (const auto &item : vxDataMap) {
            match(item);
        }

        for (auto elem : vxDataMap) {
            vxReleaseRemap(reinterpret_cast<vx_remap *>(&elem.second));
        }
    }

    TEST_F(VxDataWriterTest, WriteUsupportedType) {
        std::map<string, vx_reference> vxDataMap = {};

        vx_int32 val0 = 0;
        vxDataMap["vxScalar0"] = (vx_reference)vxCreateScalar(this->context, VX_TYPE_INT32, &val0);

        json data = R"({"type":"VX_TYPE_UNKNOWN"})"_json;
        ASSERT_NE(VxDataWriter::Process("vxScalar0", vxDataMap, data), vx_status_e::VX_SUCCESS);
        for (auto elem : vxDataMap) {
            vxReleaseImage(reinterpret_cast<vx_image *>(&elem.second));
        }
    }

    TEST_F(VxDataWriterTest, WriteUnknownKey) {
        std::map<string, vx_reference> vxDataMap = {};

        ASSERT_EQ(VxDataWriter::Process("vxRemap21", vxDataMap, json()), vx_status_e::VX_FAILURE);
    }
}
