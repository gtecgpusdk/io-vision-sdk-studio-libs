/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
const Loader = Com.Wui.Framework.Builder.Loader;
const LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
const ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
const EnvironmentHelper = Com.Wui.Framework.Localhost.Utils.EnvironmentHelper;
const Patcher = Com.Wui.Framework.Builder.Utils.Patcher;
const terminal = Loader.getInstance().getTerminal();
const filesystem = Loader.getInstance().getFileSystemHandler();
const properties = Loader.getInstance().getAppProperties();
const os = require("os");

let cwd;

function finalize($done) {
    Patcher.getInstance().Rollback($done);
}

function fail($info) {
    finalize(() => {
        LogIt.Error($info);
    });
}

function installOnWindows($callback) {
    let msysRoot = properties.externalModules + "/msys2";
    const scriptName = "tmpOpenCVInstallScript.sh";
    const buildPath = cwd + "/build-gcc";
    const scriptData = "" +
        "#!/bin/bash" + os.EOL +
        "cmake -G\"MinGW Makefiles\" " +
        "-DPYTHON_EXECUTABLE=\"" + filesystem.NormalizePath(properties.externalModules + "/python36/python.exe") + "\" " +
        "-DCMAKE_INSTALL_PREFIX=ocv_build " +
        "-DWITH_OPENEXR=OFF " +
        "-DWITH_JPEG=ON " +
        "-DWITH_TIFF=ON " +
        "-DWITH_PNG=ON " +
        "-DWITH_JASPER=ON " +
        "-DWITH_OPENCL=OFF " +
        "-DBUILD_ZLIB=ON " +
        "-DBUILD_EXAMPLES=OFF " +
        "-DBUILD_DOCS=OFF " +
        "-DBUILD_PERF_TESTS=OFF " +
        "-DBUILD_TESTS=OFF " +
        "-DBUILD_SHARED_LIBS=OFF " +
        "-DBUILD_WITH_DEBUG_INFO=OFF " +
        "-DBUILD_opencv_apps=OFF " +
        "-DBUILD_FAT_JAVA_LIB=OFF " +
        "-DOPENCV_BUILD_3RDPARTY_LIBS=ON " +
        "-DCMAKE_SH=\"CMAKE_SH-NOTFOUND\" .." + os.EOL +
        "cmake --build . --target install -- -j" + EnvironmentHelper.getCores() + os.EOL;

    const process = () => {
        if (filesystem.Write(buildPath + "/" + scriptName, scriptData, false)) {
            const cmakePath = cwd + "/CMakeLists.txt";
            let cmakeData = filesystem.Read(cmakePath).toString()
                .replace("include(cmake/OpenCVDetectPython.cmake)", "#include(cmake/OpenCVDetectPython.cmake)");
            filesystem.Rename(cmakePath, cmakePath + ".backup");
            if (filesystem.Write(cmakePath, cmakeData, false)) {
                terminal.Spawn(
                    "sh.exe", ["./" + scriptName],
                    {
                        cwd: buildPath,
                        env: {
                            PATH: msysRoot + "/usr/bin;"
                        }
                    },
                    ($exitCode) => {
                        if ($exitCode !== 0) {
                            fail("Build of OpenCV package failed.");
                        } else {
                            LogIt.Info("OpenCV build succeed.");
                            $callback();
                        }
                    });
            } else {
                fail("Build of OpenCV package failed: unable to apply path on CMakeLists.txt.");
            }
        } else {
            fail("Build of OpenCV package failed: unable to write script file.");
        }
    };

    if (!filesystem.Exists(buildPath)) {
        if (filesystem.CreateDirectory(buildPath)) {
            process();
        } else {
            fail("Can not create build directory.");
        }
    } else if (!filesystem.Exists(buildPath + "/ocv_build")) {
        process();
    } else {
        LogIt.Info("OpenCV build exists. Skipped.");
        $callback();
    }
}

function installGTK($isArm, $callback) {
    if (!$isArm) {
        terminal.Spawn("dpkg", ["-l", "libgtk2.0-0"], null, ($exitCode, $std) => {
            if ($exitCode !== 0) {
                fail("Validation of GTK package failed.");
            } else if ($std[0].indexOf("gtk") === -1) {
                terminal.Spawn("sudo apt-get install gtk2.0 libgtk2.0-dev -y", [], null, ($exitCode) => {
                    if ($exitCode !== 0) {
                        fail("Unable to install GTK package.");
                    } else {
                        $callback();
                    }
                });
            } else {
                $callback();
            }
        });
    } else {
        $callback();
    }
}

function installFFmpeg($isArm, $buildPath, $callback) {
    if (!$isArm) {
        const ffmpegBuildPath = $buildPath + "/ffmpeg_build";
        if (!filesystem.Exists(ffmpegBuildPath)) {
            filesystem.Download("https://ffmpeg.org/releases/ffmpeg-3.4.2.tar.bz2", ($headers, $path) => {
                filesystem.Unpack($path, {
                    output: ffmpegBuildPath
                }, () => {
                    filesystem.Delete($path);
                    terminal.Spawn(
                        "chmod +x configure && " +
                        "chmod +x ffbuild/version.sh && " +
                        "chmod +x ffbuild/libversion.sh && " +
                        "chmod +x ffbuild/pkgconfig_generate.sh && " +
                        "./configure " +
                        "--prefix=" + ffmpegBuildPath + "/build " +
                        "--enable-gpl " +
                        "--enable-nonfree " +
                        "--enable-postproc " +
                        "--enable-version3 " +
                        "--enable-shared " +
                        "--disable-x86asm && " +
                        "make -j4 && " +
                        "make install", [],
                        ffmpegBuildPath,
                        ($exitCode) => {
                            if ($exitCode !== 0) {
                                fail("Build of ffmpeg package failed.");
                            } else {
                                $callback();
                            }
                        });
                });
            });
        } else {
            $callback();
        }
    } else {
        $callback();
    }
}

function installOnPosix($callback) {
    const prepare = ($isArm, $callback) => {
        let buildPath = cwd + "/build-gcc";
        if ($isArm) {
            buildPath = cwd + "/build-arm";
        }

        const finish = ($skip) => {
            if ($isArm && !$skip) {
                Patcher.getInstance().Patch(
                    cwd + "/platforms/linux/aarch64-gnu.toolchain.cmake",
                    "set(GCC_COMPILER_VERSION \"\" CACHE STRING \"GCC Compiler version\")",
                    "set(GCC_COMPILER_VERSION \"8\" CACHE STRING \"GCC Compiler version\")",
                    () => {
                        $callback($skip, buildPath);
                    },
                    () => {
                        return $isArm;
                    });
            } else {
                $callback($skip, buildPath);
            }
        };

        if (!filesystem.Exists(buildPath)) {
            if (filesystem.CreateDirectory(buildPath)) {
                finish(false);
            } else {
                fail("Can not create build directory.");
            }
        } else if (!filesystem.Exists(buildPath + "/ocv_build")) {
            finish(false);
        } else {
            LogIt.Info("OpenCV build exists. Build task skipped.");
            finish(true);
        }
    };

    const process = function ($isArm, $callback) {
        prepare($isArm, ($skip, $buildPath) => {
            if (!$skip) {
                installGTK($isArm, () => {
                    installFFmpeg($isArm, $buildPath, () => {
                        let cmakeConfigure = "" +
                            "cmake ";
                        if ($isArm) {
                            cmakeConfigure += "-DCMAKE_TOOLCHAIN_FILE=../platforms/linux/aarch64-gnu.toolchain.cmake " +
                                "-DCMAKE_MAKE_PROGRAM=make ";
                        }
                        cmakeConfigure += "" +
                            "-DCMAKE_INSTALL_PREFIX=ocv_build " +
                            "-DWITH_OPENEXR=OFF " +
                            "-DWITH_JPEG=ON " +
                            "-DWITH_TIFF=ON " +
                            "-DWITH_PNG=ON " +
                            "-DWITH_JASPER=ON " +
                            "-DWITH_FFMPEG=ON " +
                            "-DWITH_TBB=OFF " +
                            "-DWITH_LIBV4L=OFF " +
                            "-DWITH_GSTREAMER=OFF " +
                            "-DWITH_1394=OFF " +
                            "-DWITH_QT=OFF " +
                            "-DWITH_OPENGL=ON " +
                            "-DWITH_VTK=ON " +
                            "-DWITH_IPP=OFF " +
                            "-DWITH_ITT=OFF " +
                            "-DWITH_MATLAB=OFF " +
                            "-DWITH_GPHOTO2=OFF " +
                            "-DWITH_CAROTENE=OFF " +
                            "-DBUILD_PNG=ON " +
                            "-DBUILD_ZLIB=ON " +
                            "-DBUILD_EXAMPLES=OFF " +
                            "-DBUILD_DOCS=OFF " +
                            "-DBUILD_PERF_TESTS=OFF " +
                            "-DBUILD_TESTS=OFF " +
                            "-DBUILD_SHARED_LIBS=OFF " +
                            "-DBUILD_WITH_DEBUG_INFO=OFF " +
                            "-DOPENCV_BUILD_3RDPARTY_LIBS=ON " +
                            "..";

                        const cmakePath = cwd + "/CMakeLists.txt";
                        let cmakeData = filesystem.Read(cmakePath).toString()
                            .replace("include(cmake/OpenCVDetectPython.cmake)", "#include(cmake/OpenCVDetectPython.cmake)");
                        filesystem.Rename(cmakePath, cmakePath + ".backup");
                        if (filesystem.Write(cmakePath, cmakeData, false)) {
                            terminal.Spawn(cmakeConfigure, [], $buildPath, ($exitCode) => {
                                if ($exitCode !== 0) {
                                    fail("Configure of OpenCV package failed.");
                                } else {
                                    terminal.Spawn("cmake --build . --target install -- -j" +
                                        EnvironmentHelper.getCores(), [], $buildPath,
                                        ($exitCode) => {
                                            if ($exitCode !== 0) {
                                                fail("Build of OpenCV package failed.");
                                            } else {
                                                LogIt.Info("OpenCV build succeed.");
                                                finalize($callback);
                                            }
                                        });
                                }
                            });
                        }
                    });
                });
            } else {
                finalize($callback);
            }
        });
    };

    process(false, () => {
        process(true, $callback);
    });
}

Process = function ($cwd, $args, $done) {
    cwd = $cwd;
    if (EnvironmentHelper.IsWindows()) {
        installOnWindows($done);
    } else {
        installOnPosix($done);
    }
};
