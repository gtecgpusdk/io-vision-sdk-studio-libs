# * ********************************************************************************************************* *
# *
# * Copyright (c) 2016 Freescale Semiconductor, Inc.
# * Copyright (c) 2017-2018 NXP
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* *

macro(OPENCV_CONFIGURE app_target test_target test_lib_target path version attributes)
    message(STATUS "Running OPENCV_CONFIGURE with (\n"
            "\tapp_target: ${app_target},\n"
            "\ttest_target: ${test_target},\n"
            "\tattributes: ${attributes},\n"
            "\tversion: ${version}\n).")

    PARSE_ARGS("${attributes}")

    if (WIN32)
        set(OTHER_OPENCV_DEPENDENCIES
                vfw32)

        if (${version} STREQUAL "3.4.0" OR ${version} STREQUAL "3.4.4")
            set(OTHER_OPENCV_DEPENDENCIES ${OTHER_OPENCV_DEPENDENCIES}
                    avifil32
                    avicap32
                    winmm
                    msvfw32
                    comctl32
                    gdi32
                    ole32
                    setupapi)
        endif ()
    endif ()

    set(OPENCV_INCLUDE_DIRS)
    set(OPENCV_LIBRARIES)

    set(_BASE_PATH ${path})
    if (NOT IS_ABSOLUTE ${_BASE_PATH})
        set(_BASE_PATH ${CMAKE_SOURCE_DIR}/${_BASE_PATH})
    endif ()

    if (TOOLCHAIN_TYPE STREQUAL "gcc")
        set(OpenCV_DIR "${_BASE_PATH}/build-gcc/ocv_build")
    elseif (TOOLCHAIN_TYPE STREQUAL "arm")
        set(OpenCV_DIR "${_BASE_PATH}/build-arm/ocv_build")
    else ()
        set(OpenCV_DIR "${_BASE_PATH}/build-msvc/ocv_build")
    endif ()
    set(OPENCV_INCLUDE_DIRS ${OpenCV_DIR}/include)

    message("\tinclude: ${OPENCV_INCLUDE_DIRS}\n")

    if (MINGW)
        set(OpenCV_RUNTIME mingw)
        execute_process(COMMAND ${CMAKE_CXX_COMPILER} -dumpmachine
                OUTPUT_VARIABLE OPENCV_GCC_TARGET_MACHINE
                OUTPUT_STRIP_TRAILING_WHITESPACE)
        if (OPENCV_GCC_TARGET_MACHINE MATCHES "amd64|x86_64|AMD64")
            set(MINGW64 1)
            set(OpenCV_ARCH x64)
        else ()
            set(OpenCV_ARCH x86)
        endif ()
    endif ()

    set(OPENCV_LIBRARIES)
    set(OPENCV_SEARCH_PATH_BASE ${OpenCV_DIR}/lib)
    if (MINGW)
        LIST(APPEND CMAKE_FIND_LIBRARY_SUFFIXES 2413.a 310.a 320.a 330.a 340.a 341.a 342.a 344.a)
        set(OPENCV_SEARCH_PATH_BASE ${OpenCV_DIR}/${OpenCV_ARCH}/${OpenCV_RUNTIME}/staticlib)
    else ()
        set(OPENCV_SEARCH_PATH_BASE ${OpenCV_DIR}/lib ${OpenCV_DIR}/share/OpenCV/3rdparty/lib)
    endif ()
    foreach (item ${ARGS_ITEMS})
        find_library(OPENCV_LIB_${item}_FOUND ${item} ${OPENCV_SEARCH_PATH_BASE} NO_DEFAULT_PATH NO_SYSTEM_ENVIRONMENT_PATH)
        if (OPENCV_LIB_${item}_FOUND)
            set(OPENCV_LIBRARIES ${OPENCV_LIBRARIES} ${OPENCV_LIB_${item}_FOUND})
            message("\tlib \"${item}\" found: ${OPENCV_LIB_${item}_FOUND}")
        else ()
            message(FATAL_ERROR "OpenCV lib NOT Found: ${item} in ${OPENCV_SEARCH_PATH_BASE}")
        endif ()
    endforeach ()
    if (MINGW)
        LIST(REMOVE_ITEM CMAKE_FIND_LIBRARY_SUFFIXES 2413.a 310.a 320.a 330.a 340.a 341.a 342.a 344.a)
    endif ()

    target_include_directories(${app_target} SYSTEM PUBLIC ${OPENCV_INCLUDE_DIRS})
    target_include_directories(${test_target} SYSTEM PUBLIC ${OPENCV_INCLUDE_DIRS})
    target_include_directories(${test_lib_target} SYSTEM PUBLIC ${OPENCV_INCLUDE_DIRS})

    target_link_libraries(${app_target} ${OPENCV_LIBRARIES} ${OTHER_OPENCV_DEPENDENCIES})
    target_link_libraries(${test_target} ${OPENCV_LIBRARIES} ${OTHER_OPENCV_DEPENDENCIES})
    target_link_libraries(${test_lib_target} ${OPENCV_LIBRARIES} ${OTHER_OPENCV_DEPENDENCIES})

endmacro()
