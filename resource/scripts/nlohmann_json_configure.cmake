# * ********************************************************************************************************* *
# *
# * Copyright (c) 2017 NXP
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* *

macro(NLOHMANN_JSON_CONFIGURE app_target test_target test_lib_target path version attributes)
    message(STATUS "Running NLOHMANN_JSON_CONFIGURE with (\n\tapp_target: ${app_target},\n\ttest_target: ${test_target},\n\tattributes: ${attributes}\n).")

    PARSE_ARGS("${attributes}")

    set(_BASE_PATH ${path})
    if (NOT IS_ABSOLUTE ${_BASE_PATH})
        set(_BASE_PATH ${CMAKE_SOURCE_DIR}/${_BASE_PATH})
    endif ()

    set(NLOHMANN_JSON_INCLUDE_DIRS ${_BASE_PATH}/src)

    target_include_directories(${app_target} PUBLIC ${NLOHMANN_JSON_INCLUDE_DIRS})
    target_include_directories(${test_target} PUBLIC ${NLOHMANN_JSON_INCLUDE_DIRS})

endmacro()
