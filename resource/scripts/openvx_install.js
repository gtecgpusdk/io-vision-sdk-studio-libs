/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
const Loader = Com.Wui.Framework.Builder.Loader;
const LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
const EnvironmentHelper = Com.Wui.Framework.Localhost.Utils.EnvironmentHelper;
const Patcher = Com.Wui.Framework.Builder.Utils.Patcher;
const terminal = Loader.getInstance().getTerminal();
const filesystem = Loader.getInstance().getFileSystemHandler();
const properties = Loader.getInstance().getAppProperties();
const project = Loader.getInstance().getProjectConfig();
const path = require("path");

let cwd;

function finalize($done) {
    Patcher.getInstance().Rollback($done);
}

function fail($info) {
    finalize(() => {
        LogIt.Error($info);
    });
}

function installOnWindows($callback) {
    let msysRoot = properties.externalModules + "/msys2";
    const arch = "32";
    const conf = "Debug";
    const osName = "Windows";
    let ovxBuildName = "ovx_build";

    const postProcess = ($basePath, $callback) => {
        const paths = filesystem.Expand($basePath + "/**/*.dll");
        let mainPath = "";

        const rename = ($index) => {
            if ($index < paths.length) {
                let name = path.basename(paths[$index]);
                if (name.indexOf("lib") === 0) {
                    filesystem.Rename(paths[$index], paths[$index].replace(name, name.substr(3)));
                    if (name === "libopenvx.dll") {
                        mainPath = path.dirname(paths[$index]);
                    }
                    rename($index + 1);
                } else {
                    rename($index + 1);
                }
            } else {
                if (mainPath.length === 0) {
                    LogIt.Error("Can not localize directory with openvx.dll.");
                }
                $callback();
            }
        };

        rename(0);
    };

    if (!filesystem.Exists(cwd + "/build-gcc/" + ovxBuildName)) {
        const prepare = ($callback) => {
            Patcher.getInstance().Patch(
                cwd + "/CMakeLists.txt",
                "cmake_minimum_required(VERSION 2.8.9)",
                "cmake_minimum_required(VERSION 2.8.9)\n" +
                "set(WIN32 false CACHE INTERNAL \"\")\n" +
                "set(CYGWIN true CACHE INTERNAL \"\")\n",
                () => {
                    Patcher.getInstance().Patch(
                        cwd + "/CMakeLists.txt",
                        "add_subdirectory( sample )\r\n" +
                        "add_subdirectory( sample-c++ )",
                        "add_subdirectory( sample )\r\n" +
                        "#add_subdirectory( sample-c++ )",
                        () => {
                            Patcher.getInstance().Patch(
                                cwd + "/include/VX/vx_types.h",
                                "#define VX_API_CALL __stdcall",
                                "#define VX_API_CALL  // __stdcall",
                                () => {
                                    Patcher.getInstance().Patch(
                                        cwd + "/include/VX/vx_types.h",
                                        "#define VX_CALLBACK __stdcall",
                                        "#define VX_CALLBACK  // __stdcall",
                                        () => {
                                            if (!filesystem.Exists(cwd + "/build-gcc")) {
                                                $callback(filesystem.CreateDirectory(cwd + "/build-gcc"));
                                            } else {
                                                $callback(true);
                                            }
                                        }
                                    )
                                }
                            )
                        },
                        () => {
                            return project.dependencies["openvx"].version === "1.1";
                        }
                    )
                });
        };

        prepare(($status) => {
            if ($status === true) {
                terminal.Spawn(
                    "sh.exe", ["-c \"", "cmake", "-DCMAKE_BUILD_TYPE=" + conf,
                        "-DCMAKE_INSTALL_PREFIX=" + "$(pwd)/install/" + osName + "/" + arch + "/" + conf,
                        "-G\\\"MinGW Makefiles\\\" -DCMAKE_SH=\\\"CMAKE_SH-NOTFOUND\\\" " +
                        "-DCMAKE_CXX_FLAGS=\\\"-static-libstdc++\\\" -DCMAKE_C_FLAGS=\\\"-static-libgcc\\\"",
                        (project.dependencies["openvx"].version === "1.2" ? "-DOPENVX_USE_NN=ON" : ""),
                        "..",
                        "\""],
                    {
                        cwd: cwd + "/build-gcc",
                        env: {
                            PATH: msysRoot + "/usr/bin",
                            NUMBER_OF_PROCESSORS: EnvironmentHelper.getCores()
                        }
                    },
                    (exitCode) => {
                        if (exitCode !== 0) {
                            fail("Build of OpenVX package failed.");
                        } else {
                            terminal.Spawn(
                                "sh.exe", ["-c \"", "cmake --build . --target install -- -j" + EnvironmentHelper.getCores(), "\""],
                                {
                                    cwd: cwd + "/build-gcc",
                                    env: {PATH: msysRoot + "/usr/bin"}
                                },
                                () => {
                                    if (filesystem.CreateDirectory(cwd + "/build-gcc/" + ovxBuildName)) {
                                        const outputPath = cwd + "/build-gcc/" + ovxBuildName;
                                        filesystem.Copy(cwd + "/build-gcc/install/" + osName + "/" + arch + "/" + conf,
                                            outputPath,
                                            ($success) => {
                                                if ($success) {
                                                    postProcess(outputPath, () => {
                                                        LogIt.Info("Openvx build succeed.");
                                                        finalize($callback);
                                                    });
                                                } else {
                                                    fail("Copy of OpenVX build dir failed.");
                                                }
                                            });
                                    } else {
                                        fail("Create of ovx_build dir failed.");
                                    }
                                });
                        }
                    });
            } else {
                fail("Unable to prepare OpenVX cmakelists or build folder.");
            }
        });
    } else {
        LogIt.Info("OpenVX already built.");
        $callback();
    }
}

function installOnPosix($callback) {
    const prepare = ($isArm, $callback) => {
        let buildPath = cwd + "/build-gcc";
        if ($isArm) {
            buildPath = cwd + "/build-arm";
        }

        const finish = ($skip) => {
            if (!$skip) {
                Patcher.getInstance().Patch(
                    cwd + "/CMakeLists.txt",
                    "add_subdirectory( sample-c++ )",
                    "#add_subdirectory( sample-c++ )",
                    () => {
                        Patcher.getInstance().Patch(
                            cwd + "/cmake_utils/CMake_linux_tools.cmake",
                            "  set(ARCH_BIT -m64 )",
                            "  #set(ARCH_BIT -m64 )",
                            () => {
                                Patcher.getInstance().Patch(
                                    cwd + "/sample/CMakeLists.txt",
                                    "add_subdirectory( tests )",
                                    "#add_subdirectory( tests )",
                                    () => {
                                        Patcher.getInstance().Patch(
                                            cwd + "/sample/cnn/CMakeLists.txt",
                                            "add_subdirectory( cnn_test )",
                                            "#add_subdirectory( cnn_test )",
                                            () => {
                                                $callback($skip, buildPath)
                                            },
                                            () => {
                                                return project.dependencies["openvx"].version === "1.2";
                                            }
                                        );
                                    }
                                );
                            },
                            () => {
                                return $isArm;
                            });
                    },
                    () => {
                        return project.dependencies["openvx"].version === "1.1";
                    });
            } else {
                $callback($skip, buildPath);
            }
        };

        if (!filesystem.Exists(buildPath)) {
            if (filesystem.CreateDirectory(buildPath)) {
                finish(false);
            } else {
                fail("Can not create build directory.");
            }
        } else if (!filesystem.Exists(buildPath + "/ovx_build")) {
            finish(false);
        } else {
            LogIt.Info("OpenVX build exists. Build task skipped.");
            finish(true);
        }
    };

    const process = ($isArm, $callback) => {
        prepare($isArm, ($skip, $buildPath) => {
            if (!$skip) {
                const arch = "64";
                const conf = "Release";
                const osName = "Linux";
                const baseArgs = ["Build.py", "--os=" + osName, "--out=" + $buildPath, "--arch=" + arch, "--conf=" + conf];
                let args = ["--c=gcc-7", "--cpp=g++-7"];
                if (project.dependencies["openvx"].version === "1.2") {
                    args.push("--nn")
                }
                if ($isArm) {
                    args = ["--c=aarch64-linux-gnu-gcc-8", "--cpp=aarch64-linux-gnu-g++-8"]
                }
                terminal.Spawn(
                    "python", baseArgs.concat(args),
                    cwd,
                    (exitCode) => {
                        if (exitCode !== 0) {
                            fail("Build of OpenVX package failed.");
                        } else {
                            if (filesystem.CreateDirectory($buildPath + "/ovx_build")) {
                                filesystem.Copy($buildPath + "/install/" + osName + "/x" + arch + "/" + conf,
                                    $buildPath + "/ovx_build",
                                    ($success) => {
                                        if ($success) {
                                            LogIt.Info("OpenVX build succeed.");
                                            finalize($callback);
                                        } else {
                                            fail("Copy of OpenVX build dir failed.");
                                        }
                                    });
                            } else {
                                fail("Create of ovx_build dir failed.");
                            }
                        }
                    });
            } else {
                finalize($callback);
            }
        });
    };

    process(false, () => {
        process(true, $callback);
    });
}

Process = function ($cwd, $args, $done) {
    cwd = $cwd;
    if (EnvironmentHelper.IsWindows()) {
        installOnWindows($done);
    } else {
        installOnPosix($done);
    }
};
