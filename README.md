# io-vision-sdk-studio-libs v2019.1.0

> OpenVX code generator back-end for Vision SDK Studio.

## Requirements

This library depends on the [WUI Builder](https://bitbucket.org/wuiframework/com-wui-framework-builder). 
See the WUI Builder requirements before you build this project.

## Project build

The project build is fully automated. For more information about the project build,
see the [WUI Builder](https://bitbucket.org/wuiframework/com-wui-framework-builder) documentation.

An interface with batch scripts is prepared for Windows users for all common project tasks:

* `install`
* `build`
* `clean`
* `test`
* `run`
* `hotdeploy`
* `docs`

> NOTE: All batch scripts are stored in the **./bin/batch** (or ./bin/bash for linux) sub-folder in the project root folder.

## Documentation

This project provides automatically generated documentation in [Doxygen](http://www.doxygen.org/index.html) 
from the C++ source by running the `docs` command from the {projectRoot}/bin/batch folder.

> NOTE: The documentation is accessible also from the {projectRoot}/build/target/docs/index.html file after a successful creation.

## History

### v2019.1.0
Added support for OVX 1.2 and NN extension. Structure and generated project execution refactoring. Added support for fastcall executor.
### v2019.0.1
Install and configure scripts for opencv and openvx has been refactored to use Patcher from WUI Builder.
Fixed opencv build options for Win platform.
### v2019.0.0
Refactoring of namespaces. Update of install scripts with focus on cross-compile. Usage of OpenCV 3.4.
### v2018.1.2
Added deep OpenVX unit tests. Fixed color conversion and rollback of necessary libs required at runtime.
### v2018.1.1
Change of releasing strategy. Added shared releases configuration.
### v2018.1.0
Fixed conversion between RGB models used in OpenCV and OpenVX.
### v2018.0.4
Added ability to load images from handle with auto recognize of image type. Added unified IOCom and Video IOComs.
Added simplified stack trace and translation table for OVX status code. Code and install scripts clean up.
### v2018.0.3
Update of opencv install suitable for Linux and ARM builds. Update of nlohman json version.
### v2018.0.2
Added full OpenVX 1.1.0 support. Bug fixes for linux build.
### v2018.0.1
Refactoring of IO read/write algorithms. Remove of unused dependencies from release package. 
Initial changes for i.MX and OpenVX 1.1.0 support. Simplified configuration of multi-platform target resources.
### v2018.0.0
Build on Linux is supported. Added support to prepare and build OpenCV and OpenVX. 
### v1.1.0
Lint clean up and code readbility refactoring. Added pulblic API documentation. Added support for vc140 toolchain.
### v1.0.4
Conversion into the WUI Framework project.
### v1.0.3
Added support for run in loop.
### v1.0.2
Added initial implementation of VxUtils. 
### v1.0.1
Basic packaging for required includes and libs.
### v1.0.0
Initial release.

## License

This software is owned or controlled by NXP Semiconductors. 
The use of this software is governed by the BSD-3-Clause Licence distributed with this material.
  
See the `LICENSE.txt` file for more details.

---

Author Jakub Cieslar, 
Copyright (c) 2017-2019 [NXP](http://nxp.com/)
