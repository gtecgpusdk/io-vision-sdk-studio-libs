/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_VISIONSDK_STUDIO_LIBS_HPP_  // NOLINT
#define IO_VISIONSDK_STUDIO_LIBS_HPP_

#include <string>
#include <iostream>
#include <vector>
#include <functional>
#include <thread>
#include <mutex>

#if defined(WIN_PLATFORM)
#include <windows.h>  // NOLINT(build/include_order)
#elif defined(LINUX_PLATFORM)
#include <sys/types.h>
#include <unistd.h>
#endif

#include <VX/vx.h>  // NOLINT(build/include_order)

#if VX_VERSION == VX_VERSION_1_2

#include <VX/vx_khr_nn.h>  // NOLINT(build/include_order)

#endif

#include <opencv2/highgui/highgui.hpp>  // NOLINT(build/include_order)
#include <opencv2/imgproc/imgproc.hpp>  // NOLINT(build/include_order)

#include <json.hpp>

// global-using-start
using std::string;
using std::function;
using std::shared_ptr;
using std::unique_ptr;
using std::weak_ptr;
using nlohmann::json;
// global-using-stop

#include "interfacesMap.hpp"
#include "Io/VisionSDK/Studio/Libs/sourceFilesMap.hpp"

#include "Io/VisionSDK/Studio/Libs/Enums/IOComType.hpp"
#include "Io/VisionSDK/Studio/Libs/Interfaces/IIOCom.hpp"
#include "Io/VisionSDK/Studio/Libs/Interfaces/IManager.hpp"
#include "Io/VisionSDK/Studio/Libs/Media/Video.hpp"
#include "Io/VisionSDK/Studio/Libs/Utils/StopWatch.hpp"
#include "Io/VisionSDK/Studio/Libs/Primitives/BaseObject.hpp"
#include "Io/VisionSDK/Studio/Libs/Primitives/BaseGraph.hpp"
#include "Io/VisionSDK/Studio/Libs/Primitives/BaseContext.hpp"
#include "Io/VisionSDK/Studio/Libs/Primitives/BaseVisualGraph.hpp"

// generated-code-start
#include "io-vision-sdk-studio-libs.hpp"
#include "Io/VisionSDK/Studio/Libs/Media/Camera.hpp"
#include "Io/VisionSDK/Studio/Libs/Media/Display.hpp"
#include "Io/VisionSDK/Studio/Libs/Media/File.hpp"
#include "Io/VisionSDK/Studio/Libs/Media/Image.hpp"
#include "Io/VisionSDK/Studio/Libs/Primitives/Any.hpp"
#include "Io/VisionSDK/Studio/Libs/Primitives/VxReferenceInfo.hpp"
#include "Io/VisionSDK/Studio/Libs/Utils/Convert.hpp"
#include "Io/VisionSDK/Studio/Libs/Utils/FileSystem.hpp"
#include "Io/VisionSDK/Studio/Libs/Utils/Manager.hpp"
#include "Io/VisionSDK/Studio/Libs/Utils/VxDataReader.hpp"
#include "Io/VisionSDK/Studio/Libs/Utils/VxDataWriter.hpp"
#include "Io/VisionSDK/Studio/Libs/Utils/VxTranslator.hpp"
// generated-code-end

#endif  // IO_VISIONSDK_STUDIO_LIBS_HPP_  NOLINT
