/* ********************************************************************************************************* *
*
* Copyright (c) 2018-2019 NXP
*
* SPDX-License-Identifier: BSD-3-Clause
* The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
* or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
*
* ********************************************************************************************************* */

#ifndef IO_VISIONSDK_STUDIO_LIBS_INTERFACES_IIOCOM_HPP_
#define IO_VISIONSDK_STUDIO_LIBS_INTERFACES_IIOCOM_HPP_

namespace Io::VisionSDK::Studio::Libs::Interfaces {
    /**
     * This class defines base interface for all IOComs.
     */
    class IIOCom {
     public:
        /**
         * Get image from IOCom.
         * @param $image Specify reference to output image object.
         * @return Returns VX_SUCCESS or vx_status_e code if failed.
         */
        virtual vx_status getFrame(vx_image &$image, bool $copy) const = 0;

        /**
         * Set image into IOCom.
         * @param $image Specify reference to input image object.
         * @return Returns VX_SUCCESS or vx_status_e code if failed.
         */
        virtual vx_status setFrame(const vx_image &$image) = 0;

        virtual vx_status getData(vx_reference &$object, bool $copy) const = 0;

        virtual vx_status setData(vx_reference &$object) = 0;

        /**
         * @return Returns type of IOCom.
         */
        virtual Io::VisionSDK::Studio::Libs::Enums::IOComType getType() const = 0;
    };
}

#endif  // IO_VISIONSDK_STUDIO_LIBS_INTERFACES_IIOCOM_HPP_
