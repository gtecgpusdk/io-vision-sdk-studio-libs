/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_VISIONSDK_STUDIO_LIBS_INTERFACES_IMANAGER_HPP_
#define IO_VISIONSDK_STUDIO_LIBS_INTERFACES_IMANAGER_HPP_

#include <string>
#include <functional>

#ifdef WIN_PLATFORM
#define API_EXPORT __declspec(dllexport)
#define API_CALL __cdecl
#else
#define API_EXPORT
#define API_CALL
#endif

namespace Io::VisionSDK::Studio::Libs::Interfaces {
    /**
     * This class defines interface for runtime manager.
     */
    class IManager {
     public:
        /**
        * This structure defines options for processing routine.
        */
        struct ProcessOptions {
            bool looped;
            bool headless;
            bool noFileIO;
            bool noCamera;
        };

        /**
         * Create VisualGraph creates each contexts, graphs, data, and nodes.
         * @return Returns casted vx_status, VX_SUCCEED if succeed. Call IManager::Translate() to get more info about not succeed codes.
         */
        virtual int Create() = 0;

        /**
         * Validate whole VisualGraph defined in create task. So IManager::Create() has to be called before this.
         * @return Returns casted vx_status, VX_SUCCEED if succeed. Call IManager::Translate() to get more info about not succeed codes.
         */
        virtual int Validate() = 0;

        /**
         * Invokes processing method. The IManager::Validate() has to be called before this.
         * @param $options Specify options to process VisualGraph with:
         *   - "$options.headless = true" disables internal displays
         *   - "$options.looped = true" causes VisualGraph process is looped internally while loopCondition in Graph is met.
         *   - "$options.noFileIO = true" disables all file IO operations (image/video read/write operations).
         *   - "$options.noCamera = true" disables camera read operation.
         * @return Returns casted vx_status, VX_SUCCESS if succeed. Call IManager::Translate() to get more info about not succeed codes.
         */
        virtual int Process(const ProcessOptions &$options) = 0;

        /**
         * Stops already processing VisualGraph.
         */
        virtual void Stop() = 0;

        /**
         * Releases all resources controlled by manager instance.
         */
        virtual void Release() = 0;

        /**
         * @return Returns true if VisualGraph is in running state, false otherwise.
         */
        virtual bool IsRunning() = 0;

        /**
         * Translate vx_status code to human readable error description.
         * @param $code Specify vx_status to translate.
         * @param $buffer Specify output string buffer.
         * @param $size Specify size of pre-allocated $buffer. The $size argument also contains output message size.
         * @note Note that if size of translated message is greater than size of $buffer only maximal portion of message will be written to
         * $buffer and $size will contains size of full translated message. The same result is given if $buffer in nullptr/NULL.
         */
        virtual void Translate(int $code, char *$buffer, int *$size) = 0;

        /**
         * Reads data value serialized in JSON.
         * @param $path Specify vxData path in form "context<id>.graph<id>.vxData<id>".
         * @param $data Specify output buffer for serialized JSON string.
         * @param $size Specify size of pre-allocated $data buffer. The $size argument also contains serialized data size or -1 when failed.
         * @note Note that if size of serialized data is greater than size of $data no data will be written to $data buffer
         * and $size will contains required $data size. The same result is given if $data is nullptr/NULL.
         * @warning Data are not available before IManager::Create().
         */
        virtual void ReadData(const char *$path, char *$data, int *$size) = 0;

        /**
         * Writes data from serialized JSON into vxData at given path.
         * @param $path Specify vxData path in form "context<id>.graph<id>.vxData<id>".
         * @param $data Specify $data buffer with '\0' terminated serialized JSON string.
         * @return Returns casted vx_status, VX_SUCCESS if succeed. Call IManager::Translate to get more info about not succeed codes.
         * @warning Data are not available before IManager::Create().
         */
        virtual int WriteData(const char *$path, const char *$data) = 0;

        /**
         * Reads image data in raw format.
         * @param $path Specify image vxData path in "context<id>.graph<id>.vxData<id>".
         * @param $buffer Specify output data buffer.
         * @param $size Specify size of pre-allocated $buffer. The $size argument also contains red data size or -1 when failed.
         * @note Note that if size of image data is greater than size of $buffer no data will be written to buffer and $size will
         * contains required $buffer size. The same result is given if $buffer is nullptr/NULL.
         * @warning Data are not available before IManager::Create().
         */
        virtual void ReadImage(const char *$path, char *$buffer, int *$size) = 0;

        /**
         * Writes image data.
         * @param $path Specify image vxData path in "context<id>.graph<id>.vxData<id>".
         * @param $buffer Specify data buffer.
         * @param $size Specify data size in buffer.
         * @return Returns casted vx_status, VX_SUCCESS if succeed. Call IManager:Translate to get more info about not succeed codes.
         * @warning Data are not available before IManager::Create().
         */
        virtual int WriteImage(const char *$path, char *$buffer, int $size) = 0;

        /**
         * Reads VisualGraph stats in serialized JSON string.
         * @param $data Specify output buffer for serialized JSON string.
         * @param $size Specify size of pre-allocated $data buffer. The $size argument also contains serialized data size or -1 when failed.
         * @note Note that if size of serialized data is greater than size of $data buffer no data will be written to $data buffer
         * and $size will contains required $data size. The same result is given if $data is nullptr/NULL.
         */
        virtual void ReadStats(char *$data, int *$size) = 0;

        virtual void RegisterOnStart(const std::function<void(const std::string &)> &$handler) = 0;

        virtual void RegisterOnStop(const std::function<void(const std::string &, int)> &$handler) = 0;

        virtual void RegisterOnError(const std::function<void(const std::string&, const std::string&)> &$handler) = 0;
    };
}

#endif  // IO_VISIONSDK_STUDIO_LIBS_INTERFACES_IMANAGER_HPP_
