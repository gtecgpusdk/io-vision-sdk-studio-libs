/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_VISIONSDK_STUDIO_LIBS_ENUMS_IOCOMTYPE_HPP_
#define IO_VISIONSDK_STUDIO_LIBS_ENUMS_IOCOMTYPE_HPP_

namespace Io::VisionSDK::Studio::Libs::Enums {
    /**
     * This class provides API to convert between image formats OpenVX and OpenCV.
     */
    enum IOComType : int {
        IMAGE,
        CAMERA,
        DISPLAY,
        VIDEO,
        FILE
    };
}

#endif  // IO_VISIONSDK_STUDIO_LIBS_ENUMS_IOCOMTYPE_HPP_
