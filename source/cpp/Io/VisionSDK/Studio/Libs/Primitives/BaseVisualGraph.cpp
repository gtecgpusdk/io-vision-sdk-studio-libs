/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Io::VisionSDK::Studio::Libs::Primitives {
    using Io::VisionSDK::Studio::Libs::Interfaces::IIOCom;

    BaseVisualGraph::~BaseVisualGraph() {
        for (const auto &item : this->contextsMap) {
            delete item.second;
        }
    }

    vx_status BaseVisualGraph::Create() {
        vx_status status = this->create();
        if (status == vx_status_e::VX_SUCCESS) {
            status = vx_status_e::VX_FAILURE;
            for (const auto &item : this->contextsMap) {
                status = item.second->Create();
                if (status != vx_status_e::VX_SUCCESS) {
                    break;
                }
            }
        }
        return status;
    }

    vx_status BaseVisualGraph::Validate() {
        vx_status status = this->validate();
        if (status == vx_status_e::VX_SUCCESS) {
            status = vx_status_e::VX_FAILURE;
            for (const auto &item : this->contextsMap) {
                status = item.second->Validate();
                if (status != vx_status_e::VX_SUCCESS) {
                    break;
                }
            }
        }
        return status;
    }

    vx_status BaseVisualGraph::Process() {
        return this->process(nullptr);
    }

    bool BaseVisualGraph::IsHeadless() const {
        return this->headless;
    }

    bool BaseVisualGraph::IsLooped() const {
        return this->looped;
    }

    void BaseVisualGraph::setLooped(bool $value) {
        this->looped = $value;
    }

    void BaseVisualGraph::setHeadless(bool $value) {
        this->headless = $value;
    }

    bool BaseVisualGraph::IsNoFileIo() const {
        return this->noFileIO;
    }

    void BaseVisualGraph::setNoFileIo(bool $noFileIo) {
        this->noFileIO = $noFileIo;
    }

    bool BaseVisualGraph::IsNoCamera() const {
        return this->noCamera;
    }

    void BaseVisualGraph::setNoCamera(bool $noCamera) {
        this->noCamera = $noCamera;
    }

    std::shared_ptr<IIOCom> BaseVisualGraph::getIoCom(const string &$name) const {
        std::shared_ptr<IIOCom> retPtr = nullptr;
        auto it = this->ioComMap.find($name);
        if (it != this->ioComMap.end()) {
            retPtr = it->second;
        }
        return retPtr;
    }

    const BaseContext *BaseVisualGraph::getContext(const string &$name) const {
        if (this->contextsMap.find($name) != this->contextsMap.end()) {
            return this->contextsMap.at($name);
        }
        return nullptr;
    }

    std::vector<int> BaseVisualGraph::getStats() const {
        return this->processStats;
    }

    void BaseVisualGraph::removeContext(const string &$name) {
        if (this->contextsMap.find($name) != this->contextsMap.end()) {
            delete this->contextsMap.at($name);
            this->contextsMap.erase($name);
        }
    }

    const std::map<string, BaseContext *> &BaseVisualGraph::getContexts() const {
        return this->contextsMap;
    }

    vx_status BaseVisualGraph::create() {
        return vx_status_e::VX_SUCCESS;
    }

    vx_status BaseVisualGraph::validate() {
        return vx_status_e::VX_SUCCESS;
    }

    vx_status BaseVisualGraph::process(const function<vx_status(int)> &$handler) {
        vx_status status = vx_status_e::VX_FAILURE;
        if ($handler != nullptr) {
            int i = 0;
            do {
                this->onStart();
                this->stopWatch.Start();
                status = $handler(i);
                this->stopWatch.Stop();
                auto ts = this->stopWatch.getElapsed();
                this->onStop(ts);
                this->processStats.push_back(ts);
            } while (this->IsLooped() && (status == VX_SUCCESS));
        }
        return status;
    }
}
