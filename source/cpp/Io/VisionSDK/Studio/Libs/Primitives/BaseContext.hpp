/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_VISIONSDK_STUDIO_LIBS_PRIMITIVES_BASECONTEXT_HPP_
#define IO_VISIONSDK_STUDIO_LIBS_PRIMITIVES_BASECONTEXT_HPP_

namespace Io::VisionSDK::Studio::Libs::Primitives {
    /**
     * BaseContext class holds all generalised operations on OpenVX context object domain.
     */
    class BaseContext : public BaseObject {
     public:
        /**
         * Construct BaseContext object from parent visual graph.
         * @param $parent Specify pointer to parent visual graph.
         */
        explicit BaseContext(BaseVisualGraph *$parent);

        /**
         * Destruct BaseContext object.
         */
        ~BaseContext();

        /**
         * @return Returns parent visual graph pointer.
         */
        const BaseVisualGraph *getParent() const;

        /**
         * @return Returns reference to internal vx_context object.
         */
        vx_context getVxContext() const;

        /**
         * Generic getter for graph instances owned by this context.
         * @param $name Specify name of graph to get.
         * @return Returns pointer to requested graph or nullptr if not found.
         */
        const BaseGraph *getGraph(const string &$name) const;

        const std::map<string, BaseGraph *> &getGraphs() const;

        std::vector<int> getStats() const;

        /**
         * Creates internal graphs. Has to be called before process().
         * @return Returns status. VX_SUCCESS if succeed, see vx_status_e for error codes.
         */
        virtual vx_status Create();

        /**
         * Process graph validation.
         * @return Returns status. VX_SUCCESS if succeed, see vx_status_e for error codes.
         */
        virtual vx_status Validate();

        /**
         * Run process routine.
         * @return Returns status. VX_SUCCESS if succeed, see vx_status_e for error codes.
         */
        virtual vx_status Process();

        /**
         * Check reference na print error trace if not valid.
         * @param $referenceInfo Specify reference info.
         * @return Returns status.
         */
        vx_status Check(const VxReferenceInfo &$referenceInfo) const;

        /**
         * Check if status is success, otherwise print error trace.
         * @param $status Specify status.
         * @param $line Tracing info - line number.
         * @param $file Tracing info - file path.
         * @return Returns forwarded $status.
         */
        vx_status Check(const vx_status &$status, int $line = __builtin_LINE(), const string &$file = __builtin_FILE()) const;

        /**
         * Append data to internal log stream. Should by used only internally but can not be private.
         * @param $data Specify data to log.
         */
        void AddLog(const string &$data) const;

     protected:
        virtual vx_status create();
        virtual vx_status validate();
        virtual vx_status process(const std::function<vx_status()> &$handler);
        void removeGraph(const string &$name);

        std::map<string, BaseGraph *> graphsMap = {};
        std::vector<int> processStats = {};

     private:
        BaseVisualGraph *parent = nullptr;
        vx_context vxContext = nullptr;
        std::ostringstream logger{};
        Io::VisionSDK::Studio::Libs::Utils::StopWatch stopWatch = {};
    };
}

#endif  // IO_VISIONSDK_STUDIO_LIBS_PRIMITIVES_BASECONTEXT_HPP_
