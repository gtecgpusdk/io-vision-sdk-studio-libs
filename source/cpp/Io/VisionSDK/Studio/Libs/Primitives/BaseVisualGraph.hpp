/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_VISIONSDK_STUDIO_LIBS_PRIMITIVES_BASEVISUALGRAPH_HPP_
#define IO_VISIONSDK_STUDIO_LIBS_PRIMITIVES_BASEVISUALGRAPH_HPP_

namespace Io::VisionSDK::Studio::Libs::Primitives {
    /**
     * This class holds main application business logic.
     */
    class BaseVisualGraph : public BaseObject {
     public:
        virtual ~BaseVisualGraph();

        /**
         * Creates all contexts aligned with this instance.
         * @return Returns status. VX_SUCCESS if succeed, see vx_status_e for error codes.
         */
        virtual vx_status Create();

        /**
         * Process graph validation.
         * @return Returns status. VX_SUCCESS if succeed, see vx_status_e for error codes.
         */
        virtual vx_status Validate();

        /**
         * Runs processing routine. This method requires initialized and created VisualGraph.
         * @return Returns status. VX_SUCCESS if succeed, see vx_status_e for error codes.
         */
        virtual vx_status Process();

        /**
         * @return Returns true if application should run without displays in native windows, false otherwise.
         */
        bool IsHeadless() const;

        /**
         * @param $value Specify true to allow application displays in native window, false otherwise.
         */
        void setHeadless(bool $value);

        /**
         * @return Returns true if application should run in loop, false otherwise.
         */
        bool IsLooped() const;

        /**
         * @param $value Specify true to force run application in loop, false otherwise.
         */
        void setLooped(bool $value);

        /**
         * @return Returns true if image/video read/write operations are disabled. Use this if all data will be generated and
         * consumed by Probe. False otherwise.
         */
        bool IsNoFileIo() const;

        /**
         * @param noFileIo Specify true to disable image/video read/write operations. Used mostly to fully control data by Probe.
         */
        void setNoFileIo(bool $noFileIo);

        /**
         * @return Returns true if camera read operation are disabled, false otherwise.
         */
        bool IsNoCamera() const;

        /**
         * @param $noCamera Specify true to disable camera read operation.
         */
        void setNoCamera(bool $noCamera);

        /**
         * @param $name Specify name of IOCom.
         * @return Returns smart pointer to requested IOCom or nullptr if not found.
         */
        std::shared_ptr<Io::VisionSDK::Studio::Libs::Interfaces::IIOCom> getIoCom(const string &$name) const;

        const BaseContext *getContext(const string &$name) const;

        const std::map<string, BaseContext *> &getContexts() const;

        std::vector<int> getStats() const;

     protected:
        virtual vx_status create();
        virtual vx_status validate();
        virtual vx_status process(const std::function<vx_status(int)> &$handler);
        void removeContext(const string &$name);

        std::map<string, std::shared_ptr<Io::VisionSDK::Studio::Libs::Interfaces::IIOCom>> ioComMap{};
        std::map<string, BaseContext *> contextsMap{};
        std::map<string, Any> propertiesMap{};

        std::vector<int> processStats = {};
        Io::VisionSDK::Studio::Libs::Utils::StopWatch stopWatch = {};

     private:
        bool headless = false;
        bool looped = false;
        bool noFileIO = false;
        bool noCamera = false;
    };
}

#endif  // IO_VISIONSDK_STUDIO_LIBS_PRIMITIVES_BASEVISUALGRAPH_HPP_
