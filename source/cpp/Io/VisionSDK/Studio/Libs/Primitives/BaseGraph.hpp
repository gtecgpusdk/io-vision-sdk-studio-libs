/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_VISIONSDK_STUDIO_LIBS_PRIMITIVES_BASEGRAPH_HPP_
#define IO_VISIONSDK_STUDIO_LIBS_PRIMITIVES_BASEGRAPH_HPP_

namespace Io::VisionSDK::Studio::Libs::Primitives {
    /**
     * BaseGraph class holds all generalised operations on OpenVX graph.
     */
    class BaseGraph : public BaseObject {
     public:
        static vx_uint32 getImageHeight(vx_reference $image);

        static vx_uint32 getImageWidth(vx_reference $image);

        static vx_df_image_e getImageType(vx_reference $image);

        /**
         * Constructs BaseGraph from parent Context.
         * @param $context Specify parent context pointer.
         */
        explicit BaseGraph(BaseContext *$context);

        /**
         * Destructs BaseGraph object, parent context will NOT be affected.
         */
        ~BaseGraph();

        /**
         * @return Returns pointer to parent context.
         */
        const BaseContext *getParent() const;

        /**
         * @return Returns reference to internal vx_graph instance.
         */
        vx_graph getVxGraph() const;

        /**
         * Generic getter for data instances owned by this graph.
         * @param $name Specify name of data to get.
         * @return Returns reference to requested data or nullptr if not found.
         */
        vx_reference getData(const string &$name) const;

        std::vector<int> getStats() const;

        /**
         * Creates internal nodes and data instances.
         * @return Returns status. VX_SUCCESS if succeed, see vx_status_e for error codes.
         */
        virtual vx_status Create();

        /**
         * Process graph validation.
         * @return Returns status. VX_SUCCESS if succeed, see vx_status_e for error codes.
         */
        virtual vx_status Validate();

        /**
         * Run previously prepared graph.
         * @return Returns status. VX_SUCCESS if succeed, see vx_status_e for error codes.
         */
        virtual vx_status Process();

     protected:
        virtual vx_status create();
        virtual vx_status validate();
        virtual vx_status process(const std::function<vx_status(int)> &$handler);
        vx_reference createImage(vx_context $context, vx_uint32 $width, vx_uint32 $height, vx_df_image $color);
        virtual bool loopCondition(int $loopCnt) const;

        std::map<string, VxReferenceInfo> vxDataMap = {};
        std::map<string, VxReferenceInfo> vxNodesMap = {};
        std::vector<int> processStats = {};

     private:
        BaseContext *parent = nullptr;
        vx_graph vxGraph = nullptr;
        Io::VisionSDK::Studio::Libs::Utils::StopWatch stopWatch = {};
    };
}

#endif  // IO_VISIONSDK_STUDIO_LIBS_PRIMITIVES_BASEGRAPH_HPP_
