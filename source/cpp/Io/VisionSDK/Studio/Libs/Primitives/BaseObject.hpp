/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_VISIONSDK_STUDIO_LIBS_PRIMITIVES_BASEOBJECT_HPP_
#define IO_VISIONSDK_STUDIO_LIBS_PRIMITIVES_BASEOBJECT_HPP_

namespace Io::VisionSDK::Studio::Libs::Primitives {
    class BaseObject {
     public:
        virtual void RegisterOnStart(const std::function<void(const BaseObject *)> &$handler);

        virtual void RegisterOnStop(const std::function<void(const BaseObject *, int)> &$handler);

        virtual void RegisterOnError(const std::function<void(const BaseObject *, const string &)> &$handler);

     protected:
        virtual void onStart() const;

        virtual void onStop(int $timestamp) const;

        virtual void onError(const string &$message) const;

     private:
        std::vector<std::function<void(const BaseObject *)>> onStartHandlers = {};
        std::vector<std::function<void(const BaseObject *, int)>> onStopHandlers = {};
        std::vector<std::function<void(const BaseObject *, const string &)>> onErrorHandlers = {};
    };
}

#endif  // IO_VISIONSDK_STUDIO_LIBS_PRIMITIVES_BASEOBJECT_HPP_
