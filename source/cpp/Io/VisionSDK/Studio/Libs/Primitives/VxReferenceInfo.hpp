/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_VISIONSDK_STUDIO_LIBS_PRIMITIVES_VXREFERENCEINFO_HPP_
#define IO_VISIONSDK_STUDIO_LIBS_PRIMITIVES_VXREFERENCEINFO_HPP_

namespace Io::VisionSDK::Studio::Libs::Primitives {
    /**
     * This class holds trace info for OpenVX objects. Line numbers matches exactly the line where constructor is called.
     * Note that this class is implicitly constructive.
     * So use VxReferenceInfo someInfo = vxCreate<object-type>(...) or = vx<node-function-name>Node(...) which call implict constructor.
     */
    class VxReferenceInfo {
     public:
        /**
         * Default constructor. Necessary for use of this class in common collections. Do not use it directly (will not store tracing data).
         */
        VxReferenceInfo() = default;

        /**
         * Construct reference info with trance data.
         * @param $reference Specify VX object reference.
         * @param $line Leave default to automatically capture line number.
         * @param $file Leave default to automatically capture file name.
         */
        VxReferenceInfo(const vx_reference &$reference, int $line = __builtin_LINE(),
                        const string &$file = __builtin_FILE());  // NOLINT

        /**
         * Cast operator automatically casts vx_reference type to type requested where called.
         * @tparam T Templated type can be any of OpenVX objects.
         * @return Returns type-casted reference.
         */
        template<typename T>
        operator T() const {
            return (T)this->reference;
        }

        /**
         * Cast operator automatically casts vx_reference type to type requested where called.
         * @tparam T Templated type can be any of OpenVX objects.
         * @return Returns type-casted reference.
         */
        template<typename T>
        operator T() {
            return (T)this->reference;
        }

        /**
         * @return Returns line number.
         */
        int getLine() const;

        /**
         * @return Returns file number.
         */
        const string &getFile() const;

     private:
        vx_reference reference = nullptr;
        int line = 0;
        string file;
    };
}

#endif  // IO_VISIONSDK_STUDIO_LIBS_PRIMITIVES_VXREFERENCEINFO_HPP_
