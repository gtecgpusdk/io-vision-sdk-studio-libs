/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Io::VisionSDK::Studio::Libs::Primitives {
    using Io::VisionSDK::Studio::Libs::Utils::StopWatch;

    vx_uint32 BaseGraph::getImageHeight(vx_reference $image) {
        vx_uint32 height = 0;
        vxQueryImage((vx_image)$image, vx_image_attribute_e::VX_IMAGE_HEIGHT, &height, sizeof(height));
        return height;
    }

    vx_uint32 BaseGraph::getImageWidth(vx_reference $image) {
        vx_uint32 width = 0;
        vxQueryImage((vx_image)$image, vx_image_attribute_e::VX_IMAGE_WIDTH, &width, sizeof(width));
        return width;
    }

    vx_df_image_e BaseGraph::getImageType(vx_reference $image) {
        vx_df_image type;
        vxQueryImage((vx_image)$image, vx_image_attribute_e::VX_IMAGE_FORMAT, &type, sizeof(type));
        return vx_df_image_e(type);
    }

    BaseGraph::BaseGraph(BaseContext *$context)
            : parent($context) {
    }

    BaseGraph::~BaseGraph() {
        if (this->vxGraph != nullptr) {
            for (auto it : this->vxDataMap) {
                vx_enum type;
#if VX_VERSION == VX_VERSION_1_0 || VX_VERSION == VX_VERSION_1_1
                vx_reference_attribute_e attrType = vx_reference_attribute_e::VX_REF_ATTRIBUTE_TYPE;
#elif VX_VERSION == VX_VERSION_1_2
                vx_reference_attribute_e attrType = VX_REFERENCE_TYPE;
#else
#error "Check proper vx_reference_attribute_e type for used OpenVX version"
#endif
                vx_status stat = vxQueryReference(it.second, attrType, &type, sizeof(type));
                if (stat == vx_status_e::VX_SUCCESS) {
                    if (type == vx_type_e::VX_TYPE_IMAGE) {
                        auto tmp = (vx_image)it.second;
                        vxReleaseImage(&tmp);
                    } else if (type == vx_type_e::VX_TYPE_SCALAR) {
                        auto tmp = (vx_scalar)it.second;
                        vxReleaseScalar(&tmp);
                    } else if (type == vx_type_e::VX_TYPE_MATRIX) {
                        auto tmp = (vx_matrix)it.second;
                        vxReleaseMatrix(&tmp);
                    } else if (type == vx_type_e::VX_TYPE_ARRAY) {
                        auto tmp = (vx_array)it.second;
                        vxReleaseArray(&tmp);
                    } else if (type == vx_type_e::VX_TYPE_REMAP) {
                        auto tmp = (vx_remap)it.second;
                        vxReleaseRemap(&tmp);
                    } else if (type == vx_type_e::VX_TYPE_CONVOLUTION) {
                        auto tmp = (vx_convolution)it.second;
                        vxReleaseConvolution(&tmp);
                    } else if (type == vx_type_e::VX_TYPE_PYRAMID) {
                        auto tmp = (vx_pyramid)it.second;
                        vxReleasePyramid(&tmp);
                    } else if (type == vx_type_e::VX_TYPE_THRESHOLD) {
                        auto tmp = (vx_threshold)it.second;
                        vxReleaseThreshold(&tmp);
                    } else if (type == vx_type_e::VX_TYPE_DISTRIBUTION) {
                        auto tmp = (vx_distribution)it.second;
                        vxReleaseDistribution(&tmp);
                    } else if (type == vx_type_e::VX_TYPE_LUT) {
                        auto tmp = (vx_lut)it.second;
                        vxReleaseLUT(&tmp);
                    }
                }
            }
            this->vxDataMap.clear();
            vxReleaseGraph(&this->vxGraph);
        }
    }

    const BaseContext *BaseGraph::getParent() const {
        return this->parent;
    }

    vx_status BaseGraph::Create() {
        this->vxGraph = vxCreateGraph(this->parent->getVxContext());
        return this->create();
    }

    vx_status BaseGraph::Validate() {
        vx_status status = this->validate();
        if (status == vx_status_e::VX_SUCCESS) {
            status = vxGetStatus((vx_reference)this->getVxGraph());
            if (status == vx_status_e::VX_SUCCESS) {
                status = this->getParent()->Check(vxVerifyGraph(this->getVxGraph()));
            }
        }
        return status;
    }

    vx_status BaseGraph::Process() {
        return this->process(nullptr);
    }

    vx_graph BaseGraph::getVxGraph() const {
        return this->vxGraph;
    }

    vx_reference BaseGraph::getData(const string &$name) const {
        if (this->vxDataMap.find($name) != this->vxDataMap.end()) {
            return this->vxDataMap.at($name);
        }
        return nullptr;
    }

    std::vector<int> BaseGraph::getStats() const {
        return this->processStats;
    }

    vx_reference BaseGraph::createImage(vx_context $context, vx_uint32 $width, vx_uint32 $height, vx_df_image $color) {
        vx_image image = vxCreateImage($context, $width, $height, $color);
        if ((vxGetStatus((vx_reference)image) == vx_status_e::VX_SUCCESS) && ($width < 10000) && ($height < 10000)) {
            vx_rectangle_t rect;
            rect.start_x = rect.start_y = 0;
            rect.end_x = $width;
            rect.end_y = $height;
            vx_imagepatch_addressing_t addr;
            void *base_ptr = nullptr;

            vx_map_id map;
            vx_status status = vxMapImagePatch(image, &rect, 0, &map, &addr, &base_ptr, vx_accessor_e::VX_READ_AND_WRITE,
                                               vx_memory_type_e::VX_MEMORY_TYPE_HOST, vx_map_flag_e::VX_NOGAP_X);

            if (status == vx_status_e::VX_SUCCESS) {
                vx_uint32 i;

                if ($color == vx_df_image_e::VX_DF_IMAGE_S16) {
                    for (i = 0; i < addr.dim_x * addr.dim_y; i++) {
                        auto *tmp = reinterpret_cast<vx_int16 *>(vxFormatImagePatchAddress1d(base_ptr, i, &addr));
                        *tmp = 0;
                    }
                } else if ($color == vx_df_image_e::VX_DF_IMAGE_U8) {
                    for (i = 0; i < addr.dim_x * addr.dim_y; i++) {
                        auto *tmp = reinterpret_cast<vx_uint8 *>(vxFormatImagePatchAddress1d(base_ptr, i, &addr));
                        *tmp = 0;
                    }
                }

                vxUnmapImagePatch(image, map);
            }
            return (vx_reference)image;
        } else {
            return nullptr;
        }
    }

    bool BaseGraph::loopCondition(int $loopCnt) const {
        return false;
    }

    vx_status BaseGraph::create() {
        return vx_status_e::VX_SUCCESS;
    }

    vx_status BaseGraph::validate() {
        return vx_status_e::VX_SUCCESS;
    }

    vx_status BaseGraph::process(const function<vx_status(int)> &$handler) {
        vx_status status = vx_status_e::VX_FAILURE;
        if ($handler != nullptr) {
            int i = 0;
            do {
                this->onStart();
                this->stopWatch.Start();
                status = $handler(i);
                this->stopWatch.Stop();
                auto ts = this->stopWatch.getElapsed();
                this->onStop(ts);
                this->processStats.push_back(ts);
            } while (this->loopCondition(i++) && (status == VX_SUCCESS));
        }
        return status;
    }
}
