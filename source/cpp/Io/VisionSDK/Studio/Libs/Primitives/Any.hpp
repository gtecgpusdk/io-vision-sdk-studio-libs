/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Denilson das Mercês Amorim
 *
 * Implementation of N4562 std::experimental::any (merged into C++17) for C++11 compilers.
 *
 * See also:
 *   + http://en.cppreference.com/w/cpp/any
 *   + http://en.cppreference.com/w/cpp/experimental/any
 *   + http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2015/n4562.html#any
 *   + https://cplusplus.github.io/LWG/lwg-active.html#2509
 *
 *
 * Distributed under the Boost Software License, Version 1.0. (See accompanying
 * file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
 *
 * *********************************************************************************************************
 *
 * Original file available from https://github.com/thelink2012/any/blob/master/any.hpp has been refactored
 * and extended for project purposes by NXP
 *
 * ********************************************************************************************************* */

#ifndef IO_VISIONSDK_STUDIO_LIBS_PRIMITIVES_ANY_HPP_
#define IO_VISIONSDK_STUDIO_LIBS_PRIMITIVES_ANY_HPP_

#include <typeinfo>
#include <type_traits>
#include <stdexcept>
#include <utility>

namespace detail {
    template<typename ValueType>
    inline ValueType any_cast_move_if_true(typename std::remove_reference<ValueType>::type *p,
                                           std::true_type /*unused*/) {
        return std::move(*p);
    }

    template<typename ValueType>
    inline ValueType any_cast_move_if_true(typename std::remove_reference<ValueType>::type *p,
                                           std::false_type /*unused*/) {
        return *p;
    }
}

namespace Io::VisionSDK::Studio::Libs::Primitives {
    class Any final {
     public:
        class BadAnyCast : public std::bad_cast {
         public:
            const char *what() const noexcept override {
                return "Bad Any cast.";
            }
        };

        Any()
                : vtable(nullptr) {
        }

        Any(const Any &rhs)  // NOLINT(runtime/explicit)
                : vtable(rhs.vtable) {
            if (!rhs.Empty()) {
                rhs.vtable->copy(rhs.storage, this->storage);
            }
        }

        Any(Any &&rhs) noexcept  // NOLINT(runtime/explicit)
                : vtable(rhs.vtable) {
            if (!rhs.Empty()) {
                rhs.vtable->move(rhs.storage, this->storage);
                rhs.vtable = nullptr;
            }
        }

        ~Any() {
            this->Clear();
        }

        template<typename ValueType, typename =
        typename std::enable_if<!std::is_same<typename std::decay<ValueType>::type, Any>::value>::type>
        Any(ValueType &&value) {  // NOLINT(runtime/explicit)
            static_assert(std::is_copy_constructible<typename std::decay<ValueType>::type>::value,
                          "T shall satisfy the CopyConstructible requirements.");
            this->construct(std::forward<ValueType>(value));
        }

        Any &operator=(const Any &$rhs) {
            Any($rhs).Swap(*this);  // NOLINT(runtime/explicit)
            return *this;
        }

        Any &operator=(Any &&$rhs) noexcept {
            Any(std::move($rhs)).Swap(*this);  // NOLINT(runtime/explicit)
            return *this;
        }

        template<typename ValueType, typename =
        typename std::enable_if<!std::is_same<typename std::decay<ValueType>::type, Any>::value>::type>
        Any &operator=(ValueType &&$value) {
            static_assert(std::is_copy_constructible<typename std::decay<ValueType>::type>::value,
                          "T shall satisfy the CopyConstructible requirements.");
            Any(std::forward<ValueType>($value)).Swap(*this);  // NOLINT(runtime/explicit)
            return *this;
        }

        void Clear() noexcept {
            if (!Empty()) {
                this->vtable->destroy(storage);
                this->vtable = nullptr;
            }
        }

        bool Empty() const noexcept {
            return this->vtable == nullptr;
        }

        const std::type_info &getType() const noexcept {
            return Empty() ? typeid(void) : this->vtable->type();
        }

        template<typename ValueType>
        ValueType getValue() {
            return Cast<ValueType>(*this);
        }

        template<typename ValueType>
        ValueType *getValueRef() {
            return Cast<ValueType>(this);
        }

        void Swap(Any &$rhs) noexcept {
            if (this->vtable != $rhs.vtable) {
                Any tmp(std::move($rhs));

                $rhs.vtable = this->vtable;
                if (this->vtable != nullptr) {
                    this->vtable->move(this->storage, $rhs.storage);
                }

                this->vtable = tmp.vtable;
                if (tmp.vtable != nullptr) {
                    tmp.vtable->move(tmp.storage, this->storage);
                    tmp.vtable = nullptr;
                }
            } else {
                if (this->vtable != nullptr)
                    this->vtable->swap(this->storage, $rhs.storage);
            }
        }

        template<typename ValueType>
        static inline ValueType Cast(const Any &$operand) {
            auto p = Cast<typename std::add_const<typename std::remove_reference<ValueType>::type>::type>(&$operand);
            if (p == nullptr) {
                throw BadAnyCast();
            }
            return *p;
        }

        template<typename ValueType>
        static inline ValueType Cast(Any &$operand) {
            auto p = Cast<typename std::remove_reference<ValueType>::type>(&$operand);
            if (p == nullptr) {
                throw BadAnyCast();
            }
            return *p;
        }

        template<typename ValueType>
        static inline ValueType Cast(Any &&$operand) {
            using canMove = std::false_type;

            auto p = Cast<typename std::remove_reference<ValueType>::type>(&$operand);
            if (p == nullptr) {
                throw BadAnyCast();
            }
            return detail::any_cast_move_if_true<ValueType>(p, canMove());
        }

        template<typename T>
        static inline const T *Cast(const Any *$operand) noexcept {
            if ($operand == nullptr || !$operand->IsTyped(typeid(T))) {
                return nullptr;
            } else {
                return $operand->Cast<T>();
            }
        }

        template<typename T>
        static inline T *Cast(Any *$operand) noexcept {
            if ($operand == nullptr || !$operand->IsTyped(typeid(T))) {
                return nullptr;
            } else {
                return $operand->Cast<T>();
            }
        }

     private:
        union storage_union {
            using stack_storage_t = typename std::aligned_storage<
                    2 * sizeof(void *), std::alignment_of<void *>::value>::type;

            void *dynamic;
            stack_storage_t stack;
        };

        struct vtable_type {
            const std::type_info &(*type)() noexcept;

            void (*destroy)(storage_union &) noexcept;

            void (*copy)(const storage_union &src, storage_union &dest);  // NOLINT(runtime/references]

            void (*move)(storage_union &src, storage_union &dest) noexcept;  // NOLINT(runtime/references]

            void (*swap)(storage_union &lhs, storage_union &rhs) noexcept;  // NOLINT(runtime/references]
        };

        template<typename T>
        struct vtable_dynamic {
            static const std::type_info &type() noexcept {
                return typeid(T);
            }

            static void destroy(storage_union &$storage) noexcept {
                delete reinterpret_cast<T *>($storage.dynamic);
            }

            static void copy(const storage_union &$src, storage_union &$dest) {
                $dest.dynamic = new T(*reinterpret_cast<const T *>($src.dynamic));
            }

            static void move(storage_union &$src, storage_union &$dest) noexcept {
                $dest.dynamic = $src.dynamic;
                $src.dynamic = nullptr;
            }

            static void swap(storage_union &$lhs, storage_union &$rhs) noexcept {
                std::swap($lhs.dynamic, $rhs.dynamic);
            }
        };

        template<typename T>
        struct vtable_stack {
            static const std::type_info &type() noexcept {
                return typeid(T);
            }

            static void destroy(storage_union &$storage) noexcept {
                reinterpret_cast<T *>(&$storage.stack)->~T();
            }

            static void copy(const storage_union &$src, storage_union &$dest) {
                new(&$dest.stack) T(reinterpret_cast<const T &>($src.stack));
            }

            static void move(storage_union &$src, storage_union &$dest) noexcept {
                new(&$dest.stack) T(std::move(reinterpret_cast<T &>($src.stack)));
                destroy($src);
            }

            static void swap(storage_union &$lhs, storage_union &$rhs) noexcept {
                std::swap(reinterpret_cast<T &>($lhs.stack), reinterpret_cast<T &>($rhs.stack));
            }
        };

        template<typename T>
        struct requires_allocation :
                std::integral_constant<bool,
                        !(std::is_nothrow_move_constructible<T>::value
                          && sizeof(T) <= sizeof(storage_union::stack)
                          && std::alignment_of<T>::value <=
                             std::alignment_of<storage_union::stack_storage_t>::value)> {
        };

        template<typename T>
        static vtable_type *vtable_for_type() {
            using VTableType =
            typename std::conditional<requires_allocation<T>::value, vtable_dynamic<T>, vtable_stack<T>>::type;
            static vtable_type table = {
                    VTableType::type, VTableType::destroy,
                    VTableType::copy, VTableType::move,
                    VTableType::swap,
            };
            return &table;
        }

     protected:
        template<typename T>
        friend const T *AnyCast(const Any *$operand) noexcept;

        template<typename T>
        friend T *AnyCast(Any *$operand) noexcept;

        bool IsTyped(const std::type_info &$t) const {
            return IsSame(this->getType(), $t);
        }

        static bool IsSame(const std::type_info &$a, const std::type_info &$b) {
            return $a == $b;
        }

        template<typename T>
        const T *Cast() const noexcept {
            return requires_allocation<typename std::decay<T>::type>::value ?
                   reinterpret_cast<const T *>(storage.dynamic) :
                   reinterpret_cast<const T *>(&storage.stack);
        }

        template<typename T>
        T *Cast() noexcept {
            return requires_allocation<typename std::decay<T>::type>::value ?
                   reinterpret_cast<T *>(storage.dynamic) :
                   reinterpret_cast<T *>(&storage.stack);
        }

     private:
        storage_union storage{};
        vtable_type *vtable{nullptr};

        template<typename ValueType, typename T>
        typename std::enable_if<requires_allocation<T>::value>::type
        do_construct(ValueType &&value) {
            storage.dynamic = new T(std::forward<ValueType>(value));
        }

        template<typename ValueType, typename T>
        typename std::enable_if<!requires_allocation<T>::value>::type
        do_construct(ValueType &&value) {
            new(&storage.stack) T(std::forward<ValueType>(value));
        }

        template<typename ValueType>
        void construct(ValueType &&$value) {
            using T = typename std::decay<ValueType>::type;

            this->vtable = vtable_for_type<T>();

            do_construct<ValueType, T>(std::forward<ValueType>($value));
        }
    };
}

namespace std {
    inline void swap(Io::VisionSDK::Studio::Libs::Primitives::Any &$lhs,
                     Io::VisionSDK::Studio::Libs::Primitives::Any &$rhs) noexcept {
        $lhs.Swap($rhs);
    }
}

#endif  // IO_VISIONSDK_STUDIO_LIBS_PRIMITIVES_ANY_HPP_
