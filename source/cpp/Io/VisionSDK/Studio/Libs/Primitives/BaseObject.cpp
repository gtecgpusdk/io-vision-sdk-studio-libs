/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Io::VisionSDK::Studio::Libs::Primitives {

    void BaseObject::RegisterOnStart(const std::function<void(const BaseObject *)> &$handler) {
        this->onStartHandlers.push_back($handler);
    }

    void BaseObject::RegisterOnStop(const std::function<void(const BaseObject *, int)> &$handler) {
        this->onStopHandlers.push_back($handler);
    }

    void BaseObject::RegisterOnError(const std::function<void(const BaseObject *, const string &)> &$handler) {
        this->onErrorHandlers.push_back($handler);
    }

    void BaseObject::onStart() const {
        for (const auto &item : this->onStartHandlers) {
            item(this);
        }
    }

    void BaseObject::onStop(int $timestamp) const {
        for (const auto &item : this->onStopHandlers) {
            item(this, $timestamp);
        }
    }

    void BaseObject::onError(const string &$message) const {
        for (const auto &item : this->onErrorHandlers) {
            item(this, $message);
        }
    }
}
