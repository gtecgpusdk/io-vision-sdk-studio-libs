/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include <list>

#include "../sourceFilesMap.hpp"

namespace Io::VisionSDK::Studio::Libs::Primitives {
    using Io::VisionSDK::Studio::Libs::Utils::VxTranslator;

    static std::list<BaseContext *> contexts{};

    void VX_CALLBACK logHandler(vx_context $context, vx_reference $ref, vx_status $status, const vx_char $string[]) {
        for (auto &item : contexts) {
            if (item->getVxContext() == $context) {
                item->AddLog("[" + VxTranslator::Translate($status) + "]: " + $string);
            }
        }
    }

    BaseContext::BaseContext(BaseVisualGraph *$parent)
            : parent($parent) {
        contexts.emplace_back(this);
        this->logger.clear();
    }

    BaseContext::~BaseContext() {
        if (this->vxContext != nullptr) {
            contexts.remove(this);
            for (const auto &it : this->graphsMap) {
                delete it.second;
            }
            this->graphsMap.clear();
            vxReleaseContext(&this->vxContext);
        }
    }

    vx_status BaseContext::Create() {
        this->vxContext = vxCreateContext();
        vxRegisterLogCallback(this->vxContext, logHandler, _vx_bool_e::vx_false_e);

        vx_status status = this->create();
        if (status == vx_status_e::VX_SUCCESS) {
            status = vxGetStatus((vx_reference)this->getVxContext());
            if (status == vx_status_e::VX_SUCCESS) {
                for (const auto &item : this->graphsMap) {
                    status = item.second->Create();
                    if (status != vx_status_e::VX_SUCCESS) {
                        break;
                    }
                }
            }
        }
        return status;
    }

    vx_status BaseContext::Validate() {
        vx_status status = this->validate();
        if (status == vx_status_e::VX_SUCCESS) {
            status = vxGetStatus((vx_reference)this->getVxContext());
            if (status == vx_status_e::VX_SUCCESS) {
                for (const auto &item : this->graphsMap) {
                    status = item.second->Validate();
                    if (status != vx_status_e::VX_SUCCESS) {
                        break;
                    }
                }
            }
        }
        return status;
    }

    vx_status BaseContext::Process() {
        return this->process(nullptr);
    }

    const BaseVisualGraph *BaseContext::getParent() const {
        return this->parent;
    }

    vx_context BaseContext::getVxContext() const {
        return this->vxContext;
    }

    const BaseGraph *BaseContext::getGraph(const string &$name) const {
        if (this->graphsMap.find($name) != this->graphsMap.end()) {
            return this->graphsMap.at($name);
        }
        return nullptr;
    }

    void BaseContext::removeGraph(const string &$name) {
        if (this->graphsMap.find($name) != this->graphsMap.end()) {
            delete this->graphsMap.at($name);
            this->graphsMap.erase($name);
        }
    }

    vx_status BaseContext::Check(const VxReferenceInfo &$referenceInfo) const {
        return this->Check(vxGetStatus($referenceInfo), $referenceInfo.getLine(), $referenceInfo.getFile());
    }

    vx_status BaseContext::Check(const vx_status &$status, int $line, const string &$file) const {
        if ($status != vx_status_e::VX_SUCCESS) {
            auto index = $file.find_last_of('\\');
            if (index == string::npos) {
                index = $file.find_last_of('/');
            }
            std::ostringstream error;
            error << "Check failed: " << VxTranslator::Translate($status, true) << " in " << $file.substr(index + 1) << ":" << $line
                  << std::endl;
            string logString = this->logger.str();
            if (!logString.empty()) {
                error << "More details: " << logString;
                const_cast<BaseContext *>(this)->logger.clear();
            }
            this->onError(error.str());
        }
        return $status;
    }

    void BaseContext::AddLog(const string &$data) const {
        const_cast<BaseContext *>(this)->logger << $data << std::endl;
    }

    std::vector<int> BaseContext::getStats() const {
        return this->processStats;
    }

    const std::map<string, BaseGraph *> &BaseContext::getGraphs() const {
        return this->graphsMap;
    }

    vx_status BaseContext::create() {
        return vx_status_e::VX_SUCCESS;
    }

    vx_status BaseContext::validate() {
        return vx_status_e::VX_SUCCESS;
    }

    vx_status BaseContext::process(const function<vx_status()> &$handler) {
        vx_status status = vx_status_e::VX_FAILURE;
        if ($handler != nullptr) {
            this->onStart();
            this->stopWatch.Start();
            status = $handler();
            this->stopWatch.Stop();
            auto ts = this->stopWatch.getElapsed();
            this->onStop(ts);
            this->processStats.push_back(ts);
        }
        return status;
    }
}
