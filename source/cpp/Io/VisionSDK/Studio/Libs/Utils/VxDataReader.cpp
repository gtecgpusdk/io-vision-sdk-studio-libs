/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include <algorithm>
#include <regex>

#include "../sourceFilesMap.hpp"

namespace Io::VisionSDK::Studio::Libs::Utils {

    json VxDataReader::readArrayElements(vx_array $input, vx_size $size, vx_type_e $type) {
        void *arrPtr = nullptr;
        vx_size stride;
        vx_status stat;

        json retVal = {{"type", VxTranslator::Translate(vx_type_e::VX_TYPE_ARRAY)}};

#if VX_VERSION == VX_VERSION_1_0
        stat = vxAccessArrayRange($input, 0, $size, &stride, &arrPtr, VX_READ_ONLY);
#elif VX_VERSION == VX_VERSION_1_1 || VX_VERSION == VX_VERSION_1_2
        vx_map_id map_id;
        stat = vxMapArrayRange($input, 0, $size, &map_id, &stride, &arrPtr, VX_READ_ONLY, VX_MEMORY_TYPE_HOST, VX_NOGAP_X);
#else
#error "readArrayElems not available for used OpenVX version"
#endif

        retVal["valueType"] = VxTranslator::Translate($type);
        if (stat == VX_SUCCESS) {
            switch ($type) {
                case VX_TYPE_INT8: {
                    retVal["value"] = readIntegralTypeArray<vx_int8>(arrPtr, stride, $size);
                    break;
                }
                case VX_TYPE_INT16: {
                    retVal["value"] = readIntegralTypeArray<vx_int16>(arrPtr, stride, $size);
                    break;
                }
                case VX_TYPE_INT32: {
                    retVal["value"] = readIntegralTypeArray<vx_int32>(arrPtr, stride, $size);
                    break;
                }
                case VX_TYPE_INT64: {
                    retVal["value"] = readIntegralTypeArray<vx_int64>(arrPtr, stride, $size);
                    break;
                }
                case VX_TYPE_BOOL: {
                    retVal["value"] = readIntegralTypeArray<vx_bool>(arrPtr, stride, $size);
                    break;
                }
                case VX_TYPE_CHAR: {
                    retVal["value"] = readIntegralTypeArray<vx_char>(arrPtr, stride, $size);
                    break;
                }
                case VX_TYPE_FLOAT64: {
                    retVal["value"] = readIntegralTypeArray<vx_float64>(arrPtr, stride, $size);
                    break;
                }
                case VX_TYPE_FLOAT32: {
                    retVal["value"] = readIntegralTypeArray<vx_float32>(arrPtr, stride, $size);
                    break;
                }
                case VX_TYPE_UINT8: {
                    retVal["value"] = readIntegralTypeArray<vx_uint8>(arrPtr, stride, $size);
                    break;
                }
                case VX_TYPE_UINT16: {
                    retVal["value"] = readIntegralTypeArray<vx_uint16>(arrPtr, stride, $size);
                    break;
                }
                case VX_TYPE_UINT32: {
                    retVal["value"] = readIntegralTypeArray<vx_uint32>(arrPtr, stride, $size);
                    break;
                }
                case VX_TYPE_UINT64: {
                    retVal["value"] = readIntegralTypeArray<vx_uint64>(arrPtr, stride, $size);
                    break;
                }
                case VX_TYPE_SIZE: {
                    retVal["value"] = readIntegralTypeArray<vx_size>(arrPtr, stride, $size);
                    break;
                }
                case VX_TYPE_KEYPOINT: {
                    retVal["value"] = readKeyPointsArray(reinterpret_cast<vx_keypoint_t *>(arrPtr), $size);
                    break;
                }
                case VX_TYPE_RECTANGLE: {
                    retVal["value"] = readRectangleArray(reinterpret_cast<vx_rectangle_t *>(arrPtr), $size);
                    break;
                }
                default: {
                    break;
                }
            }
        }

#if VX_VERSION == VX_VERSION_1_0
        stat = vxCommitArrayRange($input, 0, $size, (const void *)arrPtr);
#elif VX_VERSION == VX_VERSION_1_1 || VX_VERSION == VX_VERSION_1_2
        stat = vxUnmapArrayRange($input, map_id);
#endif

        if (stat != VX_SUCCESS) {
            std::cout << "commit fail" << std::endl;
        }
        return retVal;
    }

    json VxDataReader::readKeyPointsArray(vx_keypoint_t *$input, vx_size $size) {
        json retVal = json::array();
        for (vx_size i = 0; i < $size; i++) {
            auto tmp = $input[i];
            retVal.push_back(json({
                                          {"x",               tmp.x},
                                          {"y",               tmp.y},
                                          {"strength",        tmp.strength},
                                          {"scale",           tmp.scale},
                                          {"orientation",     tmp.orientation},
                                          {"tracking_status", tmp.tracking_status},
                                          {"error",           tmp.error}
                                  }));
        }
        return retVal;
    }

    json VxDataReader::readRectangleArray(vx_rectangle_t *$input, vx_size $size) {
        json retVal;
        for (vx_size i = 0; i < $size; i++) {
            auto tmp = $input[i];
            retVal.push_back(json({
                                          {"start_x", tmp.start_x},
                                          {"start_y", tmp.start_y},
                                          {"end_x",   tmp.end_x},
                                          {"end_y",   tmp.end_y}
                                  }));
        }
        return retVal;
    }

    json VxDataReader::readArray(vx_array $input) {
        json retVal;
        vx_status stat;

        std::vector<std::pair<vx_array_attribute_e, string>> attributes;

#if VX_VERSION == VX_VERSION_1_0
        attributes = {
                {VX_ARRAY_ATTRIBUTE_NUMITEMS,  "VX_ARRAY_ATTRIBUTE_NUMITEMS"},
                {VX_ARRAY_ATTRIBUTE_ITEMTYPE,  "VX_ARRAY_ATTRIBUTE_ITEMTYPE"}
        };
#elif VX_VERSION == VX_VERSION_1_1 || VX_VERSION == VX_VERSION_1_2
        attributes = {
                {VX_ARRAY_NUMITEMS, "VX_ARRAY_NUMITEMS"},
                {VX_ARRAY_ITEMTYPE, "VX_ARRAY_ITEMTYPE"}
        };
#else
#error "readArray not available for used OpenVX version"
#endif

        vx_size arrSize;
        stat = vxQueryArray($input, attributes[0].first, &arrSize, sizeof(arrSize));
        if (stat != VX_SUCCESS) {
            std::cout << "size failed" << std::endl;
            return retVal;
        }

        vx_enum type;

        stat = vxQueryArray($input, attributes[1].first, &type, sizeof(type));
        if (stat != VX_SUCCESS) {
            std::cout << "type failed" << std::endl;
            return retVal;
        }

        return readArrayElements($input, arrSize, vx_type_e(type));
    }

    json VxDataReader::readScalar(vx_scalar $input) {
        json scalar = {{"type", VxTranslator::Translate(vx_type_e::VX_TYPE_SCALAR)}};

        vx_scalar_attribute_e scalarAttribute;
#if VX_VERSION == VX_VERSION_1_0
        scalarAttribute = VX_SCALAR_ATTRIBUTE_TYPE;
#elif VX_VERSION == VX_VERSION_1_1 || VX_VERSION == VX_VERSION_1_2
        scalarAttribute = VX_SCALAR_TYPE;
#else
#error "readScalar not available for used OpenVX version"
#endif

        vx_enum scalarType;
        vx_status stat = vxQueryScalar($input, scalarAttribute, &scalarType, sizeof(scalarType));
        if (stat == VX_SUCCESS) {
            scalar["valueType"] = VxTranslator::Translate(vx_type_e(scalarType));
            switch (scalarType) {
                case VX_TYPE_CHAR: {
                    scalar["value"] = readScalarValue<vx_char>($input);
                    break;
                }
                case VX_TYPE_INT8: {
                    scalar["value"] = readScalarValue<vx_int8>($input);
                    break;
                }
                case VX_TYPE_UINT8: {
                    scalar["value"] = readScalarValue<vx_uint8>($input);
                    break;
                }
                case VX_TYPE_INT16: {
                    scalar["value"] = readScalarValue<vx_int16>($input);
                    break;
                }
                case VX_TYPE_UINT16: {
                    scalar["value"] = readScalarValue<vx_uint16>($input);
                    break;
                }
                case VX_TYPE_INT32: {
                    scalar["value"] = readScalarValue<vx_int32>($input);
                    break;
                }
                case VX_TYPE_UINT32: {
                    scalar["value"] = readScalarValue<vx_uint32>($input);
                    break;
                }
                case VX_TYPE_INT64: {
                    scalar["value"] = readScalarValue<vx_int64>($input);
                    break;
                }
                case VX_TYPE_UINT64: {
                    scalar["value"] = readScalarValue<vx_uint64>($input);
                    break;
                }
                case VX_TYPE_FLOAT32: {
                    scalar["value"] = readScalarValue<vx_float32>($input);
                    break;
                }
                case VX_TYPE_FLOAT64: {
                    scalar["value"] = readScalarValue<vx_float64>($input);
                    break;
                }
                case VX_TYPE_ENUM: {
                    scalar["value"] = readScalarValue<vx_int32>($input);
                    break;
                }
                case VX_TYPE_SIZE: {
                    scalar["value"] = readScalarValue<vx_size>($input);
                    break;
                }
                case VX_TYPE_DF_IMAGE: {
                    scalar["value"] = readScalarValue<vx_df_image>($input);
                    break;
                }
                case VX_TYPE_BOOL: {
                    scalar["value"] = readScalarValue<vx_bool>($input);
                    break;
                }
                default: {
                    break;
                }
            }
        }

        return scalar;
    }

    json VxDataReader::readConvolution(vx_convolution $input) {
        json convolution = {{"type", VxTranslator::Translate(vx_type_e::VX_TYPE_CONVOLUTION)}};

        std::vector<std::pair<vx_convolution_attribute_e, string>> attributes;

#if VX_VERSION == VX_VERSION_1_0
        attributes = {
                {VX_CONVOLUTION_ATTRIBUTE_ROWS,    "VX_CONVOLUTION_ATTRIBUTE_ROWS"},
                {VX_CONVOLUTION_ATTRIBUTE_COLUMNS, "VX_CONVOLUTION_ATTRIBUTE_COLUMNS"},
                {VX_CONVOLUTION_ATTRIBUTE_SCALE,   "VX_CONVOLUTION_ATTRIBUTE_SCALE"},
                {VX_CONVOLUTION_ATTRIBUTE_SIZE,    "VX_CONVOLUTION_ATTRIBUTE_SIZE"}
        };
#elif VX_VERSION == VX_VERSION_1_1 || VX_VERSION == VX_VERSION_1_2
        attributes = {
                {VX_CONVOLUTION_ROWS,    "VX_CONVOLUTION_ROWS"},
                {VX_CONVOLUTION_COLUMNS, "VX_CONVOLUTION_COLUMNS"},
                {VX_CONVOLUTION_SCALE,   "VX_CONVOLUTION_SCALE"},
                {VX_CONVOLUTION_SIZE,    "VX_CONVOLUTION_SIZE"}
        };
#else
#error "readConvolution not available for used OpenVX version"
#endif

        convolution["valueType"] = VxTranslator::Translate(vx_type_e::VX_TYPE_INT16);
        auto scale = readConvolutionAttribute<vx_uint32>($input, attributes[2].first);
        convolution["attributes"] = json::array();
        convolution["attributes"].push_back(
                {
                        {"type",      attributes[2].second},
                        {"valueType", VxTranslator::Translate(vx_type_e::VX_TYPE_UINT32)},
                        {"value",     scale}
                });

        convolution["value"] = readConvolutionElements<vx_int16>($input,
                                                                 readConvolutionAttribute<vx_size>($input, attributes[0].first),
                                                                 readConvolutionAttribute<vx_size>($input, attributes[1].first));

        return convolution;
    }

    json VxDataReader::readDistribution(vx_distribution $input) {
        json distribution = {{"type", VxTranslator::Translate(vx_type_e::VX_TYPE_DISTRIBUTION)}};

        vx_distribution_attribute_e attribute;
#if VX_VERSION == VX_VERSION_1_0
        attribute = vx_distribution_attribute_e::VX_DISTRIBUTION_ATTRIBUTE_BINS;
#elif VX_VERSION == VX_VERSION_1_1 || VX_VERSION == VX_VERSION_1_2
        attribute = vx_distribution_attribute_e::VX_DISTRIBUTION_BINS;
#else
#error "readDistribution not available for used OpenVX version"
#endif

        distribution["valueType"] = VxTranslator::Translate(vx_type_e::VX_TYPE_UINT32);
        distribution["value"] = readDistributionElements<vx_uint32>($input,
                                                                    readDistributionAttribute<vx_size>($input, attribute));

        return distribution;
    }

    json VxDataReader::readLut(vx_lut $input) {
        json lut = {{"type", VxTranslator::Translate(vx_type_e::VX_TYPE_LUT)}};

        std::vector<std::pair<vx_lut_attribute_e, string>> attributes;

#if VX_VERSION == VX_VERSION_1_0
        attributes = {
                {VX_LUT_ATTRIBUTE_TYPE,  "VX_LUT_ATTRIBUTE_TYPE"},
                {VX_LUT_ATTRIBUTE_COUNT, "VX_LUT_ATTRIBUTE_COUNT"}
        };
#elif VX_VERSION == VX_VERSION_1_1 || VX_VERSION == VX_VERSION_1_2
        attributes = {
                {VX_LUT_TYPE,  "VX_LUT_TYPE"},
                {VX_LUT_COUNT, "VX_LUT_COUNT"}
        };
#else
#error "readLut not available for used OpenVX version"
#endif

        vx_status stat;

        vx_enum lutElemType;
        stat = vxQueryLUT($input, attributes[0].first, &lutElemType, sizeof(lutElemType));
        if (stat != VX_SUCCESS) {
            return {};
        }

        vx_size lutSize;
        stat = vxQueryLUT($input, attributes[1].first, &lutSize, sizeof(lutSize));
        if (stat != VX_SUCCESS) {
            return {};
        }

        lut["valueType"] = VxTranslator::Translate(vx_type_e(lutElemType));
        switch (lutElemType) {
            case VX_TYPE_UINT8: {
                lut["value"] = readLutElements<vx_uint8>($input, lutSize);
                break;
            }
            case VX_TYPE_INT16: {
                lut["value"] = readLutElements<vx_int16>($input, lutSize);
                break;
            }
            default: {
                break;
            }
        }

        return lut;
    }

    json VxDataReader::readMatrix(vx_matrix $input) {
        json matrix = {{"type", VxTranslator::Translate(vx_type_e::VX_TYPE_MATRIX)}};

        std::vector<std::pair<vx_matrix_attribute_e, string>> attributes;

#if VX_VERSION == VX_VERSION_1_0
        attributes = {
                {VX_MATRIX_ATTRIBUTE_TYPE,    "VX_MATRIX_ATTRIBUTE_TYPE"},
                {VX_MATRIX_ATTRIBUTE_ROWS,    "VX_MATRIX_ATTRIBUTE_ROWS"},
                {VX_MATRIX_ATTRIBUTE_COLUMNS, "VX_MATRIX_ATTRIBUTE_COLUMNS"}
        };
#elif VX_VERSION == VX_VERSION_1_1 || VX_VERSION == VX_VERSION_1_2
        attributes = {
                {VX_MATRIX_TYPE,    "VX_MATRIX_TYPE"},
                {VX_MATRIX_ROWS,    "VX_MATRIX_ROWS"},
                {VX_MATRIX_COLUMNS, "VX_MATRIX_COLUMNS"}
        };
#else
#error "readMatrix not available for used OpenVX version"
#endif


        vx_enum matrixElemType;
        vx_status stat = vxQueryMatrix($input, attributes[0].first, &matrixElemType, sizeof(matrixElemType));
        matrix["valueType"] = VxTranslator::Translate(vx_type_e(matrixElemType));
        if (stat != VX_SUCCESS) {
            return {};
        }

        vx_size rows;
        stat = vxQueryMatrix($input, attributes[1].first, &rows, sizeof(rows));
        if (stat != VX_SUCCESS) {
            return {};
        }

        vx_size cols;
        stat = vxQueryMatrix($input, attributes[2].first, &cols, sizeof(cols));
        if (stat != VX_SUCCESS) {
            return {};
        }

        switch (matrixElemType) {
            case VX_TYPE_UINT8: {
                matrix["value"] = readMatrixElements<vx_uint8>($input, rows, cols);
                break;
            }
            case VX_TYPE_INT32: {
                matrix["value"] = readMatrixElements<vx_int32>($input, rows, cols);
                break;
            }
            case VX_TYPE_FLOAT32: {
                matrix["value"] = readMatrixElements<vx_float32>($input, rows, cols);
                break;
            }
            default: {
                break;
            }
        }

        return matrix;
    }

    json VxDataReader::readPyramid(vx_pyramid $input) {
        json pyramid = {{"type", VxTranslator::Translate(vx_type_e::VX_TYPE_PYRAMID)}};

        std::vector<std::pair<vx_pyramid_attribute_e, string>> attributes;

#if VX_VERSION == VX_VERSION_1_0
        attributes = {
                {VX_PYRAMID_ATTRIBUTE_LEVELS,  "VX_PYRAMID_ATTRIBUTE_LEVELS"},
                {VX_PYRAMID_ATTRIBUTE_SCALE, "VX_PYRAMID_ATTRIBUTE_SCALE"},
                {VX_PYRAMID_ATTRIBUTE_WIDTH, "VX_PYRAMID_ATTRIBUTE_WIDTH"},
                {VX_PYRAMID_ATTRIBUTE_HEIGHT, "VX_PYRAMID_ATTRIBUTE_HEIGHT"},
                {VX_PYRAMID_ATTRIBUTE_FORMAT, "VX_PYRAMID_ATTRIBUTE_FORMAT"}
        };
#elif VX_VERSION == VX_VERSION_1_1 || VX_VERSION == VX_VERSION_1_2
        attributes = {
                {VX_PYRAMID_LEVELS, "VX_PYRAMID_LEVELS"},
                {VX_PYRAMID_SCALE,  "VX_PYRAMID_SCALE"},
                {VX_PYRAMID_WIDTH,  "VX_PYRAMID_WIDTH"},
                {VX_PYRAMID_HEIGHT, "VX_PYRAMID_HEIGHT"},
                {VX_PYRAMID_FORMAT, "VX_PYRAMID_FORMAT"}
        };
#else
#error "readPyramid not available for used OpenVX version"
#endif

        json pyramidValue;
        pyramidValue.push_back(readPyramidAttribute<vx_size>($input, attributes[0].first));
        pyramidValue.push_back(readPyramidAttribute<vx_float32>($input, attributes[1].first));
        pyramidValue.push_back(readPyramidAttribute<vx_uint32>($input, attributes[2].first));
        pyramidValue.push_back(readPyramidAttribute<vx_uint32>($input, attributes[3].first));
        pyramidValue.push_back(readPyramidAttribute<vx_df_image>($input, attributes[4].first));

        pyramid["value"] = pyramidValue;
        return pyramid;
    }

    json VxDataReader::readRemap(vx_remap $input) {
        json remap = {{"type", VxTranslator::Translate(vx_type_e::VX_TYPE_REMAP)}};

        std::vector<std::pair<vx_remap_attribute_e, string>> attributes;

#if VX_VERSION == VX_VERSION_1_0
        attributes = {
                {VX_REMAP_ATTRIBUTE_SOURCE_WIDTH, "VX_REMAP_ATTRIBUTE_SOURCE_WIDTH"},
                {VX_REMAP_ATTRIBUTE_SOURCE_HEIGHT,  "VX_REMAP_ATTRIBUTE_SOURCE_HEIGHT"},
                {VX_REMAP_ATTRIBUTE_DESTINATION_WIDTH,  "VX_REMAP_ATTRIBUTE_DESTINATION_WIDTH"},
                {VX_REMAP_ATTRIBUTE_DESTINATION_HEIGHT, "VX_REMAP_ATTRIBUTE_DESTINATION_HEIGHT"}
        };
#elif VX_VERSION == VX_VERSION_1_1 || VX_VERSION == VX_VERSION_1_2
        attributes = {
                {VX_REMAP_SOURCE_WIDTH,       "VX_REMAP_SOURCE_WIDTH"},
                {VX_REMAP_SOURCE_HEIGHT,      "VX_REMAP_SOURCE_HEIGHT"},
                {VX_REMAP_DESTINATION_WIDTH,  "VX_REMAP_DESTINATION_WIDTH"},
                {VX_REMAP_DESTINATION_HEIGHT, "VX_REMAP_DESTINATION_HEIGHT"}
        };
#else
#error "readRemap not available for used OpenVX version"
#endif

        auto dw = readRemapAttribute<vx_uint32>($input, attributes[2].first);
        auto dh = readRemapAttribute<vx_uint32>($input, attributes[3].first);

        json remapValue = json::array();
        vx_float32 srcX, srcY;

        auto pushItem = [&](vx_uint32 $destX, vx_uint32 $destY, vx_float32 $srcX, vx_float32 $srcY) {
            remapValue.push_back(
                    {
                            {"dest_x", $destX},
                            {"dest_y", $destY},
                            {"src_x",  $srcX},
                            {"src_y",  $srcY}
                    });
        };

#if VX_VERSION == VX_VERSION_1_0 || VX_VERSION == VX_VERSION_1_1
        for (vx_uint32 j = 0; j < dh; j++) {
            for (vx_uint32 i = 0; i < dw; i++) {
                vxGetRemapPoint($input, i, j, &srcX, &srcY);
                pushItem(i, j, srcX, srcY);
            }
        }
#elif VX_VERSION == VX_VERSION_1_2
        vx_rectangle_t rect = {.start_x = 0, .start_y = 0, .end_x = dw, .end_y = dh};
        auto *coords = new vx_coordinates2df_t[dw * dh];

        vxCopyRemapPatch($input, &rect, sizeof(vx_coordinates2d_t) * dw, coords, vx_type_e::VX_TYPE_COORDINATES2DF,
                         vx_accessor_e::VX_READ_ONLY, vx_memory_type_e::VX_MEMORY_TYPE_HOST);
        for (vx_uint32 j = 0; j < dh; j++) {
            for (vx_uint32 i = 0; i < dw; i++) {
                srcX = coords[i + (j * dw)].x;
                srcY = coords[i + (j * dw)].y;
                pushItem(i, j, srcX, srcY);
            }
        }

        delete[] coords;
#else
#warning "readRemap not available for used OpenVX version"
#endif

        remap["value"] = remapValue;
        return remap;
    }

    json VxDataReader::readThreshold(vx_threshold $input) {
        json threshold = {{"type", VxTranslator::Translate(vx_type_e::VX_TYPE_THRESHOLD)}};

        std::vector<std::pair<vx_enum, string>> attributes;

#if VX_VERSION == VX_VERSION_1_0
        attributes = {
                {VX_THRESHOLD_ATTRIBUTE_TYPE,            "VX_THRESHOLD_ATTRIBUTE_TYPE"},
                {VX_THRESHOLD_ATTRIBUTE_THRESHOLD_VALUE, "VX_THRESHOLD_ATTRIBUTE_THRESHOLD_VALUE"},
                {VX_THRESHOLD_ATTRIBUTE_THRESHOLD_LOWER, "VX_THRESHOLD_ATTRIBUTE_THRESHOLD_LOWER"},
                {VX_THRESHOLD_ATTRIBUTE_THRESHOLD_UPPER, "VX_THRESHOLD_ATTRIBUTE_THRESHOLD_UPPER"},
                {VX_THRESHOLD_ATTRIBUTE_TRUE_VALUE,      "VX_THRESHOLD_ATTRIBUTE_TRUE_VALUE"},
                {VX_THRESHOLD_ATTRIBUTE_FALSE_VALUE,     "VX_THRESHOLD_ATTRIBUTE_FALSE_VALUE"}
        };
#elif VX_VERSION == VX_VERSION_1_1
        attributes = {
                {VX_THRESHOLD_TYPE,            "VX_THRESHOLD_TYPE"},
                {VX_THRESHOLD_THRESHOLD_VALUE, "VX_THRESHOLD_THRESHOLD_VALUE"},
                {VX_THRESHOLD_THRESHOLD_LOWER, "VX_THRESHOLD_THRESHOLD_LOWER"},
                {VX_THRESHOLD_THRESHOLD_UPPER, "VX_THRESHOLD_THRESHOLD_UPPER"},
                {VX_THRESHOLD_TRUE_VALUE,      "VX_THRESHOLD_TRUE_VALUE"},
                {VX_THRESHOLD_FALSE_VALUE,     "VX_THRESHOLD_FALSE_VALUE"}
        };
#elif VX_VERSION == VX_VERSION_1_2
        attributes = {
                {VX_THRESHOLD_TYPE,         "VX_THRESHOLD_TYPE"},
                {0,                         "VX_THRESHOLD_THRESHOLD_VALUE"},
                {0,                         "VX_THRESHOLD_THRESHOLD_LOWER"},
                {0,                         "VX_THRESHOLD_THRESHOLD_UPPER"},
                {0,                         "VX_THRESHOLD_TRUE_VALUE"},
                {0,                         "VX_THRESHOLD_FALSE_VALUE"},
                {VX_THRESHOLD_INPUT_FORMAT, "VX_THRESHOLD_INPUT_FORMAT"}
        };
#endif

        threshold["attributes"] = json::array();
        auto threshType = (vx_threshold_type_e)readThresholdAttribute<vx_enum>(
                $input, static_cast<vx_threshold_attribute_e>(attributes[0].first));

        vx_int32 thLower = 0, thUpper = 0, thValue = 0, thFalseValue = 0, thTrueValue = 0;

#if VX_VERSION == VX_VERSION_1_1 || VX_VERSION == VX_VERSION_1_0
        thLower = readThresholdAttribute<vx_int32>($input, static_cast<vx_threshold_attribute_e>(attributes[2].first));
        thUpper = readThresholdAttribute<vx_int32>($input, static_cast<vx_threshold_attribute_e>(attributes[3].first));
        thValue = readThresholdAttribute<vx_int32>($input, static_cast<vx_threshold_attribute_e>(attributes[1].first));
        thTrueValue = readThresholdAttribute<vx_int32>($input, static_cast<vx_threshold_attribute_e>(attributes[4].first));
        thFalseValue = readThresholdAttribute<vx_int32>($input, static_cast<vx_threshold_attribute_e>(attributes[5].first));

#else
        vx_pixel_value_t pt1, pt2;
        auto type = readThresholdAttribute<vx_df_image_e>($input, static_cast<vx_threshold_attribute_e>(attributes[6].first));
        auto getPixelValue = [&](const vx_pixel_value_t &$pixel) -> vx_int32 {
            vx_int32 retVal;
            if (type == vx_df_image_e::VX_DF_IMAGE_S16) {
                retVal = $pixel.S16;
            } else {
                retVal = $pixel.U8;
            }
            return retVal;
        };
        vxCopyThresholdRange($input, &pt1, &pt2, vx_accessor_e::VX_READ_ONLY, vx_memory_type_e::VX_MEMORY_TYPE_HOST);
        thLower = getPixelValue(pt1);
        thUpper = getPixelValue(pt2);
        vxCopyThresholdOutput($input, &pt1, &pt2, vx_accessor_e::VX_READ_ONLY, vx_memory_type_e::VX_MEMORY_TYPE_HOST);
        thFalseValue = getPixelValue(pt2);
        thTrueValue = getPixelValue(pt1);
        vxCopyThresholdValue($input, &pt1, vx_accessor_e::VX_READ_ONLY, vx_memory_type_e::VX_MEMORY_TYPE_HOST);
        thValue = getPixelValue(pt1);
#endif

        if (threshType == vx_threshold_type_e::VX_THRESHOLD_TYPE_RANGE) {
            threshold["attributes"].push_back({
                                                      {"type",  attributes[2].second},
                                                      {"value", thLower}
                                              });
            threshold["attributes"].push_back({
                                                      {"type",  attributes[3].second},
                                                      {"value", thUpper}
                                              });
        } else {
            threshold["attributes"].push_back({
                                                      {"type",  attributes[1].second},
                                                      {"value", thValue}
                                              });
        }
        threshold["attributes"].push_back({
                                                  {"type",  attributes[4].second},
                                                  {"value", thTrueValue}
                                          });
        threshold["attributes"].push_back({
                                                  {"type",  attributes[5].second},
                                                  {"value", thFalseValue}
                                          });

        return threshold;
    }

    json VxDataReader::Process(vx_reference $input) {
        json retVal = {};
        vx_enum requestType;
        vx_reference_attribute_e attributeType;
#if VX_VERSION == VX_VERSION_1_0 || VX_VERSION == VX_VERSION_1_1
        attributeType = VX_REF_ATTRIBUTE_TYPE;
#elif VX_VERSION == VX_VERSION_1_2
        attributeType = VX_REFERENCE_TYPE;
#else
#error "Check proper vx_reference_attribute_e type for used OpenVX version"
#endif
        vx_status stat = vxQueryReference($input, attributeType, &requestType, sizeof(requestType));
        if (stat == VX_SUCCESS) {
            switch (requestType) {
                case VX_TYPE_ARRAY: {
                    retVal = readArray((vx_array)$input);
                    break;
                }
                case VX_TYPE_SCALAR: {
                    retVal = readScalar((vx_scalar)$input);
                    break;
                }
                case VX_TYPE_CONVOLUTION: {
                    retVal = readConvolution((vx_convolution)$input);
                    break;
                }
                case VX_TYPE_DISTRIBUTION: {
                    retVal = readDistribution((vx_distribution)$input);
                    break;
                }
                case VX_TYPE_LUT: {
                    retVal = readLut((vx_lut)$input);
                    break;
                }
                case VX_TYPE_MATRIX: {
                    retVal = readMatrix((vx_matrix)$input);
                    break;
                }
                case VX_TYPE_PYRAMID: {
                    retVal = readPyramid((vx_pyramid)$input);
                    break;
                }
                case VX_TYPE_REMAP: {
                    retVal = readRemap((vx_remap)$input);
                    break;
                }
                case VX_TYPE_THRESHOLD: {
                    retVal = readThreshold((vx_threshold)$input);
                    break;
                }
                case VX_TYPE_IMAGE: {
                    retVal = {{"type", VxTranslator::Translate(vx_type_e::VX_TYPE_IMAGE)}};
                    break;
                }
                default: {
                    break;
                }
            }
        }
        return retVal;
    }

    json VxDataReader::Process(const string &$path, const std::map<string, vx_reference> &$map) {
        json retVal = {};
        auto it = $map.find($path);
        if (it != $map.end()) {
            retVal = VxDataReader::Process(it->second);
        }
        return retVal;
    }
}
