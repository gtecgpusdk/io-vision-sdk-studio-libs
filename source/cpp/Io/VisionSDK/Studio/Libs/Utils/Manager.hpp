/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_VISIONSDK_STUDIO_LIBS_UTILS_MANAGER_HPP_
#define IO_VISIONSDK_STUDIO_LIBS_UTILS_MANAGER_HPP_

namespace Io::VisionSDK::Studio::Libs::Utils {
    /**
     * This class implements runtime manager.
     */
    class Manager : public Io::VisionSDK::Studio::Libs::Interfaces::IManager {
     public:
        virtual ~Manager();

        int Create() override;

        int Validate() override;

        int Process(const ProcessOptions &$options = {.looped = false, .headless = false}) override;

        void Stop() override;

        void Release() override;

        bool IsRunning() override;

        void Translate(int $code, char *$buffer, int *$size) override;

        void ReadData(const char *$path, char *$data, int *$size) override;

        int WriteData(const char *$path, const char *$data) override;

        void ReadImage(const char *$path, char *$buffer, int *$size) override;

        int WriteImage(const char *$path, char *$buffer, int $size) override;

        void ReadStats(char *$data, int *$size) override;

        void RegisterOnStart(const std::function<void(const string &)> &$handler) override;

        void RegisterOnStop(const std::function<void(const string &, int)> &$handler) override;

        void RegisterOnError(const std::function<void(const string &, const string &)> &$handler) override;

     protected:
        virtual bool IsCreated();

        virtual bool IsValidated();

        virtual void setCreated(bool $value);

        virtual void setRunning(bool $value);

        virtual void setValidated(bool $value);

        virtual void setVisualGraph(Io::VisionSDK::Studio::Libs::Primitives::BaseVisualGraph *$visualGraph);

     private:
        static std::vector<string> parseDataPath(const string &$path);

        vx_reference resolveData(const string &$path);

        string resolveObjectName(const Io::VisionSDK::Studio::Libs::Primitives::BaseObject *$object);

        bool isCreated = false;
        bool isRunning = false;
        bool isValidated = false;

        Io::VisionSDK::Studio::Libs::Primitives::BaseVisualGraph *visualGraph = nullptr;
        std::map<vx_reference, cv::Mat> convertCache = {};
    };
}

#endif  // IO_VISIONSDK_STUDIO_LIBS_UTILS_MANAGER_HPP_
