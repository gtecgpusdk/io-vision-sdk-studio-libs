/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_VISIONSDK_STUDIO_LIBS_UTILS_FILESYSTEM_HPP_
#define IO_VISIONSDK_STUDIO_LIBS_UTILS_FILESYSTEM_HPP_

#ifdef DeleteFile
#undef DeleteFile
#endif

#ifdef CreateDirectory
#undef CreateDirectory
#endif

namespace Io::VisionSDK::Studio::Libs::Utils {
    /**
     * This class provides basic API for operations with filesystem.
     */
    class FileSystem {
     public:
        /**
         * Check path existence.
         * @param $path Specify path.
         * @return Returns true if exists, false otherwise.
         */
        static bool Exists(const string &$path);

        /**
         * @return Returns temporary directory path.
         */
        static string getTemp();

        /**
         * Creates directory.
         * @param $path Specify directory path to be created.
         */
        static void CreateDirectory(const string &$path);

        /**
         * Delete file.
         * @param $path specify file path to be deleted.
         */
        static void DeleteFile(const string &$path);
    };
}

#endif  // IO_VISIONSDK_STUDIO_LIBS_UTILS_FILESYSTEM_HPP_
