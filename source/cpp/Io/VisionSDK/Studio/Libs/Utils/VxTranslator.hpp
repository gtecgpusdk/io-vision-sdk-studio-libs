/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_VISIONSDK_STUDIO_LIBS_UTILS_VXTRANSLATOR_HPP_
#define IO_VISIONSDK_STUDIO_LIBS_UTILS_VXTRANSLATOR_HPP_

namespace Io::VisionSDK::Studio::Libs::Utils {
    /**
     * This class provides methods to translate OpenVX specific enums, error codes, etc. to readable strings.
     */
    class VxTranslator {
     public:
        /**
         * Translate status code to enumerator string or enumerator string with detailed description.
         * @param $status Specify status to translate.
         * @param $withDescription Specify true to return translated status with description.
         * @return Returns translated status string.
         */
        static string Translate(vx_status $status, bool $withDescription = false);

        /**
         * Translate type enum to string
         * @param $type Specify OpenVX type to translate.
         * @return Returns translated type string.
         */
        static string Translate(vx_type_e $type);

        /**
         * Translate string to OpenVX type.
         * @param $type Specify type string.
         * @return Returns translated OpenVX type.
         */
        static vx_type_e Translate(const string &$type);
    };
}

#endif  // IO_VISIONSDK_STUDIO_LIBS_UTILS_VXTRANSLATOR_HPP_
