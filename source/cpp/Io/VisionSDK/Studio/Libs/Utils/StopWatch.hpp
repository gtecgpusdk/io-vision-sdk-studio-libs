/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_VISIONSDK_STUDIO_LIBS_UTILS_STOPWATCH_HPP_
#define IO_VISIONSDK_STUDIO_LIBS_UTILS_STOPWATCH_HPP_

namespace Io::VisionSDK::Studio::Libs::Utils {
    /**
     * This class provides stopwatch functionality.
     */
    class StopWatch {
     public:
        /**
         * Start measurement.
         */
        void Start();

        /**
         * Stop measurement.
         */
        void Stop();

        /**
         * @return Returns elapsed time. Note that measurement has to be stopped before, otherwise returns zero.
         */
        long getElapsed() const;

     private:
        long getCurrentTs();

        long startTime;
        long elapsed;
    };
}

#endif  // IO_VISIONSDK_STUDIO_LIBS_UTILS_STOPWATCH_HPP_
