/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_VISIONSDK_STUDIO_LIBS_UTILS_VXDATAWRITER_HPP_
#define IO_VISIONSDK_STUDIO_LIBS_UTILS_VXDATAWRITER_HPP_

namespace Io::VisionSDK::Studio::Libs::Utils {

    class VxDataWriter {
     public:
        /**
         * Writes data from JSON to vxData object defined by path.
         * @param $path Specify vxData path to write.
         * @param $map Map which holds references to objects that can be read.
         * @param $data Specify vxData in JSON.
         * @return Returns vx_status, VX_SUCCEED if succeed. See vx_status_e to get more info about not succeed codes.
         */
        static vx_status Process(const string &$path, const std::map<string, vx_reference> &$map, const json &$data);

        /**
         * Writes data from JSON to vxData object.
         * @param $input Specify vxData reference.
         * @param $data Specify vxData in JSON.
         * @return Returns vx_status, VX_SUCCEED if succeed. See vx_status_e to get more info about not succeed codes.
         */
        static vx_status Process(vx_reference $input, const json &$data);

     private:
        static vx_status writeArray(vx_array $input, const json &$data);

        static vx_status writeScalar(vx_scalar $input, const json &$data);

        static vx_status writeConvolution(vx_convolution $input, const json &$data);

        static vx_status writeDistribution(vx_distribution $input, const json &$data);

        static vx_status writeLut(vx_lut $input, const json &$data);

        static vx_status writeMatrix(vx_matrix $input, const json &$data);

        static vx_status writePyramid(vx_pyramid $input, const json &$data);

        static vx_status writeRemap(vx_remap $input, const json &$data);

        static vx_status writeThreshold(vx_threshold $input, const json &$data);

        template<typename T>
        static T readArrayAttribute(vx_array $input, vx_enum $attr) {
            T attrHolder;
            vxQueryArray($input, $attr, &attrHolder, sizeof(T));
            return attrHolder;
        }

        template<typename T>
        static T readScalarAttribute(vx_scalar $input, vx_enum $attr) {
            T attrHolder;
            vxQueryScalar($input, $attr, &attrHolder, sizeof(T));
            return attrHolder;
        }

        template<typename T>
        static void writeArrayElements(void **$array, const json &$data) {
            if (*$array == nullptr) {
                *$array = new T[$data.size()];
            }
            T *array = static_cast<T *>(*$array);
            for (size_t i = 0; i < $data.size(); i++) {
                array[i] = (T)$data[i];
            }
        }

        static void writeRectangleArrayElements(void **$array, const json &$data) {
            if (*$array == nullptr) {
                *$array = new vx_rectangle_t[$data.size()];
            }
            auto *array = static_cast<vx_rectangle_t *>(*$array);
            for (size_t i = 0; i < $data.size(); i++) {
                auto obj = $data[i];
                array[i] = {
                        .start_x = obj["start_x"],
                        .start_y = obj["start_y"],
                        .end_x = obj["end_x"],
                        .end_y = obj["end_y"],
                };
            }
        }

        static void writeKeypointArrayElements(void **$array, const json &$data) {
            if (*$array == nullptr) {
                *$array = new vx_keypoint_t[$data.size()];
            }
            auto *array = static_cast<vx_keypoint_t *>(*$array);
            for (size_t i = 0; i < $data.size(); i++) {
                auto obj = $data[i];
                array[i] = {
                        .x = obj["x"],
                        .y = obj["y"],
                        .strength = obj["strength"],
                        .scale = obj["scale"],
                        .orientation = obj["orientation"],
                        .tracking_status = obj["tracking_status"],
                        .error = obj["error"]
                };
            }
        }

        template<typename T>
        static vx_status writeScalarValue(vx_scalar $scalar, const json &$data) {
            T value = $data["value"];
            return vxCopyScalar($scalar, &value, vx_accessor_e::VX_WRITE_ONLY, vx_memory_type_e::VX_MEMORY_TYPE_HOST);
        }

        template<typename T>
        static vx_status writeLutArray(vx_lut $lut, const json &$data, vx_size $count) {
            auto value = $data["value"].get<std::vector<T>>();
            if ($count == value.size()) {
                return vxCopyLUT($lut, value.data(), VX_WRITE_ONLY, VX_MEMORY_TYPE_HOST);
            } else {
                return VX_FAILURE;
            }
        }

        template<typename T>
        static vx_status writeMatrixData(vx_matrix $matrix, const json &$data, vx_size $count) {
            auto value = $data["value"].get<std::vector<T>>();
            if ($count == value.size()) {
                return vxCopyMatrix($matrix, value.data(), VX_WRITE_ONLY, VX_MEMORY_TYPE_HOST);
            } else {
                return VX_FAILURE;
            }
        }

        template<typename T>
        static T readThresholdAttribute(vx_threshold $input, vx_threshold_attribute_e $attr) {
            T attrHolder;

            vxQueryThreshold($input, $attr, &attrHolder, sizeof(T));

            return attrHolder;
        }
    };
}

#endif  // IO_VISIONSDK_STUDIO_LIBS_UTILS_VXDATAWRITER_HPP_
