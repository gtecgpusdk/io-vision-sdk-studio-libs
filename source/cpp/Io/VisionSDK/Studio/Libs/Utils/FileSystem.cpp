/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef DeleteFile
#undef DeleteFile
#endif

#ifdef CreateDirectory
#undef CreateDirectory
#endif

#include <filesystem>

#include "../sourceFilesMap.hpp"

namespace Io::VisionSDK::Studio::Libs::Utils {

    bool FileSystem::Exists(const string &$path) {
        return std::filesystem::exists($path);
    }

    string FileSystem::getTemp() {
        return std::filesystem::temp_directory_path().string();
    }

    void FileSystem::DeleteFile(const string &$path) {
        std::filesystem::remove_all($path);
    }

    void FileSystem::CreateDirectory(const string &$path) {
        if (!FileSystem::Exists($path)) {
            std::filesystem::path path($path);
            if (!std::filesystem::is_directory(path)) {
                path = path.parent_path();
            }
            std::filesystem::create_directories(path);
        }
    }
}
