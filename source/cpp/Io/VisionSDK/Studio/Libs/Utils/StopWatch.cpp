/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include <chrono>

#include "../sourceFilesMap.hpp"

namespace Io::VisionSDK::Studio::Libs::Utils {

    void StopWatch::Start() {
        this->elapsed = 0;
        this->startTime = this->getCurrentTs();
    }

    void StopWatch::Stop() {
        this->elapsed = this->getCurrentTs() - this->startTime;
    }

    long StopWatch::getElapsed() const {
        return this->elapsed;
    }

    long StopWatch::getCurrentTs() {
        return static_cast<long>(std::chrono::duration_cast<std::chrono::milliseconds>(
                std::chrono::system_clock::now().time_since_epoch()).count());
    }
}
