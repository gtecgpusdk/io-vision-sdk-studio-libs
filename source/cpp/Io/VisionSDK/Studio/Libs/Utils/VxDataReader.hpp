/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_VISIONSDK_STUDIO_LIBS_UTILS_VXDATAREADER_HPP_
#define IO_VISIONSDK_STUDIO_LIBS_UTILS_VXDATAREADER_HPP_

namespace Io::VisionSDK::Studio::Libs::Utils {

    class VxDataReader {
     public:
        /**
         * Serialize vxData defined by path to JSON.
         * @param $path Specify vxData path to read.
         * @param $map Map which holds references to objects that can be read.
         * @return Returns JSON describing the desired object.
         */
        static json Process(const string &$path, const std::map<string, vx_reference> &$map);

        /**
         * Serialize vxData to JSON.
         * @param $input Specify reference to VxData object.
         * @return Returns JSON describing the desired object.
         */
        static json Process(vx_reference $input);

     private:
        static json readArray(vx_array $input);

        static json readScalar(vx_scalar $input);

        static json readConvolution(vx_convolution $input);

        static json readDistribution(vx_distribution $input);

        static json readLut(vx_lut $input);

        static json readMatrix(vx_matrix $input);

        static json readPyramid(vx_pyramid $input);

        static json readRemap(vx_remap $input);

        static json readThreshold(vx_threshold $input);

        static json readArrayElements(vx_array $input, vx_size $size, vx_type_e $type);

        static json readKeyPointsArray(vx_keypoint_t *$input, vx_size $size);

        static json readRectangleArray(vx_rectangle_t *$input, vx_size $size);

        template<typename T>
        static json readLutElements(vx_lut $input, vx_size $size) {
            json lutElems;

#if VX_VERSION == VX_VERSION_1_0
            T *elems = new T[$size];
            vx_status stat = vxAccessLUT($input, reinterpret_cast<void **>(&elems), VX_READ_ONLY);
            if (stat == vx_status_e::VX_SUCCESS) {
                for (vx_size i = 0; i < $size * sizeof(T); i += sizeof(T)) {
                    lutElems.push_back(readLutElem<T>(elems[i], $typeName));
                }
            }
            vxCommitLUT($input, (const void *)elems);
            delete[] elems;
#elif VX_VERSION == VX_VERSION_1_1 || VX_VERSION == VX_VERSION_1_2
            T *elems = new T[$size];
            vx_status stat = vxCopyLUT($input, reinterpret_cast<void *>(elems), vx_accessor_e::VX_READ_ONLY,
                                       vx_memory_type_e::VX_MEMORY_TYPE_HOST);
            if (stat == vx_status_e::VX_SUCCESS) {
                for (vx_size i = 0; i < $size; i++) {
                    lutElems.push_back(elems[i]);
                }
            }
            delete[] elems;
#else
#error "readLutElems not available for used OpenVX version"
#endif
            return lutElems;
        }

        template<typename T>
        static json readMatrixElements(vx_matrix $input, vx_size $rows, vx_size $cols) {
            json elems = json::array();
            T *data = new T[$rows * $cols];

#if VX_VERSION == VX_VERSION_1_0
            vxReadMatrix($input, reinterpret_cast<void *>(data));
#elif VX_VERSION == VX_VERSION_1_1 || VX_VERSION == VX_VERSION_1_2
            vxCopyMatrix($input, reinterpret_cast<void *>(data), vx_accessor_e::VX_READ_ONLY,
                         vx_memory_type_e::VX_MEMORY_TYPE_HOST);
#else
#error "readMatrixElems not available for used OpenVX version"
#endif

            for (vx_size i = 0; i < $rows * $cols; i++) {
                elems.push_back(data[i]);
            }

            delete[] data;
            return elems;
        }

        template<typename T>
        static json readConvolutionElements(vx_convolution $input, vx_size $rows, vx_size $cols) {
            json elems = json::array();
            T *data = new T[$rows * $cols];
            vxCopyConvolutionCoefficients($input, reinterpret_cast<void *>(data), vx_accessor_e::VX_READ_ONLY,
                                          vx_memory_type_e::VX_MEMORY_TYPE_HOST);
            for (vx_size i = 0; i < $rows * $cols; i++) {
                elems.push_back(data[i]);
            }
            delete[] data;
            return elems;
        }

        template<typename T>
        static json readDistributionElements(vx_distribution $input, vx_size $bins) {
            json elems = json::array();
            T *data = new T[$bins];
            vxCopyDistribution($input, reinterpret_cast<void *>(data), vx_accessor_e::VX_READ_ONLY,
                               vx_memory_type_e::VX_MEMORY_TYPE_HOST);
            for (vx_size i = 0; i < $bins; i++) {
                elems.push_back(data[i]);
            }
            delete[] data;
            return elems;
        }

        template<typename T>
        static T readDistributionAttribute(vx_distribution $input, vx_distribution_attribute_e $attr) {
            T attrHolder;
            vxQueryDistribution($input, $attr, &attrHolder, sizeof(T));
            return attrHolder;
        }

        template<typename T>
        static T readConvolutionAttribute(vx_convolution $input, vx_convolution_attribute_e $attr) {
            T attrHolder;
            vxQueryConvolution($input, $attr, &attrHolder, sizeof(T));
            return attrHolder;
        }

        template<typename T>
        static T readPyramidAttribute(vx_pyramid $input, vx_pyramid_attribute_e $attr) {
            T attrHolder;
            vxQueryPyramid($input, $attr, &attrHolder, sizeof(T));
            return attrHolder;
        }

        template<typename T>
        static T readRemapAttribute(vx_remap $input, vx_remap_attribute_e $attr) {
            T attrHolder;
            vxQueryRemap($input, $attr, &attrHolder, sizeof(T));
            return attrHolder;
        }

        template<typename T>
        static T readThresholdAttribute(vx_threshold $input, vx_threshold_attribute_e $attr) {
            T attrHolder;

            vxQueryThreshold($input, $attr, &attrHolder, sizeof(T));

            return attrHolder;
        }

        template<typename T>
        static T readScalarValue(vx_scalar $input) {
            T valueHolder;

#if VX_VERSION == VX_VERSION_1_0
            vxReadScalarValue($input, &valueHolder);
#elif VX_VERSION == VX_VERSION_1_1 || VX_VERSION == VX_VERSION_1_2
            vxCopyScalar($input, &valueHolder, vx_accessor_e::VX_READ_ONLY, vx_memory_type_e::VX_MEMORY_TYPE_HOST);
#else
#error "readScalarValue not available for used OpenVX version"
#endif

            return valueHolder;
        }

        template<typename T>
        static json readIntegralTypeArray(void *$array, vx_size $stride, vx_size $size) {
            json retVal = json::array();
            T *array = static_cast<T *>($array);
            for (vx_size i = 0; i < $size; i++) {
                retVal.push_back(array[i]);
            }
            return retVal;
        }
    };
}

#endif  // IO_VISIONSDK_STUDIO_LIBS_UTILS_VXDATAREADER_HPP_
