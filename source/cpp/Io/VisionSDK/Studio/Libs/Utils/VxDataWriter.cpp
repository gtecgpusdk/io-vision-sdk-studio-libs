/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
#include "../sourceFilesMap.hpp"

namespace Io::VisionSDK::Studio::Libs::Utils {

    vx_status VxDataWriter::Process(const string &$path, const std::map<string, vx_reference> &$map, const json &$data) {
        vx_status status = vx_status_e::VX_FAILURE;
        auto it = $map.find($path);
        if (it != $map.end()) {
            status = VxDataWriter::Process(it->second, $data);
        }
        return status;
    }

    vx_status VxDataWriter::Process(vx_reference $input, const json &$data) {
        vx_status status = vx_status_e::VX_FAILURE;

        vx_enum requestType;
        vx_reference_attribute_e attributeType;
#if VX_VERSION == VX_VERSION_1_0 || VX_VERSION == VX_VERSION_1_1
        attributeType = VX_REF_ATTRIBUTE_TYPE;
#elif VX_VERSION == VX_VERSION_1_2
        attributeType = VX_REFERENCE_TYPE;
#else
#error "Check proper vx_reference_attribute_e type for used OpenVX version"
#endif
        status = vxQueryReference($input, attributeType, &requestType, sizeof(requestType));
        if (status == vx_status_e::VX_SUCCESS) {
            switch (requestType) {
                case VX_TYPE_ARRAY: {
                    status = writeArray((vx_array)$input, $data);
                    break;
                }
                case VX_TYPE_SCALAR: {
                    status = writeScalar((vx_scalar)$input, $data);
                    break;
                }
                case VX_TYPE_CONVOLUTION: {
                    status = writeConvolution((vx_convolution)$input, $data);
                    break;
                }
                case VX_TYPE_DISTRIBUTION: {
                    status = writeDistribution((vx_distribution)$input, $data);
                    break;
                }
                case VX_TYPE_LUT: {
                    status = writeLut((vx_lut)$input, $data);
                    break;
                }
                case VX_TYPE_MATRIX: {
                    status = writeMatrix((vx_matrix)$input, $data);
                    break;
                }
                case VX_TYPE_PYRAMID: {
                    status = writePyramid((vx_pyramid)$input, $data);
                    break;
                }
                case VX_TYPE_REMAP: {
                    status = writeRemap((vx_remap)$input, $data);
                    break;
                }
                case VX_TYPE_THRESHOLD: {
                    status = writeThreshold((vx_threshold)$input, $data);
                    break;
                }
                default: {
                    status = vx_status_e::VX_FAILURE;
                    break;
                }
            }
        }

        return status;
    }

    vx_status VxDataWriter::writeArray(vx_array $input, const json &$data) {
        vx_status status = vx_status_e::VX_FAILURE;

        std::vector<std::pair<vx_array_attribute_e, std::string>> attributes;

#if VX_VERSION == VX_VERSION_1_0
        attributes = {
                {VX_ARRAY_ATTRIBUTE_NUMITEMS,  "VX_ARRAY_ATTRIBUTE_NUMITEMS"},
                {VX_ARRAY_ATTRIBUTE_ITEMTYPE,  "VX_ARRAY_ATTRIBUTE_ITEMTYPE"}
        };
#elif VX_VERSION == VX_VERSION_1_1 || VX_VERSION == VX_VERSION_1_2
        attributes = {
                {VX_ARRAY_NUMITEMS, "VX_ARRAY_NUMITEMS"},
                {VX_ARRAY_ITEMTYPE, "VX_ARRAY_ITEMTYPE"},
                {VX_ARRAY_CAPACITY, "VX_ARRAY_CAPACITY"}
        };
#else
#error "readArray not available for used OpenVX version"
#endif

        auto size = readArrayAttribute<vx_size>($input, attributes[0].first);
        auto type = (vx_type_e)readArrayAttribute<vx_enum>($input, attributes[1].first);
        auto capacity = readArrayAttribute<vx_size>($input, attributes[2].first);

        if ($data["type"] == VxTranslator::Translate(vx_type_e::VX_TYPE_ARRAY)) {
            if (VxTranslator::Translate($data["valueType"].get<string>()) == type && capacity == $data["value"].size()) {
                vx_size stride = 0;
                void *arrPtr = nullptr;

                bool init = size == 0;
                unsigned mapId = 0;
                if (!init) {
#if VX_VERSION == VX_VERSION_1_0
                    status = vxAccessArrayRange($input, 0, $size, &stride, &arrPtr, VX_READ_ONLY);
#elif VX_VERSION == VX_VERSION_1_1 || VX_VERSION == VX_VERSION_1_2
                    status = vxMapArrayRange($input, 0, size, reinterpret_cast<vx_map_id *>(&mapId), &stride, &arrPtr,
                                             VX_READ_AND_WRITE, VX_MEMORY_TYPE_HOST, VX_NOGAP_X);
#else
#error "readArrayElems not available for used OpenVX version"
#endif
                } else {
                    status = vx_status_e::VX_SUCCESS;
                    size = capacity;
                }

                if (status == vx_status_e::VX_SUCCESS) {
                    switch (type) {
                        case VX_TYPE_CHAR:
                            writeArrayElements<vx_uint8>(&arrPtr, $data["value"]);
                            stride = sizeof(vx_uint8);
                            break;
                        case VX_TYPE_INT8:
                            writeArrayElements<vx_int8>(&arrPtr, $data["value"]);
                            stride = sizeof(vx_int8);
                            break;
                        case VX_TYPE_UINT8:
                            writeArrayElements<vx_uint8>(&arrPtr, $data["value"]);
                            stride = sizeof(vx_uint8);
                            break;
                        case VX_TYPE_INT16:
                            writeArrayElements<vx_int16>(&arrPtr, $data["value"]);
                            stride = sizeof(vx_int16);
                            break;
                        case VX_TYPE_UINT16:
                            writeArrayElements<vx_uint16>(&arrPtr, $data["value"]);
                            stride = sizeof(vx_uint16);
                            break;
                        case VX_TYPE_INT32:
                            writeArrayElements<vx_int32>(&arrPtr, $data["value"]);
                            stride = sizeof(vx_int32);
                            break;
                        case VX_TYPE_UINT32:
                            writeArrayElements<vx_uint32>(&arrPtr, $data["value"]);
                            stride = sizeof(vx_uint32);
                            break;
                        case VX_TYPE_INT64:
                            writeArrayElements<vx_int64>(&arrPtr, $data["value"]);
                            stride = sizeof(vx_int64);
                            break;
                        case VX_TYPE_UINT64:
                            writeArrayElements<vx_uint64>(&arrPtr, $data["value"]);
                            stride = sizeof(vx_uint64);
                            break;
                        case VX_TYPE_FLOAT32:
                            writeArrayElements<vx_float32>(&arrPtr, $data["value"]);
                            stride = sizeof(vx_float32);
                            break;
                        case VX_TYPE_FLOAT64:
                            writeArrayElements<vx_float64>(&arrPtr, $data["value"]);
                            stride = sizeof(vx_float64);
                            break;
                        case VX_TYPE_SIZE:
                            writeArrayElements<vx_size>(&arrPtr, $data["value"]);
                            stride = sizeof(vx_size);
                            break;
                        case VX_TYPE_BOOL:
                            writeArrayElements<vx_bool>(&arrPtr, $data["value"]);
                            stride = sizeof(vx_bool);
                            break;
                        case VX_TYPE_RECTANGLE:
                            writeRectangleArrayElements(&arrPtr, $data["value"]);
                            stride = sizeof(vx_rectangle_t);
                            break;
                        case VX_TYPE_KEYPOINT:
                            writeKeypointArrayElements(&arrPtr, $data["value"]);
                            stride = sizeof(vx_keypoint_t);
                            break;
                        default:
                            break;
                    }
                    if (!init) {
#if VX_VERSION == VX_VERSION_1_0
                        status = vxCommitArrayRange($input, 0, $size, (const void *)arrPtr);
#elif VX_VERSION == VX_VERSION_1_1 || VX_VERSION == VX_VERSION_1_2
                        status = vxUnmapArrayRange($input, (vx_map_id)mapId);
#endif
                    } else {
                        status = vxAddArrayItems($input, size, arrPtr, stride);
                    }
                }
            }
        }

        return status;
    }

    vx_status VxDataWriter::writeScalar(vx_scalar $input, const json &$data) {
        vx_status status = vx_status_e::VX_FAILURE;

        vx_scalar_attribute_e scalarAttribute;
#if VX_VERSION == VX_VERSION_1_0
        scalarAttribute = VX_SCALAR_ATTRIBUTE_TYPE;
#elif VX_VERSION == VX_VERSION_1_1 || VX_VERSION == VX_VERSION_1_2
        scalarAttribute = VX_SCALAR_TYPE;
#else
#error "readScalar not available for used OpenVX version"
#endif

        auto type = (vx_type_e)readScalarAttribute<vx_enum>($input, scalarAttribute);

        if ($data["type"] == VxTranslator::Translate(vx_type_e::VX_TYPE_SCALAR)) {
            if (VxTranslator::Translate($data["valueType"].get<string>()) == type) {
                switch (type) {
                    case VX_TYPE_CHAR:
                        status = writeScalarValue<vx_uint8>($input, $data);
                        break;
                    case VX_TYPE_INT8:
                        status = writeScalarValue<vx_int8>($input, $data);
                        break;
                    case VX_TYPE_UINT8:
                        status = writeScalarValue<vx_uint8>($input, $data);
                        break;
                    case VX_TYPE_INT16:
                        status = writeScalarValue<vx_int16>($input, $data);
                        break;
                    case VX_TYPE_UINT16:
                        status = writeScalarValue<vx_uint16>($input, $data);
                        break;
                    case VX_TYPE_INT32:
                        status = writeScalarValue<vx_int32>($input, $data);
                        break;
                    case VX_TYPE_UINT32:
                        status = writeScalarValue<vx_uint32>($input, $data);
                        break;
                    case VX_TYPE_INT64:
                        status = writeScalarValue<vx_int64>($input, $data);
                        break;
                    case VX_TYPE_UINT64:
                        status = writeScalarValue<vx_uint64>($input, $data);
                        break;
                    case VX_TYPE_FLOAT32:
                        status = writeScalarValue<vx_float32>($input, $data);
                        break;
                    case VX_TYPE_FLOAT64:
                        status = writeScalarValue<vx_float64>($input, $data);
                        break;
                    case VX_TYPE_SIZE:
                        status = writeScalarValue<vx_size>($input, $data);
                        break;
                    case VX_TYPE_BOOL:
                        status = writeScalarValue<vx_bool>($input, $data);
                        break;
                    default:
                        break;
                }
            }
        }

        return status;
    }

    vx_status VxDataWriter::writeConvolution(vx_convolution $input, const json &$data) {
        vx_status status = VX_FAILURE;

        if ($data["type"] == VxTranslator::Translate(vx_type_e::VX_TYPE_CONVOLUTION)) {
            json attribute = $data["attributes"][0];

            if (attribute["type"].get<string>() == "VX_CONVOLUTION_SCALE") {
                if (attribute["valueType"] == VxTranslator::Translate(vx_type_e::VX_TYPE_UINT32)) {
                    auto attributeValue = attribute["value"].get<vx_uint32>();
                    status = vxSetConvolutionAttribute($input, VX_CONVOLUTION_SCALE, &attributeValue, sizeof(attributeValue));
                }
            }

            if (status == VX_SUCCESS) {
                if ($data["valueType"] == VxTranslator::Translate(vx_type_e::VX_TYPE_INT16)) {
                    auto value = $data["value"].get<std::vector<vx_int16>>();

                    vx_size rows;
                    vx_size cols;

                    if (vxQueryConvolution($input, VX_CONVOLUTION_ROWS, &rows, sizeof(rows)) == VX_SUCCESS &&
                        vxQueryConvolution($input, VX_CONVOLUTION_COLUMNS, &cols, sizeof(cols)) == VX_SUCCESS &&
                        rows * cols == value.size()) {
                        status = vxCopyConvolutionCoefficients($input, value.data(), VX_WRITE_ONLY, VX_MEMORY_TYPE_HOST);
                    }
                }
            }
        }

        return status;
    }

    vx_status VxDataWriter::writeDistribution(vx_distribution $input, const json &$data) {
        vx_status status = VX_FAILURE;

        if ($data["type"] == VxTranslator::Translate(vx_type_e::VX_TYPE_DISTRIBUTION)) {
            if ($data["valueType"] == VxTranslator::Translate(vx_type_e::VX_TYPE_UINT32)) {
                vx_int32 offset;
                vx_uint32 range;

                auto value = $data["value"].get<std::vector<vx_uint32>>();

                if (vxQueryDistribution($input, VX_DISTRIBUTION_OFFSET, &offset, sizeof(offset)) == VX_SUCCESS &&
                    vxQueryDistribution($input, VX_DISTRIBUTION_RANGE, &range, sizeof(range)) == VX_SUCCESS &&
                    offset + range == value.size()) {
                    status = vxCopyDistribution($input, value.data(), VX_WRITE_ONLY, VX_MEMORY_TYPE_HOST);
                }
            }
        }

        return status;
    }

    vx_status VxDataWriter::writeLut(vx_lut $input, const json &$data) {
        vx_size count;
        vx_status status = vxQueryLUT($input, VX_LUT_COUNT, &count, sizeof(count));

        if (status == VX_SUCCESS && $data["type"] == VxTranslator::Translate(vx_type_e::VX_TYPE_LUT)) {
            auto valueType = VxTranslator::Translate($data["valueType"].get<string>());
            switch (valueType) {
                case VX_TYPE_UINT8: {
                    status = VxDataWriter::writeLutArray<vx_uint8>($input, $data, count);
                    break;
                }
                case VX_TYPE_INT16: {
                    status = VxDataWriter::writeLutArray<vx_int16>($input, $data, count);
                    break;
                }
                default: {
                    status = VX_FAILURE;
                    break;
                }
            }
        }

        return status;
    }

    vx_status VxDataWriter::writeMatrix(vx_matrix $input, const json &$data) {
        vx_status status = VX_FAILURE;

        if ($data["type"] == VxTranslator::Translate(vx_type_e::VX_TYPE_MATRIX)) {
            auto valueType = VxTranslator::Translate($data["valueType"].get<string>());

            vx_size rows;
            if (vxQueryMatrix($input, VX_MATRIX_ROWS, &rows, sizeof(rows)) != VX_SUCCESS) {
                return VX_FAILURE;
            }

            vx_size cols;
            if (vxQueryMatrix($input, VX_MATRIX_COLUMNS, &cols, sizeof(cols)) != VX_SUCCESS) {
                return VX_FAILURE;
            }

            vx_enum matrixType;
            if (vxQueryMatrix($input, VX_MATRIX_TYPE, &matrixType, sizeof(matrixType)) != VX_SUCCESS) {
                return VX_FAILURE;
            }

            if (matrixType == valueType) {
                vx_size count = rows * cols;
                switch (valueType) {
                    case VX_TYPE_UINT8: {
                        status = VxDataWriter::writeMatrixData<vx_uint8>($input, $data, count);
                        break;
                    }
                    case VX_TYPE_INT32: {
                        status = VxDataWriter::writeMatrixData<vx_int32>($input, $data, count);
                        break;
                    }
                    case VX_TYPE_FLOAT32: {
                        status = VxDataWriter::writeMatrixData<vx_float32>($input, $data, count);
                        break;
                    }
                    default: {
                        break;
                    }
                }
            }
        }

        return status;
    }

    vx_status VxDataWriter::writePyramid(vx_pyramid $input, const json &$data) {
        return 0;
    }

    vx_status VxDataWriter::writeRemap(vx_remap $input, const json &$data) {
        vx_status status = VX_FAILURE;

        if ($data["type"] == VxTranslator::Translate(vx_type_e::VX_TYPE_REMAP)) {
            vx_uint32 j, i, dw, dh;

            if (vxQueryRemap($input, VX_REMAP_DESTINATION_WIDTH, &dw, sizeof(dw)) != VX_SUCCESS) {
                return VX_FAILURE;
            }

            if (vxQueryRemap($input, VX_REMAP_DESTINATION_HEIGHT, &dh, sizeof(dh)) != VX_SUCCESS) {
                return VX_FAILURE;
            }

#if VX_VERSION == VX_VERSION_1_2
            auto *coords = new vx_coordinates2df_t[dw * dh];
#endif
            auto processPoint = [&](vx_float32 $srcX, vx_float32 $srcY, vx_uint32 $destX, vx_uint32 $destY) -> vx_status {
#if VX_VERSION == VX_VERSION_1_1
                return vxSetRemapPoint($input, $destX, $destY, $srcX, $srcY);
#elif VX_VERSION == VX_VERSION_1_2
                coords[$destX + ($destY * dw)].x = $srcX;
                coords[$destX + ($destY * dw)].y = $srcY;
                return vx_status_e::VX_SUCCESS;
#else
#error "readRemap is not implemented for demand OVX version"
#endif
            };

            for (const auto &elem : $data["value"]) {
                auto srcX = elem["src_x"].get<vx_float32>();
                auto srcY = elem["src_y"].get<vx_float32>();
                i = elem["dest_x"];
                j = elem["dest_y"];
                status = processPoint(srcX, srcY, i, j);
                if (status != vx_status_e::VX_SUCCESS) {
                    return status;
                }
            }
#if VX_VERSION == VX_VERSION_1_2
            vx_rectangle_t rect = {.start_x = 0, .start_y = 0, .end_x = dw, .end_y = dh};

            status = vxCopyRemapPatch($input, &rect, sizeof(vx_coordinates2d_t) * dw, coords, vx_type_e::VX_TYPE_COORDINATES2DF,
                                      vx_accessor_e::VX_WRITE_ONLY, vx_memory_type_e::VX_MEMORY_TYPE_HOST);

            delete[] coords;
#endif
        }

        return status;
    }

    vx_status VxDataWriter::writeThreshold(vx_threshold $input, const json &$data) {
        vx_status status = VX_FAILURE;

        if ($data["type"] == VxTranslator::Translate(vx_type_e::VX_TYPE_THRESHOLD)) {
#if VX_VERSION == VX_VERSION_1_1
            vx_int32 value;
            for (const json &attr : $data["attributes"]) {
                if (attr.find("value") != attr.end() && attr.find("type") != attr.end()) {
                    value = attr["value"].get<vx_int32>();
                } else {
                    status = vx_status_e::VX_FAILURE;
                    break;
                }
                if (attr["type"] == "VX_THRESHOLD_THRESHOLD_VALUE") {
                    status = vxSetThresholdAttribute($input, vx_threshold_attribute_e::VX_THRESHOLD_THRESHOLD_VALUE, &value, sizeof(value));
                } else if (attr["type"] == "VX_THRESHOLD_THRESHOLD_LOWER") {
                    status = vxSetThresholdAttribute($input, vx_threshold_attribute_e::VX_THRESHOLD_THRESHOLD_LOWER, &value, sizeof(value));
                } else if (attr["type"] == "VX_THRESHOLD_THRESHOLD_UPPER") {
                    status = vxSetThresholdAttribute($input, vx_threshold_attribute_e::VX_THRESHOLD_THRESHOLD_UPPER, &value, sizeof(value));
                } else if (attr["type"] == "VX_THRESHOLD_TRUE_VALUE") {
                    status = vxSetThresholdAttribute($input, vx_threshold_attribute_e::VX_THRESHOLD_TRUE_VALUE, &value, sizeof(value));
                } else if (attr["type"] == "VX_THRESHOLD_FALSE_VALUE") {
                    status = vxSetThresholdAttribute($input, vx_threshold_attribute_e::VX_THRESHOLD_FALSE_VALUE, &value, sizeof(value));
                }
                if (status != vx_status_e::VX_SUCCESS) {
                    break;
                }
            }
#elif VX_VERSION == VX_VERSION_1_2
            vx_pixel_value_t trueVal;
            vx_pixel_value_t falseVal;
            vx_pixel_value_t lowerVal;
            vx_pixel_value_t upperVal;
            vx_pixel_value_t binaryVal;

            auto setPixelValue = [](const vx_df_image_e &$inputFormat, const vx_int32 &$value, vx_pixel_value_t &$pixel) {
                if ($inputFormat == vx_df_image_e::VX_DF_IMAGE_S16) {
                    $pixel.S16 = static_cast<vx_int16>($value);
                } else {
                    $pixel.U8 = static_cast<vx_uint8>($value);
                }
            };

            auto inputFormat = readThresholdAttribute<vx_df_image_e>($input, VX_THRESHOLD_INPUT_FORMAT);

            for (const auto &attr : $data["attributes"]) {
                if (attr["type"] == "VX_THRESHOLD_THRESHOLD_VALUE") {
                    setPixelValue(inputFormat, attr["value"].get<vx_int32>(), binaryVal);
                }
                if (attr["type"] == "VX_THRESHOLD_THRESHOLD_LOWER") {
                    setPixelValue(inputFormat, attr["value"].get<vx_int32>(), lowerVal);
                }
                if (attr["type"] == "VX_THRESHOLD_THRESHOLD_UPPER") {
                    setPixelValue(inputFormat, attr["value"].get<vx_int32>(), upperVal);
                }
                if (attr["type"] == "VX_THRESHOLD_TRUE_VALUE") {
                    setPixelValue(inputFormat, attr["value"].get<vx_int32>(), trueVal);
                }
                if (attr["type"] == "VX_THRESHOLD_FALSE_VALUE") {
                    setPixelValue(inputFormat, attr["value"].get<vx_int32>(), falseVal);
                }
            }

            auto thrType = readThresholdAttribute<vx_threshold_type_e>($input, VX_THRESHOLD_TYPE);
            if (thrType == vx_threshold_type_e::VX_THRESHOLD_TYPE_BINARY) {
                status = vxCopyThresholdValue($input, &binaryVal, vx_accessor_e::VX_WRITE_ONLY, vx_memory_type_e::VX_MEMORY_TYPE_HOST);
            } else if (thrType == vx_threshold_type_e::VX_THRESHOLD_TYPE_RANGE) {
                status = vxCopyThresholdRange($input, &lowerVal, &upperVal, vx_accessor_e::VX_WRITE_ONLY,
                                              vx_memory_type_e::VX_MEMORY_TYPE_HOST);
            }

            status = (status == VX_SUCCESS ? vxCopyThresholdOutput($input, &trueVal, &falseVal, vx_accessor_e::VX_WRITE_ONLY,
                                                                   vx_memory_type_e::VX_MEMORY_TYPE_HOST) : VX_FAILURE);
#else
#error "writeThreshold is not implemented for demand OVX version"
#endif
        }

        return status;
    }
}
