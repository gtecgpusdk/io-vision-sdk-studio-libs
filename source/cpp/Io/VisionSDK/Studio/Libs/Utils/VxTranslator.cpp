/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Io::VisionSDK::Studio::Libs::Utils {
    // @formatter:off
    // NOLINT
    std::map<vx_status_e, std::pair<string, string>> vxStatusTranslateMap = {
            {vx_status_e::VX_ERROR_REFERENCE_NONZERO,  std::pair<string, string>{"VX_ERROR_REFERENCE_NONZERO", "Indicates that an operation did not complete due to a reference count being non-zero."}},  // NOLINT
            {vx_status_e::VX_ERROR_MULTIPLE_WRITERS,   std::pair<string, string>{"VX_ERROR_MULTIPLE_WRITERS", "Indicates that the graph has more than one node outputting to the same data object. This is an invalid graph structure."}},  // NOLINT
            {vx_status_e::VX_ERROR_GRAPH_ABANDONED,    std::pair<string, string>{"VX_ERROR_GRAPH_ABANDONED", "Indicates that the graph is stopped due to an error or a callback that abandoned execution."}},  // NOLINT
            {vx_status_e::VX_ERROR_GRAPH_SCHEDULED,    std::pair<string, string>{"VX_ERROR_GRAPH_SCHEDULED", "Indicates that the supplied graph already has been scheduled and may be currently executing."}},  // NOLINT
            {vx_status_e::VX_ERROR_INVALID_SCOPE,      std::pair<string, string>{"VX_ERROR_INVALID_SCOPE", "Indicates that the supplied parameter is from another scope and cannot be used in the current scope."}},  // NOLINT
            {vx_status_e::VX_ERROR_INVALID_NODE,       std::pair<string, string>{"VX_ERROR_INVALID_NODE", "Indicates that the supplied node could not be created."}},  // NOLINT
            {vx_status_e::VX_ERROR_INVALID_GRAPH,      std::pair<string, string>{"VX_ERROR_INVALID_GRAPH", "Indicates that the supplied graph has invalid connections (cycles)."}},  // NOLINT
            {vx_status_e::VX_ERROR_INVALID_TYPE,       std::pair<string, string>{"VX_ERROR_INVALID_TYPE", "Indicates that the supplied type parameter is incorrect."}},  // NOLINT
            {vx_status_e::VX_ERROR_INVALID_VALUE,      std::pair<string, string>{"VX_ERROR_INVALID_VALUE", "Indicates that the supplied parameter has an incorrect value."}},  // NOLINT
            {vx_status_e::VX_ERROR_INVALID_DIMENSION,  std::pair<string, string>{"VX_ERROR_INVALID_DIMENSION", "Indicates that the supplied parameter is too big or too small in dimension."}},  // NOLINT
            {vx_status_e::VX_ERROR_INVALID_FORMAT,     std::pair<string, string>{"VX_ERROR_INVALID_FORMAT", "Indicates that the supplied parameter is in an invalid format."}},  // NOLINT
            {vx_status_e::VX_ERROR_INVALID_LINK,       std::pair<string, string>{"VX_ERROR_INVALID_LINK", "Indicates that the link is not possible as specified. The parameters are incompatible."}},  // NOLINT
            {vx_status_e::VX_ERROR_INVALID_REFERENCE,  std::pair<string, string>{"VX_ERROR_INVALID_REFERENCE", "Indicates that the reference provided is not valid."}},  // NOLINT
            {vx_status_e::VX_ERROR_INVALID_MODULE,     std::pair<string, string>{"VX_ERROR_INVALID_MODULE", "This is returned from ref vxLoadKernels when the module does not contain the entry point."}},  // NOLINT
            {vx_status_e::VX_ERROR_INVALID_PARAMETERS, std::pair<string, string>{"VX_ERROR_INVALID_PARAMETERS", "Indicates that the supplied parameter information does not match the kernel contract."}},  // NOLINT
            {vx_status_e::VX_ERROR_OPTIMIZED_AWAY,     std::pair<string, string>{"VX_ERROR_OPTIMIZED_AWAY", "Indicates that the object refereed to has been optimized out of existence."}},  // NOLINT
            {vx_status_e::VX_ERROR_NO_MEMORY,          std::pair<string, string>{"VX_ERROR_NO_MEMORY", "Indicates that an internal or implicit allocation failed. Typically catastrophic. After detection, deconstruct the context. see vxVerifyGraph."}},  // NOLINT
            {vx_status_e::VX_ERROR_NO_RESOURCES,       std::pair<string, string>{"VX_ERROR_NO_RESOURCES", "Indicates that an internal or implicit resource can not be acquired (not memory). This is typically catastrophic. After detection, deconstruct the context. see vxVerifyGraph."}},  // NOLINT
            {vx_status_e::VX_ERROR_NOT_COMPATIBLE,     std::pair<string, string>{"VX_ERROR_NOT_COMPATIBLE", "Indicates that the attempt to link two parameters together failed due to type incompatibilty. "}},  // NOLINT
            {vx_status_e::VX_ERROR_NOT_ALLOCATED,      std::pair<string, string>{"VX_ERROR_NOT_ALLOCATED", "Indicates to the system that the parameter must be allocated by the system."}},  // NOLINT
            {vx_status_e::VX_ERROR_NOT_SUFFICIENT,     std::pair<string, string>{"VX_ERROR_NOT_SUFFICIENT", "Indicates that the given graph has failed verification due to an insufficient number of required parameters, which cannot be automatically created. Typically this indicates required atomic parameters. see vxVerifyGraph."}},  // NOLINT
            {vx_status_e::VX_ERROR_NOT_SUPPORTED,      std::pair<string, string>{"VX_ERROR_NOT_SUPPORTED", "Indicates that the requested set of parameters produce a configuration that cannot be supported. Refer to the supplied documentation on the configured kernels. see vx_kernel_e. This is also returned if a function to set an attribute is called on a Read-only attribute."}},  // NOLINT
            {vx_status_e::VX_ERROR_NOT_IMPLEMENTED,    std::pair<string, string>{"VX_ERROR_NOT_IMPLEMENTED", "Indicates that the requested kernel is missing. see vx_kernel_e vxGetKernelByName."}},  // NOLINT
            {vx_status_e::VX_FAILURE,                  std::pair<string, string>{"VX_FAILURE", "Indicates a generic error code, used when no other describes the error."}},  // NOLINT
            {vx_status_e::VX_SUCCESS,                  std::pair<string, string>{"VX_SUCCESS", "No error."}}  // NOLINT
    };
    std::map<vx_enum, string> vxTypeTranslateMap = {
            {vx_type_e::VX_TYPE_INVALID,        "VX_TYPE_INVALID"},
            {vx_type_e::VX_TYPE_CHAR,           "VX_TYPE_CHAR"},
            {vx_type_e::VX_TYPE_INT8,           "VX_TYPE_INT8"},
            {vx_type_e::VX_TYPE_UINT8,          "VX_TYPE_UINT8"},
            {vx_type_e::VX_TYPE_INT16,          "VX_TYPE_INT16"},
            {vx_type_e::VX_TYPE_UINT16,         "VX_TYPE_UINT16"},
            {vx_type_e::VX_TYPE_INT32,          "VX_TYPE_INT32"},
            {vx_type_e::VX_TYPE_UINT32,         "VX_TYPE_UINT32"},
            {vx_type_e::VX_TYPE_INT64,          "VX_TYPE_INT64"},
            {vx_type_e::VX_TYPE_UINT64,         "VX_TYPE_UINT64"},
            {vx_type_e::VX_TYPE_FLOAT32,        "VX_TYPE_FLOAT32"},
            {vx_type_e::VX_TYPE_FLOAT64,        "VX_TYPE_FLOAT64"},
            {vx_type_e::VX_TYPE_ENUM,           "VX_TYPE_ENUM"},
            {vx_type_e::VX_TYPE_SIZE,           "VX_TYPE_SIZE"},
            {vx_type_e::VX_TYPE_DF_IMAGE,       "VX_TYPE_DF_IMAGE"},
            {vx_type_e::VX_TYPE_BOOL,           "VX_TYPE_BOOL"},
            {vx_type_e::VX_TYPE_RECTANGLE,      "VX_TYPE_RECTANGLE"},
            {vx_type_e::VX_TYPE_KEYPOINT,       "VX_TYPE_KEYPOINT"},
            {vx_type_e::VX_TYPE_COORDINATES2D,  "VX_TYPE_COORDINATES2D"},
            {vx_type_e::VX_TYPE_COORDINATES3D,  "VX_TYPE_COORDINATES3D"},
            {vx_type_e::VX_TYPE_REFERENCE,      "VX_TYPE_REFERENCE"},
            {vx_type_e::VX_TYPE_CONTEXT,        "VX_TYPE_CONTEXT"},
            {vx_type_e::VX_TYPE_GRAPH,          "VX_TYPE_GRAPH"},
            {vx_type_e::VX_TYPE_NODE,           "VX_TYPE_NODE"},
            {vx_type_e::VX_TYPE_KERNEL,         "VX_TYPE_KERNEL"},
            {vx_type_e::VX_TYPE_PARAMETER,      "VX_TYPE_PARAMETER"},
            {vx_type_e::VX_TYPE_DELAY,          "VX_TYPE_DELAY"},
            {vx_type_e::VX_TYPE_LUT,            "VX_TYPE_LUT"},
            {vx_type_e::VX_TYPE_DISTRIBUTION,   "VX_TYPE_DISTRIBUTION"},
            {vx_type_e::VX_TYPE_PYRAMID,        "VX_TYPE_PYRAMID"},
            {vx_type_e::VX_TYPE_THRESHOLD,      "VX_TYPE_THRESHOLD"},
            {vx_type_e::VX_TYPE_MATRIX,         "VX_TYPE_MATRIX"},
            {vx_type_e::VX_TYPE_CONVOLUTION,    "VX_TYPE_CONVOLUTION"},
            {vx_type_e::VX_TYPE_SCALAR,         "VX_TYPE_SCALAR"},
            {vx_type_e::VX_TYPE_ARRAY,          "VX_TYPE_ARRAY"},
            {vx_type_e::VX_TYPE_IMAGE,          "VX_TYPE_IMAGE"},
            {vx_type_e::VX_TYPE_REMAP,          "VX_TYPE_REMAP"},
            {vx_type_e::VX_TYPE_ERROR,          "VX_TYPE_ERROR"},
            {vx_type_e::VX_TYPE_META_FORMAT,    "VX_TYPE_META_FORMAT"},
            {vx_type_e::VX_TYPE_OBJECT_ARRAY,   "VX_TYPE_OBJECT_ARRAY"}
    };
    // @formatter:on

    string VxTranslator::Translate(vx_status $status, bool $withDescription) {
        string retVal = std::to_string($status);
        auto it = vxStatusTranslateMap.find(vx_status_e($status));
        if (it != vxStatusTranslateMap.end()) {
            retVal = it->second.first;
            if ($withDescription) {
                retVal += " (" + it->second.second + ")";
            }
        }
        return retVal;
    }

    string VxTranslator::Translate(vx_type_e $type) {
        string retVal = std::to_string($type);
        auto it = vxTypeTranslateMap.find($type);
        if (it != vxTypeTranslateMap.end()) {
            retVal = it->second;
        }
        return retVal;
    }

    vx_type_e VxTranslator::Translate(const string &$type) {
        vx_type_e type = vx_type_e::VX_TYPE_INVALID;

        auto it = std::find_if(vxTypeTranslateMap.cbegin(), vxTypeTranslateMap.cend(),
                               [&](const std::pair<vx_enum, string> &$pair) -> bool {
                                   return $pair.second == $type;
                               });
        if (it != vxTypeTranslateMap.end()) {
            type = vx_type_e(it->first);
        }
        return type;
    }
}
