/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Io::VisionSDK::Studio::Libs::Utils {
    using Io::VisionSDK::Studio::Libs::Primitives::BaseVisualGraph;
    using Io::VisionSDK::Studio::Libs::Primitives::BaseObject;

    Manager::~Manager() {
        delete this->visualGraph;
    }

    int Manager::Create() {
        vx_status status = vx_status_e::VX_FAILURE;
        if (!this->IsCreated() && this->visualGraph != nullptr) {
            status = this->visualGraph->Create();
            this->setCreated(status == vx_status_e::VX_SUCCESS);
        }
        return status;
    }

    int Manager::Validate() {
        vx_status status = vx_status_e::VX_FAILURE;
        if (!this->IsValidated() && this->IsCreated()) {
            status = this->visualGraph->Validate();
            this->setValidated(status == vx_status_e::VX_SUCCESS);
        }
        return status;
    }

    int Manager::Process(const Manager::ProcessOptions &$options) {
        vx_status status = vx_status_e::VX_FAILURE;
        if (!this->IsRunning() && this->IsValidated()) {
            this->setRunning(true);
            StopWatch stopWatch{};
            this->visualGraph->setHeadless($options.headless);
            this->visualGraph->setLooped($options.looped);
            this->visualGraph->setNoFileIo($options.noFileIO);
            this->visualGraph->setNoCamera($options.noCamera);
            string name;
            stopWatch.Start();

            status = this->visualGraph->Process();

            stopWatch.Stop();

            this->setRunning(false);
        }
        return status;
    }

    void Manager::Release() {
        this->Stop();
        while (this->IsRunning()) {
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
        }
        delete this->visualGraph;
    }

    bool Manager::IsRunning() {
        return this->isRunning;
    }

    void Manager::Stop() {
        this->visualGraph->setLooped(false);
    }

    void Manager::Translate(int $code, char *$buffer, int *$size) {
        if ($size != nullptr) {
            string msg = VxTranslator::Translate($code, true);
            int size = static_cast<int>(msg.size());
            if ($buffer != nullptr && (*$size > 0)) {
                std::copy_n(msg.c_str(), (size > *$size) ? *$size : size, $buffer);
            }
            *$size = size;
        }
    }

    void Manager::ReadData(const char *$path, char *$data, int *$size) {
        if ($size != nullptr) {
            if (this->IsCreated() && $path != nullptr) {
                auto data = Io::VisionSDK::Studio::Libs::Utils::VxDataReader::Process(this->resolveData($path)).dump();
                int size = static_cast<int>(data.size());
                if ($data != nullptr && (*$size > 0) && (size <= *$size)) {
                    std::copy_n(data.c_str(), size, $data);
                }
                *$size = size;
            } else {
                *$size = vx_status_e::VX_FAILURE;
            }
        }
    }

    int Manager::WriteData(const char *$path, const char *$data) {
        vx_status status = vx_status_e::VX_FAILURE;
        if (this->IsCreated() && $path != nullptr && $data != nullptr) {
            string dataString = string($data);
            if (!dataString.empty()) {
                json data;
                try {
                    data = json::parse(dataString);
                } catch (const std::exception &$e) {}
                if (!data.empty()) {
                    status = Io::VisionSDK::Studio::Libs::Utils::VxDataWriter::Process(this->resolveData($path), data);
                }
            }
        }
        return status;
    }

    void Manager::ReadImage(const char *$path, char *$buffer, int *$size) {
        if ($size != nullptr) {
            int size = vx_status_e::VX_FAILURE;
            if (this->IsCreated() && $path != nullptr) {
                vx_reference ref = this->resolveData($path);
                if (ref != nullptr) {
                    vx_enum requestType;
                    vx_reference_attribute_e attributeType;
#if VX_VERSION == VX_VERSION_1_0 || VX_VERSION == VX_VERSION_1_1
                    attributeType = VX_REF_ATTRIBUTE_TYPE;
#elif VX_VERSION == VX_VERSION_1_2
                    attributeType = VX_REFERENCE_TYPE;
#else
#error "Check proper vx_reference_attribute_e type for used OpenVX version"
#endif
                    vx_status status = vxQueryReference(ref, attributeType, &requestType, sizeof(requestType));
                    if (status == vx_status_e::VX_SUCCESS) {
                        if (requestType == vx_type_e::VX_TYPE_IMAGE) {
                            cv::Mat image;
                            status = Convert::VxToCv((vx_image)ref, image, vxGetContext(ref));
                            if (status == vx_status_e::VX_SUCCESS) {
                                std::vector<uchar> buffer{};
                                auto it = this->convertCache.find(ref);
                                if (it == this->convertCache.end()) {
                                    this->convertCache[ref] = cv::Mat();
                                }
                                cv::imencode(".png", image, buffer);

                                size = static_cast<int>(buffer.size());
                                if ($buffer != nullptr && (*$size > 0) && (size <= *$size)) {
                                    std::copy_n(buffer.data(), size, $buffer);
                                }
                            }
                        }
                    }
                }
            }
            *$size = size;
        }
    }

    int Manager::WriteImage(const char *$path, char *$buffer, int $size) {
        vx_status status = vx_status_e::VX_FAILURE;
        if (this->IsCreated() && ($path != nullptr) && ($buffer != nullptr) && ($size > 0)) {
            vx_reference ref = this->resolveData($path);
            if (ref != nullptr) {
                vx_enum requestType;
                vx_reference_attribute_e attributeType;
#if VX_VERSION == VX_VERSION_1_0 || VX_VERSION == VX_VERSION_1_1
                attributeType = VX_REF_ATTRIBUTE_TYPE;
#elif VX_VERSION == VX_VERSION_1_2
                attributeType = VX_REFERENCE_TYPE;
#else
#error "Check proper vx_reference_attribute_e type for used OpenVX version"
#endif
                status = vxQueryReference(ref, attributeType, &requestType, sizeof(requestType));
                if (status == vx_status_e::VX_SUCCESS) {
                    std::vector<uchar> data($buffer, $buffer + $size);
                    auto it = this->convertCache.find(ref);
                    if (it != this->convertCache.end()) {
                        this->convertCache[ref] = cv::Mat();
                        it = this->convertCache.find(ref);
                    }
                    cv::Mat decoded = cv::imdecode(data, cv::ImreadModes::IMREAD_UNCHANGED, &(it->second));

                    auto tmp = (vx_image)ref;
                    status = Convert::CvToVx(decoded, tmp, vxGetContext(ref), true);
                }
            }
        }
        return status;
    }

    void Manager::ReadStats(char *$data, int *$size) {
        if ($size != nullptr) {
            if (this->IsCreated()) {
                json stats = {};
                json contextStats = {};
                for (const auto &context : this->visualGraph->getContexts()) {
                    json graphStats = {};
                    for (const auto &graph : context.second->getGraphs()) {
                        graphStats[graph.first] = graph.second->getStats();
                    }
                    contextStats[context.first] = {
                            {"elapsed", context.second->getStats()},
                            {"graphs",  graphStats}
                    };
                }
                stats["contexts"] = contextStats;

                string data = stats.dump();
                int size = static_cast<int>(data.size());
                if ($data != nullptr && (*$size > 0) && (size <= *$size)) {
                    std::copy_n(data.c_str(), size, $data);
                }
                *$size = size;
            } else {
                *$size = vx_status_e::VX_FAILURE;
            }
        }
    }

    std::vector<string> Manager::parseDataPath(const string &$path) {
        std::vector<string> paths = {};
        std::size_t index = {0};
        while ((index = $path.find_first_not_of('.', index)) != string::npos) {
            auto found = $path.find_first_of('.', index);
            paths.emplace_back($path.substr(index, found - index));
            index = found;
        }

        return paths;
    }

    vx_reference Manager::resolveData(const string &$path) {
        vx_reference retVal = nullptr;
        if (this->visualGraph != nullptr) {
            auto pathElements = Manager::parseDataPath($path);
            if (pathElements.size() == 3) {
                auto context = this->visualGraph->getContext(pathElements[0]);
                if (context != nullptr) {
                    auto graph = context->getGraph(pathElements[1]);
                    if (graph != nullptr) {
                        retVal = graph->getData(pathElements[2]);
                    }
                }
            }
        }
        return retVal;
    }

    bool Manager::IsCreated() {
        return this->isCreated;
    }

    bool Manager::IsValidated() {
        return this->isValidated;
    }

    void Manager::setCreated(bool $value) {
        this->isCreated = $value;
    }

    void Manager::setRunning(bool $value) {
        this->isRunning = $value;
    }

    void Manager::setValidated(bool $value) {
        this->isValidated = $value;
    }

    void Manager::setVisualGraph(BaseVisualGraph *$visualGraph) {
        this->visualGraph = $visualGraph;
    }

    void Manager::RegisterOnStart(const std::function<void(const string &)> &$handler) {
        if (this->visualGraph != nullptr) {
            auto handler = [this, $handler](const BaseObject *$owner) {
                string name = this->resolveObjectName($owner);
                if (!name.empty()) {
                    if ($handler != nullptr) {
                        $handler(name);
                    } else {
                        std::cerr << "onStart handler is null" << std::endl;
                    }
                }
            };
            for (const auto &context : this->visualGraph->getContexts()) {
                context.second->RegisterOnStart(handler);
                for (const auto &graph : context.second->getGraphs()) {
                    graph.second->RegisterOnStart(handler);
                }
            }
        }
    }

    void Manager::RegisterOnStop(const std::function<void(const string &, int)> &$handler) {
        if (this->visualGraph != nullptr) {
            auto handler = [this, $handler](const BaseObject *$owner, int $elapsed) {
                string name = this->resolveObjectName($owner);
                if (!name.empty()) {
                    if ($handler != nullptr) {
                        $handler(name, $elapsed);
                    } else {
                        std::cerr << "onStop handler is null" << std::endl;
                    }
                }
            };
            for (const auto &context : this->visualGraph->getContexts()) {
                context.second->RegisterOnStop(handler);
                for (const auto &graph : context.second->getGraphs()) {
                    graph.second->RegisterOnStop(handler);
                }
            }
        }
    }

    void Manager::RegisterOnError(const std::function<void(const string &, const string &)> &$handler) {
        if (this->visualGraph != nullptr) {
            auto handler = [this, $handler](const BaseObject *$owner, const string &$message) {
                string name = this->resolveObjectName($owner);
                if (!name.empty()) {
                    if ($handler != nullptr) {
                        $handler(name, $message);
                    } else {
                        std::cerr << "onError handler is null" << std::endl;
                    }
                }
            };
            for (const auto &context : this->visualGraph->getContexts()) {
                context.second->RegisterOnError(handler);
                for (const auto &graph : context.second->getGraphs()) {
                    graph.second->RegisterOnError(handler);
                }
            }
        }
    }

    string Manager::resolveObjectName(const Io::VisionSDK::Studio::Libs::Primitives::BaseObject *$object) {
        string name;
        for (const auto &context : this->visualGraph->getContexts()) {
            if (name.empty()) {
                if (context.second != $object) {
                    for (const auto &graph : context.second->getGraphs()) {
                        if (graph.second == $object) {
                            name = graph.first;
                            break;
                        }
                    }
                } else {
                    name = context.first;
                    break;
                }
            } else {
                break;
            }
        }
        return name;
    }
}
