/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include <ctime>
#include <mutex>

#include "../sourceFilesMap.hpp"

namespace Io::VisionSDK::Studio::Libs::Media {
    using Io::VisionSDK::Studio::Libs::Utils::Convert;
    using Io::VisionSDK::Studio::Libs::Enums::IOComType;

    std::map<string, std::thread *> threads = {};
    std::map<string, cv::Mat> data = {};
    std::map<string, std::mutex *> locker = {};
    int opened = 0;

    void Display::openInThread(const string &$name) {
        cv::namedWindow($name, CV_WINDOW_AUTOSIZE);

        opened++;
        do {
            if (locker[$name]->try_lock()) {
                cv::imshow($name, data[$name]);
                locker[$name]->unlock();
            }
            if (cv::waitKey(50) == 27) {
                break;
            }
        } while (static_cast<int>(cv::getWindowProperty($name, 0)) >= 0);
        opened--;

        threads.erase($name);
    }

    vx_status Display::Show(const cv::Mat &$img, const string &$name) {
        vx_status status = VX_SUCCESS;
        string winTitle = $name;
        if (winTitle.empty()) {
            winTitle = std::to_string(time(nullptr));
            int iterator = 0;
            string tmp = winTitle;
            while (threads.find(tmp) != threads.end()) {
                tmp = winTitle + "_" + std::to_string(++iterator);
            }
            winTitle = tmp;
        }

        if (locker.find(winTitle) == locker.end()) {
            locker.emplace(winTitle, new std::mutex());
        }

        locker[winTitle]->lock();
        data[winTitle] = $img;
        locker[winTitle]->unlock();
        if (threads.find(winTitle) == threads.end()) {
            threads.emplace(winTitle, new std::thread(Display::openInThread, winTitle));
            threads[winTitle]->detach();
        }
        return status;
    }

    vx_status Display::Show(const vx_context &$context, const vx_image &$image, const string &$name) {
        vx_status status;
        cv::Mat img;
        status = Convert::VxToCv($image, img, $context);
        if (status == VX_SUCCESS) {
            status = Display::Show(img, $name);
        }
        return status;
    }

    void Display::Close(const string &$name) {
        cv::destroyWindow($name);
    }

    void Display::CloseAll() {
        cv::destroyAllWindows();
    }

    void Display::WaitForAllClosed() {
        do {
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
        } while (opened > 0);

        threads = {};
    }

    Display::Display(const vx_context &$context, const string &$name)
            : context($context),
              name($name) {}

    void Display::Close() {
        cv::destroyWindow(this->name);
    }

    vx_status Display::getFrame(vx_image &$image, bool $copy) const {
        throw std::runtime_error("Method getFrame is not supported by Display type IOCom.");
    }

    vx_status Display::setFrame(const vx_image &$image) {
        vx_status status;
        cv::Mat img;
        status = Convert::VxToCv($image, img, this->context);
        if (status == vx_status_e::VX_SUCCESS) {
            status = Display::Show(img, this->name);
        }
        return status;
    }

    vx_status Display::getData(vx_reference &$object, bool $copy) const {
        auto imgRef = (vx_image)$object;
        return this->getFrame(imgRef, $copy);
    }

    vx_status Display::setData(vx_reference &$object) {
        auto imgRef = (vx_image)$object;
        return this->setFrame(imgRef);
    }

    IOComType Display::getType() const {
        return IOComType::DISPLAY;
    }
}
