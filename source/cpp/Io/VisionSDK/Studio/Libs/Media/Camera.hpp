/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_VISIONSDK_STUDIO_LIBS_MEDIA_CAMERA_HPP_
#define IO_VISIONSDK_STUDIO_LIBS_MEDIA_CAMERA_HPP_

namespace Io::VisionSDK::Studio::Libs::Media {
    /**
     * This class provides API to open and use camera streams.
     */
    class Camera: public Video {
     public:
        explicit Camera(const vx_context &$context);

        /**
         * Open camera device on specified index.
         * @param $device Specify device index [default = 0].
         * @return Returns true if succeed, false otherwise.
         */
        virtual bool Open(int $device = 0);

        vx_status setFrame(const vx_image &$image) override;

        vx_status setData(vx_reference &$object) override;

        Io::VisionSDK::Studio::Libs::Enums::IOComType getType() const override;

     private:
        double getCurrentTime() override;

        int getCurrentFrame() override;

        int getTotalFrames() override;

        double getFps() override;
    };
}

#endif  // IO_VISIONSDK_STUDIO_LIBS_MEDIA_CAMERA_HPP_
