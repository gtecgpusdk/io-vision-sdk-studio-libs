/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Io::VisionSDK::Studio::Libs::Media {
    using Io::VisionSDK::Studio::Libs::Enums::IOComType;

    Camera::Camera(const vx_context &$context) : Video($context) {}

    bool Camera::Open(int $device) {
        this->capture.open($device);
        return this->capture.isOpened();
    }

    vx_status Camera::setFrame(const vx_image &$image) {
        throw std::runtime_error("Method setFrame is not supported by Camera.");
    }

    vx_status Camera::setData(vx_reference &$object) {
        throw std::runtime_error("Method setData is not supported by Camera.");
    }

    double Io::VisionSDK::Studio::Libs::Media::Camera::getCurrentTime() {
        throw std::runtime_error("Method getCurrentTime is not supported by Camera.");
    }

    int Io::VisionSDK::Studio::Libs::Media::Camera::getCurrentFrame() {
        throw std::runtime_error("Method getCurrentFrame is not supported by Camera.");
    }

    int Io::VisionSDK::Studio::Libs::Media::Camera::getTotalFrames() {
        throw std::runtime_error("Method getTotalFrames is not supported by Camera.");
    }

    double Io::VisionSDK::Studio::Libs::Media::Camera::getFps() {
        throw std::runtime_error("Method getFps is not supported by Camera.");
    }

    IOComType Camera::getType() const {
        return IOComType::CAMERA;
    }
}
