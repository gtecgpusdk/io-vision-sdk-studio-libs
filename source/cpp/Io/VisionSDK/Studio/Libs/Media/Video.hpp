/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_VISIONSDK_STUDIO_LIBS_MEDIA_VIDEO_HPP_
#define IO_VISIONSDK_STUDIO_LIBS_MEDIA_VIDEO_HPP_

namespace Io::VisionSDK::Studio::Libs::Media {
    /**
     * This class provides API to load video from file and also methods to capture frame and other features
     * necessary for video access.
     */
    class Video : public Io::VisionSDK::Studio::Libs::Interfaces::IIOCom {
     public:
        /**
         * Constructs Video stream object.
         * @param $context Specify vx_context.
         */
        explicit Video(const vx_context &$context);

        /**
         * Open video input stream from specified path.
         * @param $path Specify path to video file.
         * @return Returns true if succeed, false otherwise.
         */
        virtual bool Open(const string &$path);

        /**
         * Open video stream with specified destination path.
         * @param $path Specify path to save file.
         * @param $fps Specify video fps or 0 for raw data.
         * @param $type Specify frame color type.
         * @param $width Specify with of incoming frames.
         * @param $height Specify height of incoming frames.
         * @param $codec Specify fourcc shortcut of codec or leave empty to use default one. (i.e. PIM1 - MJPEG1, MJPG - Moving JPEG)
         * @return Returns true if success, false if file is already opened by another application, or wrong parameter input (like unavailable codec).
         */
        virtual bool Open(const string &$path, double $fps, vx_df_image_e $type, int $width, int $height, const string &$codec = "");

        /**
         * Close video stream.
         */
        virtual void Close();

        /**
         * @return Returns time of current frame.
         */
        virtual double getCurrentTime();

        /**
         * @return Returns index of current frame.
         */
        virtual int getCurrentFrame();

        /**
         * @return Returns total frames count.
         */
        virtual int getTotalFrames();

        /**
         * @return Returns video stream internally defined FPS.
         */
        virtual double getFps();

        /**
         * @return Returns true if video stream is opened, false otherwise.
         */
        virtual bool IsOpen();

        /**
         * @return Returns frame width in pixels.
         */
        virtual int getWidth();

        /**
         * @return Returns frame height in pixels.
         */
        virtual int getHeight();

        vx_status getFrame(vx_image &$image, bool $copy = false) const override;

        vx_status setFrame(const vx_image &$image) override;

        vx_status getData(vx_reference &$object, bool $copy) const override;

        vx_status setData(vx_reference &$object) override;

        Io::VisionSDK::Studio::Libs::Enums::IOComType getType() const override;

     protected:
        cv::VideoCapture capture;
        cv::VideoWriter videoWriter;

        bool isOutput = false;
        bool convertToGray = false;
        cv::Mat convertTmp;

     private:
        vx_context context = nullptr;
    };
}

#endif  // IO_VISIONSDK_STUDIO_LIBS_MEDIA_VIDEO_HPP_
