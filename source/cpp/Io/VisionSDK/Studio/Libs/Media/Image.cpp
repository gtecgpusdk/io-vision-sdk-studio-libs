/* ********************************************************************************************************* *
*
* Copyright (c) 2017-2019 NXP
*
* SPDX-License-Identifier: BSD-3-Clause
* The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
* or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
*
* ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Io::VisionSDK::Studio::Libs::Media {
    using Io::VisionSDK::Studio::Libs::Utils::Convert;
    using Io::VisionSDK::Studio::Libs::Enums::IOComType;
    using Io::VisionSDK::Studio::Libs::Utils::FileSystem;

    vx_status Image::Load(const string &$path, const vx_context &$context, vx_image &$image, bool $copy) {
        cv::Mat cvImg = cv::imread($path, cv::ImreadModes::IMREAD_UNCHANGED);
        if (cvImg.empty()) {
            return vx_status_e::VX_FAILURE;
        }
        return Convert::CvToVx(cvImg, $image, $context, $copy);
    }

    vx_status Image::Save(const string &$path, const vx_context &$context, const vx_image &$image) {
        vx_status status;

        cv::Mat cvImg;
        status = Convert::VxToCv($image, cvImg, $context);

        if (status == vx_status_e::VX_SUCCESS) {
            FileSystem::CreateDirectory($path);
            status = cv::imwrite($path, cvImg) ? vx_status_e::VX_SUCCESS : vx_status_e::VX_FAILURE;
        }

        return status;
    }

    Image::Image(const string &$path, const vx_context &$context)
            : path($path),
              parentContext($context) {}

    vx_status Image::getFrame(vx_image &$image, bool $copy) const {
        if (this->path.empty()) {
            return vx_status_e::VX_ERROR_INVALID_VALUE;
        }
        if (this->image.empty()) {
            const_cast<Image *>(this)->image = cv::imread(this->path, cv::ImreadModes::IMREAD_UNCHANGED);
            if (this->image.empty()) {
                return vx_status_e::VX_FAILURE;
            }
        }

        return Convert::CvToVx(this->image, $image, this->parentContext, $copy);
    }

    vx_status Image::setFrame(const vx_image &$image) {
        vx_status status = Convert::VxToCv($image, this->image, this->parentContext);
        if (status == vx_status_e::VX_SUCCESS) {
            FileSystem::CreateDirectory(this->path);
            status = cv::imwrite(this->path, this->image) ? vx_status_e::VX_SUCCESS : vx_status_e::VX_FAILURE;
        }
        return status;
    }

    vx_status Image::getData(vx_reference &$object, bool $copy) const {
        auto imgRef = (vx_image)$object;
        return this->getFrame(imgRef, $copy);
    }

    vx_status Image::setData(vx_reference &$object) {
        auto imgRef = (vx_image)$object;
        return this->setFrame(imgRef);
    }

    const string &Image::getPath() const {
        return this->path;
    }

    IOComType Image::getType() const {
        return IOComType::IMAGE;
    }
}
