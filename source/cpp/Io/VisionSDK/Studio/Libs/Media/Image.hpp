/* ********************************************************************************************************* *
*
* Copyright (c) 2017-2019 NXP
*
* SPDX-License-Identifier: BSD-3-Clause
* The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
* or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
*
* ********************************************************************************************************* */

#ifndef IO_VISIONSDK_STUDIO_LIBS_MEDIA_IMAGE_HPP_
#define IO_VISIONSDK_STUDIO_LIBS_MEDIA_IMAGE_HPP_

namespace Io::VisionSDK::Studio::Libs::Media {
    /**
     * This class provides API to access image files.
     */
    class Image : public Io::VisionSDK::Studio::Libs::Interfaces::IIOCom {
     public:
        /**
         * Static method to load image file into vx_image object.
         * @param $path Specify image file path.
         * @param $context Specify vx_context owner.
         * @param $image Specify reference for loaded image. Referenced pointer will be replaced by new one if $copy is false.
         * @param $copy Specify true to force copy data from $input buffer to $output buffer, false by default.
         * @return Returns vx_status.
         */
        static vx_status Load(const string &$path, const vx_context &$context, vx_image &$image, bool $copy = false);

        /**
         * Static method to save vx_image object into file.
         * @param $path Specify image file path.
         * @param $context Specify vx_context owner.
         * @param $image Specify image to save.
         * @return Returns vx_status.
         */
        static vx_status Save(const string &$path, const vx_context &$context, const vx_image &$image);

        /**
         * Constructs Image object from image file path.
         * @param $path Specify path to image file.
         * @param $context Specify context.
         */
        Image(const string &$path, const vx_context &$context);

        vx_status getFrame(vx_image &$image, bool $copy = false) const override;

        vx_status setFrame(const vx_image &$image) override;

        vx_status getData(vx_reference &$object, bool $copy) const override;

        vx_status setData(vx_reference &$object) override;

        Io::VisionSDK::Studio::Libs::Enums::IOComType getType() const override;

        /**
         * @return Returns image file path referenced with this object at construction.
         */
        const string &getPath() const;

     protected:
        cv::Mat image;

     private:
        vx_context parentContext = nullptr;
        string path = "";
    };
}

#endif  // IO_VISIONSDK_STUDIO_LIBS_MEDIA_IMAGE_HPP_
