/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_VISIONSDK_STUDIO_LIBS_MEDIA_FILE_HPP_
#define IO_VISIONSDK_STUDIO_LIBS_MEDIA_FILE_HPP_

namespace Io::VisionSDK::Studio::Libs::Media {
    class File : public Io::VisionSDK::Studio::Libs::Interfaces::IIOCom {
     public:
        explicit File(const string &$path);

        vx_status getFrame(vx_image &$image, bool $copy) const override;

        vx_status setFrame(const vx_image &$image) override;

        vx_status getData(vx_reference &$object, bool $copy) const override;

        vx_status setData(vx_reference &$object) override;

        Enums::IOComType getType() const override;

     private:
        string path;
    };
}

#endif  // IO_VISIONSDK_STUDIO_LIBS_MEDIA_FILE_HPP_
