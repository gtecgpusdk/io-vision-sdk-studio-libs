/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include <filesystem>
#include <fstream>
#include "../sourceFilesMap.hpp"

#if VX_VERSION == VX_VERSION_1_2

static vx_status copyToFromTensor(vx_tensor $tensor, int16_t *$buffer, ssize_t $bufSize, vx_accessor_e $accessor) {
    vx_status status;
    vx_size numDims;
    status = vxQueryTensor($tensor, vx_tensor_attribute_e::VX_TENSOR_NUMBER_OF_DIMS, &numDims, sizeof(numDims));
    vx_type_e data_type;
    status |= vxQueryTensor($tensor, vx_tensor_attribute_e::VX_TENSOR_DATA_TYPE, &data_type, sizeof(data_type));
    vx_uint8 fixed_point_pos = 0;
    status |= vxQueryTensor($tensor, vx_tensor_attribute_e::VX_TENSOR_FIXED_POINT_POSITION, &fixed_point_pos, sizeof(fixed_point_pos));

    if ((status == vx_status_e::VX_SUCCESS) && (data_type == vx_type_e::VX_TYPE_INT16) && (fixed_point_pos == 8)) {
        vx_size elementSize = 2;
        std::vector<vx_size> dims;
        dims.resize(numDims);        // vx_size dims[numDims];
        std::vector<vx_size> start;
        start.resize(numDims);      // vx_size start[numDims];
        std::vector<vx_size> strides;
        strides.resize(numDims);  // vx_size strides[numDims];
        status = vxQueryTensor($tensor, vx_tensor_attribute_e::VX_TENSOR_DIMS, &dims[0], dims.size() * sizeof(vx_size));
        if (status == vx_status_e::VX_SUCCESS) {
            for (vx_size i = 0; i < numDims; i++) {
                start[i] = 0;
                if (i == 0) {
                    strides[i] = elementSize;
                } else {
                    strides[i] = strides[i - 1] * dims[i - 1];
                }
            }
            if (($bufSize == -1)
                || ($bufSize == static_cast<ssize_t>(strides[numDims - 1] * dims[numDims - 1]))) {
                status = vxCopyTensorPatch($tensor, numDims, &start[0], &dims[0], &strides[0], $buffer, $accessor,
                                           vx_memory_type_e::VX_MEMORY_TYPE_HOST);
                $bufSize = strides[numDims - 1] * dims[numDims - 1] / elementSize;
            } else {
                status = vx_status_e::VX_FAILURE;
            }
        }
    }
    return status;
}

template<typename InType, typename OutType>
static vx_status convert(const std::vector<InType> &$in, std::vector<OutType> &$out, std::function<OutType(InType)> $converter) {
    if ($in.size() != $out.size()) {
        return vx_status_e::VX_FAILURE;
    }

    auto inData = $in.data();
    auto outData = $out.data();

    for (unsigned i = 0; i < $in.size(); i++) {
        outData[i] = $converter(inData[i]);
    }

    return vx_status_e::VX_SUCCESS;
}

static vx_status copyDataFromTensor(vx_tensor $tensor, std::vector<float> &$output) {
    std::vector<int16_t> outputQ78;
    outputQ78.resize($output.size());
    copyToFromTensor($tensor, outputQ78.data(), -1, vx_accessor_e::VX_READ_ONLY);

    const auto q78ToFloatConvertFunction = [](int16_t $val) -> float {
        return (static_cast<float>($val)) / 256.0f;
    };

    return convert<int16_t, float>(outputQ78, $output, q78ToFloatConvertFunction);
}

#endif

namespace Io::VisionSDK::Studio::Libs::Media {
    using Io::VisionSDK::Studio::Libs::Enums::IOComType;

    File::File(const string &$path) {
        this->path = $path;
    }

    vx_status File::getFrame(vx_image &$image, bool $copy) const {
        return vx_status_e::VX_FAILURE;
    }

    vx_status File::setFrame(const vx_image &$image) {
        return vx_status_e::VX_FAILURE;
    }

    vx_status File::getData(vx_reference &$object, bool $copy) const {
        vx_type_e type;
#if VX_VERSION == VX_VERSION_1_1
        vx_reference_attribute_e typeAttribute = vx_reference_attribute_e::VX_REF_ATTRIBUTE_TYPE;
#elif VX_VERSION == VX_VERSION_1_2
        vx_reference_attribute_e typeAttribute = vx_reference_attribute_e::VX_REFERENCE_TYPE;
#endif
        vx_status status = vxQueryReference($object, typeAttribute, &type, sizeof(vx_type_e));

        if (status == vx_status_e::VX_SUCCESS) {
            switch (type) {
                case vx_type_e::VX_TYPE_IMAGE: {
                    auto imgRef = (vx_image)$object;
                    status = this->getFrame(imgRef, $copy);
                    break;
                }
#if VX_VERSION == VX_VERSION_1_2
                case vx_type_e::VX_TYPE_TENSOR: {
                    std::ifstream input(this->path);
                    string line;
                    std::vector<float> arr;
                    std::vector<int16_t> arrQ78;

                    while (getline(input, line)) {
                        std::istringstream s(line);
                        std::string field;
                        while (getline(s, field, ',')) {
                            float res;
                            std::istringstream val(field);
                            if (!(val >> res)) {
                                return vx_status_e::VX_FAILURE;
                            }
                            arr.emplace_back(res);
                        }
                    }

                    const auto floatToQ78ConvertFunction = [](float $val) -> int16_t {
                        float r = $val < 0.0f ? -0.5f : 0.5f;
                        int tmpValue = static_cast<int>(($val * 256.0 + r));
                        int16_t value = tmpValue > SHRT_MAX ? SHRT_MAX : (tmpValue < SHRT_MIN ? SHRT_MIN : (int16_t)tmpValue);
                        int16_t res;
                        memcpy(&res, &value, sizeof(int16_t));
                        return res;
                    };

                    arrQ78.resize(arr.size());
                    status = convert<float, int16_t>(arr, arrQ78, floatToQ78ConvertFunction);
                    if (status == vx_status_e::VX_SUCCESS) {
                        auto tensor = (vx_tensor)$object;
                        status = copyToFromTensor(tensor, arrQ78.data(), -1, vx_accessor_e::VX_WRITE_ONLY);
                    }
                    break;
                }
#endif
                default: {
                    status = vx_status_e::VX_ERROR_INVALID_REFERENCE;
                }
            }
        }

        return status;
    }

    vx_status File::setData(vx_reference &$object) {
        vx_type_e type;
#if VX_VERSION == VX_VERSION_1_1
        vx_reference_attribute_e typeAttribute = vx_reference_attribute_e::VX_REF_ATTRIBUTE_TYPE;
#elif VX_VERSION == VX_VERSION_1_2
        vx_reference_attribute_e typeAttribute = vx_reference_attribute_e::VX_REFERENCE_TYPE;
#endif
        vx_status status = vxQueryReference($object, typeAttribute, &type, sizeof(vx_type_e));

        if (status == vx_status_e::VX_SUCCESS) {
            switch (type) {
                case vx_type_e::VX_TYPE_IMAGE: {
                    auto imgRef = (vx_image)$object;
                    status = this->setFrame(imgRef);
                    break;
                }
#if VX_VERSION == VX_VERSION_1_2
                case vx_type_e::VX_TYPE_TENSOR: {
                    std::vector<float> out(1);
                    status = copyDataFromTensor((vx_tensor)$object, out);

                    if (status == vx_status_e::VX_SUCCESS) {
                        std::ofstream output(this->path);
                        if (output.is_open()) {
                            for (std::size_t i = 0; i < out.size(); i++) {
                                output << out[i];
                                if (i < out.size() - 1) {
                                    output << " ,";
                                }
                            }
                            status = vx_status_e::VX_SUCCESS;
                        }
                    }
                    break;
                }
#endif
                default: {
                    status = vx_status_e::VX_ERROR_INVALID_REFERENCE;
                }
            }
        }

        return status;
    }

    IOComType File::getType() const {
        return IOComType::FILE;
    }
}
