/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Io::VisionSDK::Studio::Libs::Media {
    using Io::VisionSDK::Studio::Libs::Utils::Convert;
    using Io::VisionSDK::Studio::Libs::Enums::IOComType;

    Video::Video(const vx_context &$context)
            : context($context) {}

    bool Video::Open(const string &$path) {
        this->isOutput = false;
        this->capture.open($path);
        return this->capture.isOpened();
    }

    bool Video::Open(const string &$path, double $fps, vx_df_image_e $type, int $width, int $height, const string &$codec) {
        this->isOutput = true;

        bool isColor = false;
        if (($type == vx_df_image_e::VX_DF_IMAGE_RGB) || ($type == vx_df_image_e::VX_DF_IMAGE_RGBX)) {
            isColor = true;
        }

        int fourcc = 0;  // uncompressed
        if (!$codec.empty()) {
            if ($codec == "-1" || $codec == "unknown") {
                fourcc = -1;
            } else {
                fourcc = CV_FOURCC($codec[0], $codec[1], $codec[2], $codec[3]);
            }
        }

        // for invalid fps force set raw format
        if ($fps <= 0) {
            $fps = 0;
            fourcc = 0;
        }

        return this->videoWriter.open($path, fourcc, $fps, {$width, $height}, isColor);
    }

    void Video::Close() {
        if (!this->isOutput) {
            if (this->capture.isOpened()) {
                this->capture.release();
            }
        } else {
            if (this->videoWriter.isOpened()) {
                this->videoWriter.release();
            }
        }
    }

    double Video::getCurrentTime() {
        double retVal = 0.0;
        if (!this->isOutput && this->capture.isOpened()) {
            retVal = this->capture.get(cv::VideoCaptureProperties::CAP_PROP_POS_MSEC);
        }
        return retVal;
    }

    int Video::getCurrentFrame() {
        int retVal = 0;
        if (!this->isOutput && this->capture.isOpened()) {
            retVal = static_cast<int>(this->capture.get(cv::VideoCaptureProperties::CAP_PROP_POS_FRAMES));
        }
        return retVal;
    }

    int Video::getTotalFrames() {
        int retVal = 0;
        if (!this->isOutput && this->capture.isOpened()) {
            retVal = static_cast<int>(this->capture.get(cv::VideoCaptureProperties::CAP_PROP_FRAME_COUNT));
        }
        return retVal;
    }

    double Video::getFps() {
        double retVal = 0.0;
        if (!this->isOutput && this->capture.isOpened()) {
            retVal = static_cast<int>(this->capture.get(cv::VideoCaptureProperties::CAP_PROP_FPS));
        }
        return retVal;
    }

    bool Video::IsOpen() {
        return !this->isOutput ? this->capture.isOpened() : this->videoWriter.isOpened();
    }

    int Video::getWidth() {
        int retVal = 0;
        if (!this->isOutput && this->capture.isOpened()) {
            retVal = static_cast<int>(this->capture.get(cv::VideoCaptureProperties::CAP_PROP_FRAME_WIDTH));
        }
        return retVal;
    }

    int Video::getHeight() {
        int retVal = 0;
        if (!this->isOutput && this->capture.isOpened()) {
            retVal = static_cast<int>(this->capture.get(cv::VideoCaptureProperties::CAP_PROP_FRAME_HEIGHT));
        }
        return retVal;
    }

    vx_status Video::getFrame(vx_image &$image, bool $copy) const {
        vx_status retStatus;
        if (!this->isOutput) {
            cv::Mat frame;
            if (this->capture.isOpened()) {
                if (!this->convertToGray) {
                    const_cast<Video *>(this)->capture >> frame;
                } else {
                    const_cast<Video *>(this)->capture >> const_cast<Video *>(this)->convertTmp;
                    cv::cvtColor(convertTmp, frame, cv::ColorConversionCodes::COLOR_BGR2BGRA);
                    // TODO(nxa33894) may other formats will need to be converted by different way, has to be checked
                }
            }

            retStatus = Convert::CvToVx(frame, $image, this->context, $copy);
        } else {
            throw std::runtime_error("getFrame is not supported for video output stream.");
        }
        return retStatus;
    }

    vx_status Video::setFrame(const vx_image &$image) {
        vx_status retStatus = vx_status_e::VX_FAILURE;
        if (this->isOutput) {
            cv::Mat frame;
            if (this->videoWriter.isOpened()) {
                // TODO(nxa33894) may check format and selection is used to be here
                retStatus = Convert::VxToCv($image, frame, this->context);
                if (retStatus == vx_status_e::VX_SUCCESS) {
                    this->videoWriter << frame;
                }
            }
        } else {
            throw std::runtime_error("setFrame is not supported for video input stream.");
        }
        return retStatus;
    }

    vx_status Video::getData(vx_reference &$object, bool $copy) const {
        auto imgRef = (vx_image)$object;
        return this->getFrame(imgRef, $copy);
    }

    vx_status Video::setData(vx_reference &$object) {
        auto imgRef = (vx_image)$object;
        return this->setFrame(imgRef);
    }

    IOComType Video::getType() const {
        return IOComType::VIDEO;
    }
}
