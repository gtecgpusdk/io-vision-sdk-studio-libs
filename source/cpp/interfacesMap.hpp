/** WARNING: this file has been automatically generated from C++ interfaces and classes, which exist in this package. */
// NOLINT (legal/copyright)

#ifndef IO_VISIONSDK_STUDIO_LIBS_INTERFACESMAP_HPP_  // NOLINT
#define IO_VISIONSDK_STUDIO_LIBS_INTERFACESMAP_HPP_

namespace Io {
    namespace VisionSDK {
        namespace Studio {
            namespace Libs {
                namespace Enums {
                    enum IOComType : int;
                }
                namespace Interfaces {
                    class IIOCom;
                    class IManager;
                }
                namespace Media {
                    class Camera;
                    class Display;
                    class File;
                    class Image;
                    class Video;
                }
                namespace Primitives {
                    class Any;
                    class BaseContext;
                    class BaseGraph;
                    class BaseObject;
                    class BaseVisualGraph;
                    class VxReferenceInfo;
                }
                namespace Utils {
                    class Convert;
                    class FileSystem;
                    class Manager;
                    class StopWatch;
                    class VxDataReader;
                    class VxDataWriter;
                    class VxTranslator;
                }
            }
        }
    }
}

#endif  // IO_VISIONSDK_STUDIO_LIBS_INTERFACESMAP_HPP_  // NOLINT
